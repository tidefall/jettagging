import numpy as np
from datetime import datetime
import json
import h5py
from data_utilities import expected_tags, make_fileNames, unfile_name,\
        get_var_keys, get_tar_key, get_2DIPsig_key
from contextlib import ExitStack
import csv
from os import path, mkdir
import errno
import sys

def readCSV(dir_name, file_names):
    """Read the files of event data.

    The files should be in csv format in the specified directory.
    For jet variable and meta variables they should be arranged one line per event.
    For track variables they should be arranged one line per jet.

    Parameters
    ----------
    dir_name : string
        The name of the directory to look for the files in
    file_names : list of strings
        The names of the files to read

    Returns
    -------
    data : 3d list
        contains the contents of each file named, organised per line.

    """
    data = []
    with ExitStack() as stack:
        data_files = [stack.enter_context(open(path.join(dir_name, fname),
                                          'r')) for fname in file_names]
        data_CSVs = [csv.reader(file) for file in data_files]
        for dCSV in data_CSVs:
            data.append([[float(x) for x in row] for row in dCSV])
    return data


def vars_by_jet(dir_name="./practice/", file_names=None):
    """Read the csv files on the disk and return the variables indexed by jet

    The files should be in csv format in the specified directory.
    For jet variable and meta variables they should be arranged one line per event.
    For track variables they should be arranged one line per jet.
    This function will resort them so that they are aranged first by 
    variable catigory (Meta, jet, track)
    then by variable
    then by jet

    Parameters
    ----------
    dir_name : String, optional
        The name of the directory to look in.
        Defaults to the practice directory
    file_names : list of strings, optional
        The names of the files in the directory (Default value = None)

    Returns
    -------
        all_names : 2D list of strings
            names of each variable
            aranged by varaible type (meta, jet, track)
        all_vars : 3D list of floats
            value of each varaibles organised in the same manner as all_names

    """
    if file_names is None:
        file_names = make_fileNames(expected_tags())
    meta_fNames, jet_fNames, track_fNames, etaRel_fNames = file_names
    # Get nice formatng for the names
    meta_names = unfile_name(meta_fNames)
    jet_names = unfile_name(jet_fNames)
    track_names = unfile_name(track_fNames)
    etaRel_names = unfile_name(etaRel_fNames)
    # Get the perJet and track variables as normal
    jet_vars = [sum(j_var, []) for j_var in readCSV(dir_name, jet_fNames)]
    track_vars = readCSV(dir_name, track_fNames)
    etaRel_vars = readCSV(dir_name, etaRel_fNames)
    # Use the number of jets per event to resize the meta variables
    meta_vars_per_event = readCSV(dir_name, meta_fNames)
    meta_vars_per_event = [sum(var, []) for var in meta_vars_per_event]
    jets_per_event = meta_vars_per_event[meta_names.index("num jets per event")]
    meta_vars = [sum([[val] * int(jets) 
                     for jets, val in zip(jets_per_event, var)], [])
                 for var in meta_vars_per_event]
    all_names = [meta_names, jet_names, track_names, etaRel_names]
    all_vars = [meta_vars, jet_vars, track_vars, etaRel_vars]
    return all_names, all_vars


def input_keys_names(var_keys, all_names):
    """
    Helper function to get the names and locations of the variabels
    that will be inputs for the net.

    Parameters
    ----------
    var_keys : list of ints
        number of this variable that is required as input
        only not equal to 1 for track varaibles
    all_names : 2D list of strings
        names of each variable

    Returns
    -------
    key_names_list : list of tuples

    """
    num_required = [1 for var in var_keys]  # Default to 1
    # for some variables we take the values of several tracks
    num_required[var_keys.index((2, "3D IP sig"))] = 4 
    keys_names_list = []
    for var_reached, num_var in enumerate(num_required):
        var_key = var_keys[var_reached]
        var_index2 = all_names[var_key[0]].index(var_key[1])
        if var_key[0] == 1:
            keys_names_list.append((var_key[1], var_key[0], var_index2))
        else:
            for track in range(num_var):
                input_name = var_key[1]+" "+str(track + 1) 
                keys_names_list.append((input_name, var_key[0],
                                        var_index2, track))
    return keys_names_list


def twoD_IP_sig_array(all_names, jets):
    """
    Obtain the 2D IP sig for all jets, required as a discriminating varaibles
    This is a helper function.

    Parameters
    ----------
    all_names : 2D list of strings
        names of each variable
    jets : 3D list of floats
        the jet variables aranged as in all_names
        

    Returns
    -------
    twoD_IP_sigs: 2D numpy array of floats
        the 2D IP sig of each track, arranged by jet
        The array is padded with nan as not all tracks have
        the same number of jets

    """
    key = get_2DIPsig_key()
    index = all_names[key[0]].index(key[1])
    twoD_IP_sigs_list = jets[key[0]][index]
    # pad with nan
    # find the maximum number of tracks
    max_tracks = max(len(jet) for jet in twoD_IP_sigs_list)
    twoD_IP_sigs = np.full((len(twoD_IP_sigs_list), max_tracks), np.nan)
    for jetN, jet in enumerate(twoD_IP_sigs_list):
        twoD_IP_sigs[jetN, :len(jet)] = jet
    return twoD_IP_sigs

   
def target_input_arrays(all_names, jets):
    """A function to create a variable array and a list of targets for
    reco vertex catigory, any other catigory is a cut down of this.

    Parameters
    ----------
    all_names : 2D list of strings
        names of each variable
    jets : 3D list of floats
        the jet variables aranged as in all_names
        

    Returns
    -------
    targets : numpy array of ints
        the target of each jet as an array of ints
    target_dic : dictionary
        keys are strings naming jet flavour
        values are ints as in the target array
    variables : 2D numpy array of floats
        the values of the input variables, arranged by jet
    variables_dic : dictionary
        keys are the key names of the variables
        values are the indices they are found at
        in the variable array

    """
    n_jets = len(jets[0][0])
    # a dictionary linking sorted jets to target_labels
    # these are particle IDs
    pid_dic = {5: 'b', 4: 'c'}
    jet_catagories = list(pid_dic.values()) + ['udsg']
    target_dic = {k: i for i, k in enumerate(jet_catagories)}
    # process the targets
    tar_key = get_tar_key()
    tar_index = all_names[tar_key[0]].index(tar_key[1])
    jet_pids = jets[tar_key[0]][tar_index]
    targets = np.array([target_dic[pid_dic.get(abs(int(pid)), 'udsg')]
                        for pid in jet_pids])
    # begin to extract the variables
    variables_dic = {}
    var_keys = get_var_keys()
    keys_names_list = input_keys_names(var_keys, all_names)
    n_vars = len(keys_names_list)
    # it is easiest t extract by variable then transpose the matrix
    # default value is nan
    variablesT = np.full((n_vars, n_jets), np.nan)
    for e_i, entry in enumerate(keys_names_list):
        variables_dic[entry[0]] = e_i
        if(len(entry) == 3):  # the variable is jets based
            variable_values = jets[entry[1]][entry[2]]
        elif(len(entry) == 4):  # the variable is tracks based
            variable_values = []
            for tracks in jets[entry[1]][entry[2]]:
                if len(tracks) > entry[3]:
                    variable_values.append(tracks[entry[3]])
                else:
                    variable_values.append(np.nan)
        else:
            print("Error, what kind of input key is ", entry)
        variablesT[e_i] = variable_values
    variables = variablesT.T
    return targets, target_dic, variables, variables_dic


def save_processed_variables(directory_name, name_base, targets, target_dic,
                             variables, variables_dic, twoD_IP_sig):
    """Once the processing is done, save the results to hdf5 files

    Parameters
    ----------
    directory_name : string
        name of the directory to place the saved varaibles        
    name_base : string
        a base name that will be included in all file names
    targets : numpy array of ints
        the target of each jet as an array of ints
    target_dic : dictionary
        keys are strings naming jet flavour
        values are ints as in the target array
    variables : 2D numpy array of floats
        the values of the input variables, arranged by jet
    variables_dic : dictionary
        keys are the key names of the variables
        values are the indices they are found at
        in the variable array
    twoD_IP_sigs: 2D numpy array of floats
        the 2D IP sig of each track, arranged by jet
        The array is padded with nan as not all tracks have
        the same number of jets

    """
    file_path_hdf5 = path.join(directory_name, name_base + ".h5")
    with h5py.File(file_path_hdf5, 'w') as df:
        df.create_dataset("variables", data=variables)
        df.create_dataset("targets", data=targets)
        df.create_dataset("2DIPsig", data=twoD_IP_sig)
    file_path_tdic = path.join(directory_name, name_base + "_tdic.json")
    with open(file_path_tdic, 'w') as jf:
        json.dump(target_dic, jf)
    file_path_vdic = path.join(directory_name, name_base + "_vdic.json")
    with open(file_path_vdic, 'w') as jf:
        json.dump(variables_dic, jf)


def read_processed_variables(directory_name, name_base):
    """retreve the vaiables and the tagerts from the hdf5

    Parameters
    ----------
    directory_name : string
        the name of the directory to read from
    name_base :
        the base name of the files to be read

    Returns
    -------
    targets : numpy array of ints
        the target of each jet as an array of ints
    target_dic : dictionary
        keys are strings naming jet flavour
        values are ints as in the target array
    variables : 2D numpy array of floats
        the values of the input variables, arranged by jet
    variables_dic : dictionary
        keys are the key names of the variables
        values are the indices they are found at
        in the variable array
    twoD_IP_sigs: 2D numpy array of floats
        the 2D IP sig of each track, arranged by jet
        The array is padded with nan as not all tracks have
        the same number of jets

    """
    file_path = path.join(directory_name, name_base + ".h5")
    with h5py.File(file_path, 'r') as df:
        targets = df["targets"][:]
        variables = df["variables"][:]
        twoD_IP_sig = df["2DIPsig"][:]
    file_path_tdic = path.join(directory_name, name_base + "_tdic.json")
    with open(file_path_tdic, 'r') as jf:
        target_dic = json.loads(jf.read())
    file_path_vdic = path.join(directory_name, name_base + "_vdic.json")
    with open(file_path_vdic, 'r') as jf:
        variables_dic = json.loads(jf.read())
    variables_dic = {k: int(v) for k, v in variables_dic.items()}
    return targets, target_dic, variables, variables_dic, twoD_IP_sig


def save_combined_data(directory_name, name_base, target_dic, data_dic, data):
    """
    Read the data from file, in an all-in-one form

    Parameters
    ----------
    directory_name : string
        name of the directory that the data is in
    name_base : string
        base name of the data files
    target_dic : dictionary
        dictionary whos keys are the string names of the targets
        the values are the numbers that represent the target in the data
    data_dic : dictionary
        dictionary where the keys are the names of the columns in the data
        the values are the indices of the columns
    data : 2D numpy array
        array containing all variable, targets and discriminating variables
        the structure is given by data_dic
    """
    file_path_hdf5 = path.join(directory_name, name_base + ".h5")
    with h5py.File(file_path_hdf5, 'w') as df:
        df.create_dataset("data", data=data)
    file_path_tdic = path.join(directory_name, name_base + "_tdic.json")
    with open(file_path_tdic, 'w') as jf:
        json.dump(target_dic, jf)
    file_path_vdic = path.join(directory_name, name_base + "_ddic.json")
    with open(file_path_vdic, 'w') as jf:
        json.dump(data_dic, jf)


def read_combined_data(directory_name, name_base):
    """retreve the vaiables and the tagerts from the hdf5

    Parameters
    ----------
    directory_name : string
        name of the directory that the data is in
    name_base : string
        base name of the data files
        

    Returns
    -------
    target_dic : dictionary
        dictionary whos keys are the string names of the targets
        the values are the numbers that represent the target in the data
    data_dic : dictionary
        dictionary where the keys are the names of the columns in the data
        the values are the indices of the columns
    data : 2D numpy array
        array containing all variable, targets and discriminating variables
        the structure is given by data_dic

    """
    file_path = path.join(directory_name, name_base + ".h5")
    with h5py.File(file_path, 'r') as df:
        data = df["data"][:]
    file_path_tdic = path.join(directory_name, name_base + "_tdic.json")
    with open(file_path_tdic, 'r') as jf:
        target_dic = json.loads(jf.read())
    file_path_ddic = path.join(directory_name, name_base + "_ddic.json")
    with open(file_path_ddic, 'r') as jf:
        data_dic = json.loads(jf.read())
    data_dic = {k: int(v) for k, v in data_dic.items()}
    return target_dic, data_dic, data


if __name__ == "__main__":
    """ This module can be run with certain defaults or used as a library.
    """
    read_dir = "practice"
    print("Reading the variables from {0}...".format(read_dir))
    all_names, all_vars = vars_by_jet(read_dir)
    print("Making to numpy arrays")
    targets, target_dic, variables, variables_dic =\
        target_input_arrays(all_names, all_vars)
    print("finding the 2d IP sig")
    twoD_IP_sig = twoD_IP_sig_array(all_names, all_vars)
    dir_name = "unproc_hdf5_" + datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    base_name = "whole_tree"
    print("saving in folder {0} with base name {1} ..."
          .format(dir_name, base_name))
    try:
        mkdir(dir_name)
    except OSError as e:
        if e.errno != errno.EEXIST:
            print("Some error when trying to make a folder for saving")
            sys.exit(1)
    save_processed_variables(dir_name, base_name, targets,
                             target_dic, variables, variables_dic, twoD_IP_sig)

