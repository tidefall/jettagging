# -*- coding: utf-8 -*-
"""
Created on Wed Feb 14 11:42:40 2018

@author: henry

A module to load the data from the root file
"""
from data_utilities import make_fileNames
from contextlib import ExitStack
import csv
from os import path
import ROOT
import numpy as np
import math  # for nan
import torch.utils.data as data


class RootTreeData(data.Dataset):
    """Reads data from a root ttree in a .root file"""
    def __init__(self, file_path="TTTo2L2Nu_ntp.root",
                 tree_path="btagana/ttree",
                 start_index=0, events_required=100):
        """Initilisation method.

        Parameters
        ----------
        file_path : string
            the full or relative path to the file
        tree_path : string
            location of the tree within the file
        start_index : int
            Index of the first event to read
        events_required : int
            number of events to read (reads at least 1 event)

        """
        my_file = ROOT.TFile(file_path)
        self.my_tree = my_file.Get(tree_path)
        num_entries = self.my_tree.GetEntries()
        # Check the inputs are use-able
        assert start_index > -1, "Start index must be positive"
        assert start_index < num_entries, \
            "Start index must be lower than number of entries"
        if events_required < 0:  # GO to end of events
            events_required = num_entries - start_index
        assert events_required + start_index <= num_entries, \
            "Not enough entries after start index"
        self.n_events = events_required
        if self.n_events == 0:
            self.n_events = 1
        # Dictionary of values discribing the shape of the data
        # each event has one int
        self.meta = {"num jets per event": [],  # nJet
                     # ^ clean
                     "num trks per event": []  # nTrkTagVarCSV
                     # ^ clean
                    }
        # Dictionary of values relating to the Jet as a whole
        # Each even is length num jets
        self.jets = {"parton flavour": [],  # Jet_partonFlavour
                                            # = PID of the parton that
                                            # created the jet
                     # ^ keeps some data at 0
                     # zero is not a valid PID
                     "num eta-rel trks per jet": [],
                     # TagVarCSV_jetNTracksEtaRel
                     # ^ seems clean, note never equal to 1
                     # var 1 ~~~~~~~~~~~~~~
                     "SV 2D flight dist sig": [],  # TagVarCSV_flightDistance2dSig
                     # ^ stores indetermined values at -9999
                     # var 2 ~~~~~~~~~~~~~~~~~~~
                     "num SVs": [],  # TagVarCSV_jetNSecondaryVertices
                     # ^ clean
                     # var 4 ~~~~~~~~~~~~
                     "SV mass": [],  # TagVarCSV_vertexMass
                     # ^ stores indetermined values at -9999
                     # var 5 ~~~~~~~~~~
                     "num trks from SV": [],  # TagVarCSV_vertexNTracks
                     # ^ seems clean, can equal 0
                     # var 6 ~~~~~~~~~~~~~
                     "SV energy ratio": [],  # TagVarCSV_vertexEnergyRatio
                     # ^ stores indetermined values at -9999
                     # var 7 ~~~~~~~~~~~~
                     "SV delta R": [],  # TagVarCSV_vertexJetDeltaR = ??
                     # ^ stores indetermined values at -9999
                     # var 17 ~~~~~~~~~~~~~~~~
                     "num trks per jet": [],  # TagVarCSV_jetNTracks
                     # ^ seems clean, can equal 0
                     # var 18 ~~~~~~~~~~~`
                     "jet pt": [],  # Jet_pt
                     # ^ clean
                     # var 19 ~~~~~~~~~~~~~
                     "jet eta": [],  # Jet_eta
                     # ^ clean
                     # var 14 ~~~~~~~~~~~~
                     "summed trks Et ratio": [],  # TagVarCSV_trackSumJetEtRatio
                     # ^ stores indetermined values at -9999
                     # var 15 ~~~~~~~~~~~~~
                     "summed delta R": [],  # TagVarCSV_trackSumJetDeltaR
                     # ^ stores indetermined values at -9999
                     # var 16 ~~~~~~~~~~~~~
                     "first trk 2D IP sig above charm": [],
                     # TagVarCSV_trackSip2dSigAboveCharm
                     # ^ stores indetermined values at -9999
                     }

        # Dictionary of track specific veriables
        # Each even is length num tracks
        self.tracks = {
                       # Needed for sorting
                       "2D IP sig": [],  # TagVarCSV_trackSip2dSig
                       # ^ clean (Should I be using the above charm value here)
                       # var 8 ~~~~~~~~~~~~~~~~~
                       "3D IP sig": [],  # TagVarCSV_trackSip3dSig
                       # ^ clean
                       # var 9 ~~~~~~~~~~~~~~~
                       "trk relative pt": [],  # TagVarCSV_trackPtRel
                       # ^clean
                       # var 10 ~~~~~~~~~~~~~~
                       "trk delta R": [],  # TagVarCSV_trackDeltaR
                       # ^ clean
                       # var 11 ~~~~~~~~~~~~~~~
                       "trk pt ratio": [],  # TagVarCSV_trackPtRatio
                       # ^ clean
                       # var 12 ~~~~~~~~~~~~~~
                       "trk distance": [],  # TagVarCSV_trackJetDistVal
                       # ^ clean
                       # var 13 ~~~~~~~~~~~~~~~~
                       "trk decay length": [],  # TagVarCSV_trackDecayLenVal
                       # ^ clean
                      }
        # Gets its own dictionary because it a diffrent length
        self.etaRel = {  # var 3 ~~~~~~~~~~~~~~
                             "trk eta rel": []  # TagVarCSV_trackEtaRel
                             # ^ clean
                            }
        i = 0
        # Go through th event in the tree, fixing the problems listed above
        while self.my_tree.GetEntry(i+start_index):
            # clean
            self.meta["num jets per event"].append(int(self.my_tree.nJet))
            # clean
            self.meta["num trks per event"]\
                .append(int(self.my_tree.nTrkTagVarCSV))
            # Zero is not a PID, but it won't be confused for anything else
            self.jets["parton flavour"]\
                .append(list(self.my_tree.Jet_partonFlavour))
            # clean
            self.jets["num trks per jet"]\
                .append(list(self.my_tree.TagVarCSV_jetNTracks))
            # clean
            self.jets["num eta-rel trks per jet"]\
                .append(list(self.my_tree.TagVarCSV_jetNTracksEtaRel))
            # Values at -9999 are N.A
            raw_fd_2d = list(self.my_tree.TagVarCSV_flightDistance2dSig)
            self.jets["SV 2D flight dist sig"]\
                .append(self._remove_unphysical(raw_fd_2d))
            # clean
            self.jets["num SVs"]\
                .append(list(self.my_tree.TagVarCSV_jetNSecondaryVertices))
            # Values at -9999 are N.A
            raw_mass = list(self.my_tree.TagVarCSV_vertexMass)
            self.jets["SV mass"].append(self._remove_unphysical(raw_mass))
            # clean
            self.jets["num trks from SV"]\
                .append(list(self.my_tree.TagVarCSV_vertexNTracks))
            # Values at -9999 are N.A
            raw_er = list(self.my_tree.TagVarCSV_vertexEnergyRatio)
            self.jets["SV energy ratio"]\
                .append(self._remove_unphysical(raw_er))
            # Values at -9999 are N.A
            raw_dr = list(self.my_tree.TagVarCSV_vertexJetDeltaR)
            self.jets["SV delta R"].append(self._remove_unphysical(raw_dr))
            # clean
            self.jets["jet pt"].append(list(self.my_tree.Jet_pt))
            # clean
            self.jets["jet eta"].append(list(self.my_tree.Jet_eta))
            # Values at -9999 are N.A
            raw_2d_ac = list(self.my_tree.TagVarCSV_trackSip2dSigAboveCharm)
            self.jets["first trk 2D IP sig above charm"]\
                .append(self._remove_unphysical(raw_2d_ac))
            # Values at -9999 are N.A
            raw_etr = list(self.my_tree.TagVarCSV_trackSumJetEtRatio)
            self.jets["summed trks Et ratio"]\
                .append(self._remove_unphysical(raw_etr))
            # Values at -9999 are N.A
            raw_sjdr = list(self.my_tree.TagVarCSV_trackSumJetDeltaR)
            self.jets["summed delta R"]\
                .append(self._remove_unphysical(raw_sjdr))
            # clean
            self.tracks["2D IP sig"]\
                .append(list(self.my_tree.TagVarCSV_trackSip2dSig))
            # clean
            self.tracks["3D IP sig"]\
                .append(list(self.my_tree.TagVarCSV_trackSip3dSig))
            # clean
            self.tracks["trk relative pt"]\
                .append(list(self.my_tree.TagVarCSV_trackPtRel))
            # clean
            self.tracks["trk delta R"]\
                .append(list(self.my_tree.TagVarCSV_trackDeltaR))
            # clean
            self.tracks["trk pt ratio"]\
                .append(list(self.my_tree.TagVarCSV_trackPtRatio))
            # clean
            self.tracks["trk distance"]\
                .append(list(self.my_tree.TagVarCSV_trackJetDistVal))
            # clean
            self.tracks["trk decay length"]\
                .append(list(self.my_tree.TagVarCSV_trackDecayLenVal))
            # clean
            self.etaRel["trk eta rel"]\
                .append(list(self.my_tree.TagVarCSV_trackEtaRel))
            i += 1
            if i >= events_required:
                break
        self.meta_tags = sorted(self.meta.keys())
        self.jet_tags = sorted(self.jets.keys())
        self.track_tags = sorted(self.tracks.keys())
        self.etaRel_tags = sorted(self.etaRel.keys())

    def __getitem__(self, selector):
        """ Get a single event from the tree
       
        treeData[event number][perJet, perTrack, perEtaRel][n,  m]

        Parameters
        ----------
        selector : tuple of (string, int)
            The first item in the tuple discribes type of variable to be
            returned; "perJet", "perTrack", "perEtaRel" or "meta". The second
            item is the event number.

        Returns
        -------
        : np_array of floats
            The set of data of the specified type for the specifed event
        """
        perString, index = selector
        if perString == "perJet":
            jets_i = [[float(a) for a in v[index]]
                      for k, v in sorted(self.jets.items())]
            return np.array(jets_i)
        elif perString == "perTrack":
            tracks_i = [[float(a) for a in v[index]]
                        for k, v in sorted(self.tracks.items())]
            return np.array(tracks_i)
        elif perString == "perEtaRel":
            etaRel_i = [v[index] for k, v in self.etaRel.items()]
            return np.array(etaRel_i)
        elif perString == "meta":
            meta_i = [v[index] for k, v in sorted(self.meta.items())]
            return np.array(meta_i)
        else:
            print("Vaild indices are tuples with (str, int) where\
            str = perJet, perTrack, meta or perEtaRel")
            raise IndexError

    def __len__(self):
        """The number of events in the RootTreeData"""
        return self.n_events

    def _remove_unphysical(self, the_values, storage_lid=-9000):
        """Helper function. Removes values that are below the storage lid."""
        return [x if x > storage_lid else math.nan for x in the_values]


def toCSV(tree_data, dir_name):
    """Make a csv out of the given tree data object.

    Parameters
    ----------
    tree_data : RootTreeData
        the data to be put into the csv
    dir_name : string
        The path to the directory to save at
    """
    num_events = tree_data.n_events
    # Create some filenames
    tree_data_for_tags = RootTreeData(events_required=0)
    tags_list = [tree_data_for_tags.meta_tags,
                 tree_data_for_tags.jet_tags,
                 tree_data_for_tags.track_tags,
                 tree_data_for_tags.etaRel_tags]
    meta_file_names, jet_file_names, track_file_names, etaRel_file_names =\
        make_fileNames(tags_list, dir_name=dir_name)
    # using ExitStack to properly close all the files
    with ExitStack() as stack:
        # open all the files
        meta_files = [stack.enter_context(open(fname, 'a')) for fname
                      in meta_file_names]
        meta_CSVs = [csv.writer(file) for file in meta_files]
        jet_files = [stack.enter_context(open(fname, 'a')) for fname
                     in jet_file_names]
        jet_CSVs = [csv.writer(file) for file in jet_files]
        track_files = [stack.enter_context(open(fname, 'a')) for fname
                       in track_file_names]
        track_CSVs = [csv.writer(file) for file in track_files]
        etaRel_files = [stack.enter_context(open(fname, 'a')) for fname
                        in etaRel_file_names]
        etaRel_CSVs = [csv.writer(file) for file in etaRel_files]
        # write the data
        for event in range(num_events):
            # meta variables each on their own row
            for variable, m_CSV in enumerate(meta_CSVs):
                m_CSV.writerow([tree_data["meta", event][variable], ])
            # per jet variables arranged by event
            for variable, j_CSV in enumerate(jet_CSVs):
                j_CSV.writerow(tree_data["perJet", event][variable])
            # per track variables arranged by jet
            N_tracks_in_jets = tree_data.jets["num trks per jet"][event]
            for variable, t_CSV in enumerate(track_CSVs):
                event_tracks = tree_data["perTrack", event][variable]
                i_jet_start = 0
                # go through each jet
                for N_tracks_in_jet in N_tracks_in_jets:
                    i_jet_end = int(i_jet_start + N_tracks_in_jet)
                    t_CSV.writerow(event_tracks[i_jet_start:i_jet_end])
                    i_jet_start = i_jet_end
                # check we actually got all the tracks
                assert i_jet_start == len(event_tracks), \
                    (("The event number is {2}, variable is {3}." +
                      "The last slice was to {0}," +
                      "the number of tracks in the event is {1}")
                     .format(i_jet_end, len(event_tracks),
                             event, variable))
            # per track variables arranged by jet
            N_etaRel_in_jets = \
                tree_data.jets["num eta-rel trks per jet"][event]
            for variable, e_CSV in enumerate(etaRel_CSVs):
                event_etaRel = tree_data["perEtaRel", event][variable]
                i_jet_start = 0
                # go through each jet
                for N_etaRel_in_jet in N_etaRel_in_jets:
                    i_jet_end = int(i_jet_start + N_etaRel_in_jet)
                    e_CSV.writerow(event_etaRel[i_jet_start:i_jet_end])
                    i_jet_start = i_jet_end
                # check we actually got all the tracks
                assert i_jet_start == len(event_etaRel)
            if(event % 1000 == 0):
                print("Reached event ", event)


if __name__ == "__main__":
    """Provides a way to run this module is a default manner"""
    print("reading all events into a tree data object")
    tree_data = RootTreeData(events_required=-1)
    print("events read in, writing to csvs")
    toCSV(tree_data, "csvs")
    
