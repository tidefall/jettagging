import time
import numpy as np
import ipdb
import h5py
import csv
from split_reweigh import split_inputs_by_target
import matplotlib.pyplot as plt
import matplotlib
import copy
import torch
from torch.autograd import Variable
from data_utilities import get_all_input_dic
from sklearn.metrics import roc_curve, auc
import sys
import os
from sys import argv
from data_utilities import plot_colour


def prediction_for_target(net, test_loader, target_dic):
    """A function to mesure the predictied probabilities of the test data being
    the focus target

    Parameters
    ----------
    net : Net
        the neural network to be trained
    test_loader : DataSet
        an object to give the data in the correct form
    target_dic : dictionary
        dictionary whos keys are the string names of the targets
        the values are the numbers that represent the target in the data

    Returns
    -------
    is_target : dictionary of dictionarys
        the first dictionary has keys corrsiponding the the flavour being predicted for
        the inner dictionary has keys corrisponding to the true flavour
        the values of the inner dictionary are the predictions made for that section
        by the net
    test_targets : numpy array
        the targets of the test set
    outputs : numpy array
        the outputs of the net for each target

    """
    net.eval()
    test_targets, test_inputs = test_loader.get_all()
    outputs = net(test_inputs)
    is_target = {predicted_name: {true_flavour:[] for true_flavour in target_dic}
                 for predicted_name in target_dic}
    inverse_target_dic = {v: k for k, v in target_dic.items()}
    test_targets = test_targets.data.numpy()
    outputs = outputs.data.numpy()
    for target, output in zip(test_targets, outputs):
        true_i = np.argmax(target)
        true_flavour = inverse_target_dic[true_i]
        for pred_name, pred_i in target_dic.items():
            neg_out = -output[pred_i]
            pred = 1./(1+np.exp(neg_out))
            is_target[pred_name][true_flavour].append(pred)
    return is_target, test_targets, outputs


def hist_predictions(target_dic, results_dics, name_bases, save_dir, save_base):
    """
    punction to plot and save histograms of the predictions of the net(s) by true jet flavour

    Parameters
    ----------
    target_dic : dictionary
        dictionary whos keys are the string names of the targets
        the values are the numbers that represent the target in the data
    results_dics : list of dictionarys
        each item in the list is an is_target dictionary;
        is_target : dictionary of dictionarys
            the first dictionary has keys corrsiponding the the flavour being predicted for
            the inner dictionary has keys corrisponding to the true flavour
            the values of the inner dictionary are the predictions made for that section
            by the net
    name_bases : list of strings
        the names to give each net in the plots 
    save_dir : string
        directory to save plots into
    save_base : string
        base name to put in all saved file names
    """
    plt.style.use('ggplot')
    target_names = list(target_dic.keys())
    colour = plot_colour()
    if len(results_dics) > 1:
        fig, ax_arr = plt.subplots(nrows=len(results_dics), ncols=len(target_dic),
                                   sharex=True, sharey=True, figsize=(14, 10))
        for col, name in enumerate(target_names):
            for row, is_target in enumerate(results_dics):
                ax = ax_arr[row, col]
                net_name = name_bases[row]
                prediction_list = [is_target[name][nm] for nm in target_names]
                color_list = [colour[nm] for nm in target_names]
                ax.hist(prediction_list, bins=50, normed=True, histtype='step',
                        label=target_names, range=[0.,1.], color=color_list,
                        log=True)
                ax.legend(loc="lower left")
                ax.set_title("{0}; predicting is {1}".format(net_name, name))
                ax.set_xlabel("discriminator")
                ax.set_ylabel("Normalised frequency")
    else:
        fig, ax_arr = plt.subplots(nrows=len(target_dic), ncols=1,
                                   sharex=True, sharey=True, figsize=(9, 10))
        is_target = results_dics[0]
        net_name = name_bases[0]
        for row, name in enumerate(target_names):
            ax = ax_arr[row]
            prediction_list = [is_target[name][nm] for nm in target_names]
            color_list = [colour[nm] for nm in target_names]
            ax.hist(prediction_list, bins=50, normed=True, histtype='step',
                    label=target_names, range=[0.,1.], color=color_list,
                    log=True)
            ax.legend(loc="lower left")
            ax.set_title("{0}; predicting is {1}".format(net_name, name))
            ax.set_xlabel("discriminator")
            ax.set_ylabel("Normalised frequency")

    save_name = os.path.join(save_dir, save_base + "_hist_preds.eps")
    plt.savefig(save_name)
    plt.show()


def roc(target_dic, targets, outputs_list, name_bases, save_dir, save_base):
    """

    Parameters
    ----------
    target_dic : dictionary
        dictionary whos keys are the string names of the targets
        the values are the numbers that represent the target in the data
    test_targets : numpy array
        the targets of the test set
    outputs_list :  list of numpy arrays
        listfor each net of
        the outputs of the net for each target
    name_bases : list of strings
        the names to give each net in the plots 
    save_dir : string
        directory to save plots into
    save_base : string
        base name to put in all saved file names

    """
    plt.style.use('ggplot')
    colour = plot_colour()
    plt.figure(figsize=(12, 12))
    plt.plot([0, 1], [0, 1], linestyle='--', color=colour["grey"])
    plt.xlabel("False positive rate")
    plt.ylabel("True positive rate")
    plt.xlim([0., 1.])
    plt.ylim([0., 1.05])
    linestyles = ['-', '--', '-.', ':']
    for i, outputs in enumerate(outputs_list):
        line_style = linestyles[i % len(linestyles)]
        net_name = name_bases[i]
        for name in target_dic:
            true_index = target_dic[name]
            binary_targets = targets[:, true_index]
            neg_outs = -outputs[:, true_index]
            preds = 1./(1+np.exp(neg_outs))
            fpr, tpr, _ = roc_curve(binary_targets, preds)
            this_auc = auc(fpr, tpr)
            label = "{}; target {} (area={:5.3f})".format(net_name, name, this_auc)
            # if "SV" in net_name:
            #     plt.plot(fpr, tpr, color=colour[name], label=label,
            #              linestyle=line_style, marker='o', markevery=100,
            #              mfc=colour['grey'], mew=0.)
            # else:
            plt.plot(fpr, tpr, color=colour[name], label=label,
                     linestyle=line_style)
    plt.legend(loc="lower right")
    save_name = os.path.join(save_dir, save_base + "_roc.eps")
    plt.savefig(save_name)
    plt.show()


if __name__ == "__main__":
    er_code = 0
    # check the first argument exists
    arg_num = 1
    if len(argv) < arg_num+1:
        print("Please specify the date of the hdf5s to read.")
        sys.exit(1)
    else:
        start_date = argv[arg_num]
        arg_num += 1
    # check the second argument exists
    if len(sys.argv) < arg_num+1:
        print("Please specify the start point, either reweigh or split")
        sys.exit(1)
    else:
        start_point = sys.argv[arg_num]
        arg_num += 1
    # check the third argument exists
    if len(sys.argv) < arg_num+1:
        print("Please give the base name")
        sys.exit(1)
    else:
        read_base = sys.argv[arg_num]
        arg_num += 1
    # check the fifth argument exists
    if len(sys.argv) < arg_num+1:
        print("Please specify a directory for the output")
        sys.exit(1)
    else:
        out_dir = sys.argv[arg_num]
        arg_num += 1
    # check the third argument exists
    if len(sys.argv) < arg_num+1:
        print("Please specify the base name for the output")
        sys.exit(1)
    else:
        out_base = sys.argv[arg_num]
        arg_num += 1
    if len(sys.argv) < arg_num+1:
        print("Please specify the number of nets being plotted")
        sys.exit(1)
    else:
        num_nets = int(sys.argv[arg_num])
        arg_num += 1
    # check the forth argument exists
    if len(argv) < arg_num+1:
        print("Please specify the names of the net to evaluate")
        sys.exit(1)
    else:
        state_names = argv[arg_num:arg_num+num_nets]
        arg_num += num_nets
    if len(argv) < arg_num+1:
        name_bases = [name[:-21].replace('_', ' ') for name in state_names]
    else:
        name_bases = argv[arg_num:arg_num+num_nets]
        arg_num += num_nets
    print("Print names; {}".format(name_bases))
    # Get the net
    nets = []
    results_dics = []
    outputs_list = []
    for state_name in state_names:
        print("Loading net from {0}".format(state_name))
        state = torch.load(state_name)
        i_chosen_vars = state['i chosen vars']
        num_inps = state['num inputs']
        num_tars = state['num targets']
        print("NUmber of inputs = {0}, number of targets = {1}".format(num_inps, num_tars))
        is_CSVv2 = True  # chenge this bit for other nets....
        if is_CSVv2:
            from NN_CSVv2 import Net, read_data, select_indices, DataSet
        net = Net(num_inps, num_tars)
        net.load_state_dict(state['net'][-1])
        nets.append(net)
    
        # get the trainign data
        target_dic, data_dic, data = read_data(start_date, start_point, read_base)
        print("Number of testing jets = {0},".format(len(data)))
        print("Targets found = {0}".format(target_dic))

        data_dic, data = select_indices(data_dic, data, i_chosen_vars)
        print("Testing on data variables;")
        print(data_dic)

        # make a test loader
        test_loader = DataSet(target_dic, data_dic, data, 5, targets_one_hot=True, is_train=False)

        is_target_dic, targets, outputs = prediction_for_target(net, test_loader, target_dic)
        outputs_list.append(outputs)
        results_dics.append(is_target_dic)
    hist_predictions(target_dic, results_dics, name_bases, out_dir, out_base)
    roc(target_dic, targets, outputs_list, name_bases, out_dir, out_base)
    
    sys.exit(er_code)

