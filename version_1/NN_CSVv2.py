"""
Neural network for processing data.
An immitation of CSVv2

"""
import copy
import sys
import csv
import ipdb
import time
import os
from sys import argv  # For getting file paths with wildcards
from csv_to_hdf5 import read_combined_data
from lr_scheduler import ReduceLROnPlateau
from split_reweigh import balence_target_weights
import torch
import torch.nn as nn
import numpy as np
from data_utilities import get_all_input_dic
from torch.autograd import Variable


#########################################################
#
# FUNCTION DECLARATIONS
#
#########################################################

class Net(nn.Module):
    """A 'deep' neural net with one hidden layer"""
    def __init__(self, input_size, num_classes):
        """Initilzation for the net. Creates the layer that will be used.

        Parameters
        ----------
        input_size : int
            the number of nodes at the input layer. This must equal the number
            of variables in each data point.
        num_classes : int
            number of nodes at the output layer. This must equal the number of
            classes we wish to sort the data into.

        """
        hidden_size = input_size*2
        super(Net, self).__init__()
        # create a linear fully connected layer
        self.hidden_layer = nn.Linear(input_size, hidden_size)
        # create a ReLU activation function
        self.relu = nn.ReLU()
        # create anouther linear fully connected layer
        self.out_layer = nn.Linear(hidden_size, num_classes)

    def forward(self, x):
        """Defines the actions for a forward pass through the net.

        Parameters
        ----------
        x : torch.FloatTensor
            the input variables from ths data point

        Returns
        -------
        x : torch.FloatTensor
            the output of the net
        
        """
        # begin by passing the data to a linear fully connected layer,
        # this si the hidden layer
        x = self.hidden_layer(x)
        # apply the activation function at the hidden layer
        x = self.relu(x)
        # move onto the output layer, which is also linear and fully connected
        x = self.out_layer(x)
        return x


def mse_loss(output, target):
    """A loss function to measure the performance of a net against its targets

    Parameters
    ----------
    output : torch.FloatTensor
        The output of the net
    target : torch.FloatTensor
        The ideal output

    Returns
    -------
    : torch.FloatTensor
        the loss
    
    """
    return torch.sum((output - target)**2)


class StableBCELoss(nn.modules.Module):
    """ """
    def __init__(self):
        super(StableBCELoss, self).__init__()

    def forward(self, input, target):
        """forward propagation function

        Parameters
        ----------
        input : torch.FloatTensor
            the actual output of the network
        target : torch.FloatTensor
            The ideal output

        Returns
        -------
        : torch.FloatTensor
            the loss

        
        """
        neg_abs = - input.abs()
        loss = input.clamp(min=0) - input * target + (1 + neg_abs.exp()).log()
        return loss.mean()


class DataSet:
    """this is basicaly a dataloader, except it holds the whole set in memory,
    which is not very efficient, but I don't have such big data set that this is an issue,
    if you do you will need to fix this.
    """
    def __init__(self, target_dic, data_dic, data, batch_size, targets_one_hot=False, is_train=False):
        """
        Initilisation function

        Parameters
        ----------
        target_dic : dictionary
            dictionary whos keys are the string names of the targets
            the values are the numbers that represent the target in the data
        data_dic : dictionary
            dictionary where the keys are the names of the columns in the data
            the values are the indices of the columns
        data : 2D numpy array
            array containing all variable, targets and discriminating variables
            the structure is given by data_dic
        batch_size : int
            number of jets to deliver in each call to next_batch
        targets_one_hot : boolean
            does the loss function require the targets to be in "one hot" form
            (default value = False)
        is_train : boolean
            is this a training set?
            if it is not to be treated as a training set batches will not be set up
            (default value = False)
        """
        self.targets_one_hot = targets_one_hot
        self.num_jets = len(data)
        self.target_dic = target_dic
        self.num_target_options = len(self.target_dic)
        self.data_dic = data_dic
        # some items are only needed for traing data
        if is_train:
            self.batch_size = batch_size
            print("Batch size = {0}".format(self.batch_size))
            self.weights = data[:, data_dic['weight']]
            # these values are needed for picking based on weight
            self.w_cumulative = np.cumsum(self.weights)
            self.w_sum = sum(self.weights)
            # we will be picking an index
            self.indices = np.arange(self.num_jets)
            self.num_batches = int(self.num_jets/self.batch_size)
        # set up the targets
        self.targets = np.array(data[:, self.data_dic['target']])
        if self.targets_one_hot:
            self.targets = np.array(one_hot(self.num_target_options, self.targets))
        else:
            self.targets.astype(int)
        # set up the inputs
        all_input_keys = get_all_input_dic().keys()
        self.input_dic = {k: v for k, v in data_dic.items() if k in all_input_keys}
        self.input_indices = sorted(list(self.input_dic.values()))
        self.inputs = data[:, self.input_indices]
        # if its not the training set we don't need ot be able to list index
        if not is_train:
            self.inputs = Variable(torch.FloatTensor(self.inputs),
                                   requires_grad=True)
            if self.targets_one_hot:
                self.targets = Variable(torch.FloatTensor(self.targets),
                                        requires_grad=False)
            else:
                self.targets = [int(x) for x in self.targets]
                self.targets = Variable(torch.LongTensor(self.targets),
                                        requires_grad=False)
    
    def next_batch(self):
        """ get the next batch of data and targets
        
        Returns
        -------
        batch_tars : torch.autograd.Variable
            set of targets for this batch
        batch_inps : torch.autograd.Variable
            matching set of network inputs for this batch
        """
        choices = np.searchsorted(self.w_cumulative,
                                  np.random.rand(self.batch_size) * self.w_sum)
        batch_inps = self.inputs[choices]
        batch_inps = Variable(torch.FloatTensor(batch_inps),
                              requires_grad=True)
        batch_tars = self.targets[choices]
        if self.targets_one_hot:
            batch_tars = Variable(torch.FloatTensor(batch_tars),
                                  requires_grad=False)
        else:
            batch_tars = [int(x) for x in batch_tars]
            batch_tars = Variable(torch.LongTensor(batch_tars),
                                  requires_grad=False)
        return batch_tars, batch_inps

    def get_all(self):
        """ function to return all the data in the dataset
        
        normally used for testing data
        
        Returns
        -------
        targets : torch.autograd.Variable
            all targets for the data set
        inputs : torch.autograd.Variable
            all inputs for the data set
        """
        return self.targets, self.inputs


def train(net, optimiser, target_dic, data_dic, train_data, test_data, save_name, end_t, state):
    """

    Parameters
    ----------
    net : Net
        the neural network to be trained
    optimiser : any kind of torch.optim.Optimser object
        the orpimser to be used in training
    target_dic : dictionary
        dictionary whos keys are the string names of the targets
        the values are the numbers that represent the target in the data
    data_dic : dictionary
        dictionary where the keys are the names of the columns in the data
        the values are the indices of the columns
    train_data : 2D numpy array
        array containing all variable, targets and discriminating variables
        the structure is given by data_dic
        this set used for training
    test_data : 2D numpy array
        array containing all variable, targets and discriminating variables
        the structure is given by data_dic
        this set used for testing
    save_name : string
        the base name for saving objects
    end_t : float
        time to end at to avoid being killed by wall time
    state : dictionary
        dictionary discribing the previos stae of the object

    """
    batch_size = state['batch size']
    data_loader = DataSet(target_dic, data_dic, train_data, batch_size,
                          is_train=True, targets_one_hot=True)
    # set up test data
    test_loader = DataSet(target_dic, data_dic, test_data, batch_size,
                          targets_one_hot=True)
    test_targets, test_inputs = test_loader.get_all()
    # make a loss function
    print("Loss function is custom stable BCE loss")
    loss_func = StableBCELoss()
    # make a schaduler to reduce the learning rate when loss is not decreasing
    scheduler = ReduceLROnPlateau(optimiser, epsilon=5e-4)
    # decide how frequently to save in epochs
    save_frequency = 10
    # set up to save the loss over time
    csv_file_name = "loss_" + save_name + ".csv"
    epoch_reached = state['epochs'][-1]
    print("Begingin epochs:")
    column_names = ["epoch_reached", "train_loss", "test_loss",
                    "mag_weights", "current learning rate"]
    print(column_names)
    if epoch_reached == 0:
        with open(csv_file_name, "a") as csv_file:
            csv_writer = csv.writer(csv_file, delimiter=' ')
            csv_writer.writerow(column_names)
    while time.time() < end_t:
        net.train()
        train_loss, sum_weight = epoch(net, loss_func, optimiser, data_loader) 
        # calculate the loss on the test set
        net.eval()
        test_outputs = net(test_inputs)
        test_loss = loss_func(test_outputs, test_targets).data[0]
        # check to see if the lr should decrease
        scheduler.step(test_loss, epoch_reached)
        # look at current learning rate
        current_lr = optimiser.param_groups[0]['lr']
        progress = [epoch_reached, train_loss, test_loss, sum_weight, current_lr]
        print("Epoch; {0}".format(progress))
        epoch_reached += 1 
        with open(csv_file_name, "a") as csv_file:
            csv_writer = csv.writer(csv_file, delimiter=' ')
            csv_writer.writerow(progress)
        if epoch_reached % save_frequency == 0:
            state['epochs'].append(epoch_reached)
            state['optimiser'] = optimiser.state_dict()
            state['net'].append(net.state_dict())
            state['learning rate'] = current_lr
            torch.save(state, state_name)
            print("Epoch {0}, state saved in {1}, time remaining = {2}"
                  .format(epoch_reached, state_name, end_t - time.time()))
    print("Finishing...")
    state['epochs'].append(epoch_reached)
    state['optimiser'] = optimiser.state_dict()
    state['net'].append(net.state_dict())
    torch.save(state, state_name)
    print("Epoch {0}, state saved in {1}".format(epoch_reached, state_name))
    sys.exit(er_code)


def epoch(net, criterion, optimiser, data_loader): 
    """

    Parameters
    ----------
    net : Net
        the neural network to be trained
    criterion : loss function
        probably a torch.nn.module.Module
        a function that computes the loss for the current batch
    optimiser : any kind of torch.optim.Optimser object
        the orpimser to be used in training
    data_loader : DataSet
        an object to give the data one batch at a time

    Returns
    -------
    ave_loss : float like object
        the average loss for the batch, for plotting
    sum_weight : float like object
        the sum of all the weights and bias terms in the network
        after this batch
        measure of complexity of the network

    """
    # Start working through the training epochs
    # each epoch goes over every data point
    sum_loss = 0.
    for i in range(data_loader.num_batches):
        target_batch, input_batch = data_loader.next_batch()
        # reset the optimiser
        optimiser.zero_grad()  # zero the gradient buffer
        # forward pass
        outputs = net(input_batch)
        # find loss
        loss = criterion(outputs, target_batch)
        # backwards pass
        loss.backward()
        sum_loss += loss.data[0]
        # optimise weights according to gradient found by backpropigation
        optimiser.step()
    ave_loss = sum_loss/data_loader.num_batches
    hidden_weights = net.hidden_layer.weight.data
    out_weights = net.out_layer.weight.data
    sum_weight = (torch.sum(torch.abs(hidden_weights)) + 
                  torch.sum(torch.abs(out_weights)))
    return ave_loss, sum_weight


def one_hot(num_label_options, labels):
    """A function to conver a list of labels to an array of targets

    Parameters
    ----------
    num_label_options : int
        the varierty of labels that will appear in the labels
    labels : list like of ints
        the label for each target

    Returns
    -------
    targets : 2D list of ints
        targets in one hot format

    """
    # create a list of zeros with the right shape for the target
    targets = np.zeros((len(labels), num_label_options))
    # make it a normal list because th np floats dont seem to play well 
    # with pytorch
    targets = targets.tolist()
    # put 1 into the position in the target vector that corrisponds to
    # the correct target
    for target, label in zip(targets, labels):
        target[int(round(label))] = 1.
    return targets


def read_data(start_date,  start_point, read_base):
    """
    Short script to read the data from file

    Parameters
    ----------
    start_date : string
        the data given on the directory name to read from        
    start_point : string
        the start point name that is part of the directory name containg the files
    read_base : string
        identifying component of the file names to read from

    Returns
    -------
    target_dic : dictionary
        dictionary whos keys are the string names of the targets
        the values are the numbers that represent the target in the data
    data_dic : dictionary
        dictionary where the keys are the names of the columns in the data
        the values are the indices of the columns
    data : 2D numpy array
        array containing all variable, targets and discriminating variables
        the structure is given by data_dic

    """
    read_dir = start_point + "_data_" + start_date
    # check the directory exists
    if not os.path.isdir("./" + read_dir):
        print("The directory {0} does not exist. exiting."
              .format(read_dir))
        sys.exit(10)
    print("Reading the variables from the hdf5 in {0} with base name {1}.."
          .format(read_dir, read_base))
    target_dic, data_dic, data = read_combined_data(read_dir, read_base)
    return target_dic, data_dic, data


def select_indices(data_dic, data, i_chosen_vars):
    """
    A function to select indices that will be int input for the net.

    It cuts down the data matrix, and the data dictionary to match 
    these indices.

    Parameters
    ----------
    data_dic : dictionary
        dictionary where the keys are the names of the columns in the data
        the values are the indices of the columns
    data : 2D numpy array
        array containing all variable, targets and discriminating variables
        the structure is given by data_dic
    i_chosen_vars : list of ints
        the indices of the variables that will be chosen

    Returns
    -------
    data_dic : dictionary
        dictionary where the keys are the names of the columns in the data
        the values are the indices of the columns
    data : 2D numpy array
        array containing all variable, targets and discriminating variables
        the structure is given by data_dic

    """
    if 'allind' in i_chosen_vars:
        print("all indices used")
        return data_dic, data
    print("Varaible indices chosen {0}".format(i_chosen_vars))
    # get the values out of the input dictionary
    new_data_dic = {}
    defaults = ['target', 'weight']
    for name in defaults:
        if name in data_dic:
            new_data_dic[name] = data_dic[name]
    for var in i_chosen_vars:
        found = False
        for k, v in data_dic.items():
            if var == v:
                new_data_dic[k] = v
                found = True
                break
        if not found:
            print("Warning {0} dosn't match any input in {1}"
                  .format(var, data_dic))
    data_dic = new_data_dic
    # now kill any column not in the new input dic
    data_indices = sorted(list(data_dic.values()))
    data = data[:, data_indices]
    # now the indices listed in the dictionary need to be put in the right places
    index_remap = {old: new for new, old in enumerate(sorted(data_indices))}
    data_dic = {name: index_remap[old] for name, old in data_dic.items()}
    assert data.shape[1] == len(data_dic),\
        "Cutting items from the input went wrong"
    return data_dic, data


if __name__ == "__main__":
    er_code = 0
    # the script is designed ot end gracfully before wall time gets it
    start_t = time.time()
    print("Starting")
    arg_num = 1
    # get desired run time in minutes
    if len(sys.argv) < arg_num+1:
        print("Please give a desired run time in minutes")
        sys.exit(1)
    else:
        run_time = int(argv[arg_num])*60
        end_t = start_t + run_time
        print("running for {0} minutes".format(run_time))
        arg_num += 1
    # check if an existing net is to be loaded
    if len(sys.argv) < arg_num+1:
        print("please specify an existing net, or say None")
        sys.exit(1)
    else:
        state_name = sys.argv[arg_num]
        if "none" in state_name.lower():
            new_net = True
        else:
            new_net = False
        arg_num += 1
    # get save name
    if len(sys.argv) < arg_num+1:
        print("Please specify the save_name")
        sys.exit(1)
    else:
        save_name = sys.argv[arg_num]
        arg_num += 1
    # if it's a new net we need setup parameters
    if new_net:
        # parameters to read data
        if len(argv) < arg_num+1:
            print("Please specify the date and time of the hdf5s to read.")
            sys.exit(1)
        else:
            start_date = argv[arg_num]
            arg_num += 1
        if len(sys.argv) < arg_num+1:
            print("Please specify the start point, either reweigh or split")
            sys.exit(1)
        else:
            start_point = sys.argv[arg_num]
            arg_num += 1
        # check the third argument exists
        if len(sys.argv) < arg_num+1:
            print("Please specify the base_name")
            sys.exit(1)
        else:
            read_base = sys.argv[arg_num]
            arg_num += 1
        # should the net be done with flavour renormalsiing?
        if len(sys.argv) < arg_num+1:
            print("please say true or false for flavour normalising")
            sys.exit(1)
        elif "true" in argv[arg_num].lower():
            normalise_flavours = True
            arg_num += 1
        elif "false" in argv[arg_num].lower():
            normalise_flavours = False
            arg_num += 1
        else:
            print("Didn't recognise flavor normalise argument")
            sys.exit(1)
        # get an initial learning rate
        if len(sys.argv) < arg_num+1:
            print("please specify an initial learning rate")
            sys.exit(1)
        else:
            learning_rate = float(sys.argv[arg_num])
            arg_num += 1
        # get a the weight decay
        if len(sys.argv) < arg_num+1:
            print("please specify a weight decay")
            sys.exit(1)
        else:
            weight_decay = float(sys.argv[arg_num])
            arg_num += 1
        # if arguments exist past this point the are indices to use
        if len(sys.argv) < arg_num+1:
            i_chosen_vars = ['allind']
            print("Using all variables")
        else:
            print("using partial varaible set")
            i_chosen_vars = [int(entry) for entry in sys.argv[arg_num:]]
    else:  # we are reading an existing net, get the parmeters
        # we have a previous state to load from
        print("Loading previous progress from {0}".format(state_name))
        state = torch.load(state_name)
        # now extract information from the state
        learning_rate = state['learning rate']
        epoch_reached = state['epochs']
        batch_size = state['batch size']
        num_inps = state['num inputs']
        num_tars = state['num targets']
        i_chosen_vars = state['i chosen vars']
        weight_decay = state['weight decay']
        normalise_flavours = state['normalise flavours']
        start_date = state['start date']
        start_point = state['start point']
        read_base = state['base name']
    # now should ahev all variables required to start
    test_base = "test_" + read_base
    train_base = "train_" + read_base
    test_target_dic, test_data_dic, test_data = read_data(start_date, start_point, test_base)
    target_dic, data_dic, train_data = read_data(start_date, start_point, train_base)
    # assert len(test_data_dic) == len(data_dic) - 1
    print("Test data dic is {0}".format(test_data_dic))
    print("train data dic is {0}".format(data_dic))
    print("Test target dic is {0}".format(test_target_dic))
    print("train target dic is {0}".format(target_dic))
    assert test_target_dic == target_dic
    # decide which variables are used
    if i_chosen_vars[0] == 'allind':
        num_inps = len(set(get_all_input_dic()) & set(data_dic))
    else:
        num_inps = len(i_chosen_vars)
        data_dic, train_data = select_indices(data_dic, train_data, i_chosen_vars)
        _, test_data = select_indices(test_data_dic, test_data, i_chosen_vars)
    # if desired, normalise the flavours
    print("Normalising the flavours in test_set")
    if normalise_flavours:
        train_data = balence_target_weights(target_dic, data_dic, train_data)
    # are we loading a previous net
    if new_net:
        print("No net loaded")
        # create the variabels for a state
        epoch_reached = 0
        state_name = save_name + "_net_" + start_date + ".torch"
        batch_size = 5
        num_jets = len(train_data)
        num_tars = len(target_dic)
        net = Net(num_inps, num_tars)
        optimiser = torch.optim.Adam(net.parameters(), lr=learning_rate,
                                     weight_decay=weight_decay)
        print("Adam optimiser with weight decay {0}".format(weight_decay))
        # create a new state disctionary
        state = {
                'epochs': [epoch_reached],
                'batch size': batch_size,
                'net': [net.state_dict()],
                'optimiser': optimiser.state_dict(),
                'num inputs': num_inps,
                'num targets': num_tars,
                'i chosen vars': i_chosen_vars,
                'weight decay': weight_decay,
                'learning rate': learning_rate,
                'normalise flavours': normalise_flavours,
                'start date': start_date,
                'start point': start_point,
                'base name': read_base
                }
    else:  # we have loaded a net
        # use the information to create the net and optimiser
        net = Net(num_inps, num_tars)
        optimiser = torch.optim.Adam(net.parameters(), lr=learning_rate, weight_decay=weight_decay)
        print("Adam optimiser with weight decay {0}".format(weight_decay))
        optimiser.load_state_dict(state['optimiser'])
        # load the most recent net
        net.load_state_dict(state['net'][-1])
    print("The columns used are {0}".format(data_dic))
    train(net, optimiser, target_dic, data_dic, train_data, test_data, save_name, end_t, state)


