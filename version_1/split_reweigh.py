import numpy as np
import time
import ipdb
import csv
import os
from tillHoffmann import gaussian_kde
from scipy.stats import norm
import sys
from os import mkdir, errno
import matplotlib.pyplot as plt
from csv_to_hdf5 import read_processed_variables, save_processed_variables,\
    read_combined_data, save_combined_data
from bisect import bisect, bisect_left
from data_utilities import get_uniform_input_names, make_psudo_dic,\
        make_no_dic 


def vertex_split(targets, inputs, inputs_dic, twoD_IP_sigs):
    """Split the variabels by the vertex type

    Parameters
    ----------
    targets : numpy array of ints
        the target of each jet as an array of ints
    variables : 2D numpy array of floats
        the values of the input variables, arranged by jet
    variables_dic : dictionary
        keys are the key names of the variables
        values are the indices they are found at
        in the variable array
    twoD_IP_sigs: 2D numpy array of floats
        the 2D IP sig of each track, arranged by jet
        The array is padded with nan as not all tracks have
        the same number of jets
        

    Returns
    -------
    vertex : tuple of 3
        Data for jets found to be RecoVertex type.
        first item is the variable dictionary
        second item is the targets
        third item is the input variables
    psudo : tuple of 3
        Data for jets found to be PsudoVertex type.
        same format as vertex
    no : tuple of 3
        Data for jets found to be NoVertex type.
        same format as vertex

    """
    # The number of seconadry vertice is found in the jet vars
    num_svs = inputs[:, inputs_dic["num SVs"]]
    # the number of tracks is also found in the jet vars
    num_tracks = inputs[:, inputs_dic["num trks per jet"]]
    # the mass of the vertex is in the jets variable
    vertex_mass = inputs[:, inputs_dic["SV mass"]]
    # The mass of the K-Short to compare against
    mass_KShort = 497.614
    # lists to contain the vertex, psudovertex and novertex lists of jets
    # will be converted to numpy arrays at the end but its easier to
    # append to normal lists of lists
    inputs_vertex = []
    inputs_psudo = []
    inputs_no = []
    targets_vertex = []
    targets_psudo = []
    targets_no = []
    for jetN in range(len(num_svs)):
        if int(num_svs[jetN]) > 0:
            inputs_vertex.append(inputs[jetN])
            targets_vertex.append(targets[jetN])
        elif(int(num_tracks[jetN]) > 1  # WRONG TODO fix this
             and sum(map(abs, twoD_IP_sigs[jetN])) > 2
             and abs(vertex_mass[jetN] - mass_KShort > 50)):
            inputs_psudo.append(inputs[jetN])
            targets_psudo.append(targets[jetN])
        else:
            inputs_no.append(inputs[jetN])
            targets_no.append(targets[jetN])
    # now cut out variables that don't exist in psudo or no
    psudo_dic, psudo_previous_indices = make_psudo_dic(inputs_dic)
    no_dic, no_previous_indices = make_no_dic(inputs_dic)
    if len(inputs_psudo) > 0:
        cut_psudo = np.array(inputs_psudo)[:, psudo_previous_indices]
    else:
        cut_psudo = np.array([]).reshape((0, len(psudo_dic)))
    if len(inputs_no) > 0:
        cut_no = np.array(inputs_no)[:, no_previous_indices]
    else:
        cut_no = np.array([]).reshape((0, len(no_dic)))
    vertex = (inputs_dic, np.array(targets_vertex), np.array(inputs_vertex))
    psudo = (psudo_dic, np.array(targets_psudo), cut_psudo)
    no = (no_dic, np.array(targets_no), cut_no)
    return vertex, psudo, no


def reweigh_flavours(target_dic, data_dic, data, t_end, density_tol=0.002,
                     plot=True):
    """Regularise the flavours so that chosen uniform varaibles
    have the same distribution for all target catigories.

    Parameters
    ----------
    target_dic : dictionary
        dictionary whos keys are the string names of the targets
        the values are the numbers that represent the target in the data
    data_dic : dictionary
        dictionary where the keys are the names of the columns in the data
        the values are the indices of the columns
    data : 2D numpy array
        array containing all variable, targets and discriminating variables
        the structure is given by data_dic
    t_end : float
        time at which to stop itterations
    density_tol : float
        float that relates to how close the density distributions must get before
        they are considred equal and the process is terminated
         (Default value = 0.002)
    plot : boolean
        should the progress be plotted dynamicly?
         (Default value = True)

    Returns
    -------
    data_dic : dictionary
        dictionary where the keys are the names of the columns in the data
        the values are the indices of the columns
        returned because it possibly has had the weight's index added to it
    all_data : 2D numpy array
        array containing all variable, targets and discriminating variables
        the structure is given by data_dic
    total_density_difs : dictionary
        the denisty difernces for each flavour, for each uniform variable
        at each point in time.

    """
    # we will match to flavour 'b'
    match_flavour = target_dic['b']
    flavour_indices = list(target_dic.values())
    total_num_jets = data.shape[0]
    num_vars = data.shape[1]
    print("Total number of jets = {0}, number of variables = {1}"
          .format(total_num_jets, num_vars))
    # split the inputs by target
    data_groups = split_data_by_target(target_dic, data_dic, data,
                                       target_name="target")
    num_jets = {k: v.shape[0] for k, v in data_groups.items()}
    flavour_indices = data_groups.keys()
    # create a dictionary to hold sorted lists of the values that
    # will take uniform distributions
    # also generate some meta data about these values
    print("Calculating the uniform")
    uniform_inp_names = get_uniform_input_names()
    uniform_indices = [data_dic[name] for name in uniform_inp_names] 
    uniform = Uniform(uniform_indices, match_flavour, data_groups, data_dic)
    print("setting up the plot")
    if plot:
        colour_list = ['b', 'g', 'r', 'c', 'm', 'y', 'k']
        flavour_colour = {}
        assert len(colour_list) > len(data_groups)
        num_uniform = len(uniform_indices)
        plt.ion()
        fig, ax_arr = plt.subplots(nrows=num_uniform+1)
        lines = {f_index: {u_index: None for u_index in uniform_indices}
                 for f_index in data_groups}
        dif_lines = {f_index: {u_index: None for u_index in uniform_indices}
                     for f_index in data_groups if f_index != match_flavour}
        w_ax = ax_arr[0]
        w_ax.hist([uniform.weights[f_index] for f_index in flavour_indices],
                  color=colour_list[:len(flavour_indices)], histtype='step',
                  label=flavour_indices, bins=100, range=(-1,16))
        w_ax.legend()
        for d_ax, u_name in zip(ax_arr[1:], uniform_inp_names):
            u_index = data_dic[u_name]
            d_ax.set_title(u_name)
            for i, f_index in enumerate(flavour_indices):
                colour = colour_list[i]
                flavour_colour[f_index] = colour
                unsorted_density = uniform.density[f_index][u_index]
                unsorted_values = uniform.values[f_index][u_index]
                sorted_dens = sorted(zip(unsorted_values, unsorted_density),
                                     key=lambda tup: tup[0])
                xs, y1s = zip(*sorted_dens)
                lines[f_index][u_index], = d_ax.plot(xs, y1s, c=colour,
                                                     label=str(f_index))
                if f_index != match_flavour:
                    unsorted_density_dif = uniform.density_dif[f_index][u_index]
                    sorted_dens_dif = sorted(zip(unsorted_values, unsorted_density_dif),
                                             key=lambda tup: tup[0])
                    _, y2s = zip(*sorted_dens_dif)
                    dif_lines[f_index][u_index], = d_ax.plot(xs, y2s, c=colour,
                                                             linestyle='--')
            d_ax.legend()
        plt.show()
        fig.canvas.draw()
    else:
        ax_arr = None
        flavour_colour = None
    # now the weights will be altered untill the uniform distributions are close
    density_tol *= total_num_jets
    ave_density_tol = density_tol/2
    # keep track of how close the densities are as we go
    total_density_difs = {flavour: {u_index: []
                                    for u_index in uniform_indices}
                          for flavour in flavour_indices
                          if flavour != match_flavour}
    batch_size = min(int(max(num_jets.values())*0.0005), 10)
    counter = 0
    print("Begging itterations")
    while continue_reweighing(total_density_difs, ave_density_tol):
        counter += 1
        all_done, total_density_difs =\
            reweigh_check(uniform, density_tol, batch_size, total_density_difs)
        # update the uniform
        uniform.update()
        if plot:
            # update the plot
            for u_name in uniform_inp_names:
                w_ax.clear()
                w_ax.hist([uniform.weights[f_index] for f_index in flavour_indices if f_index!=match_flavour],
                          color=[colour_list[i] for i, f_i in enumerate(flavour_indices) if f_i!=match_flavour], histtype='step',
                          label=[f_i for f_i in flavour_indices if f_i!=match_flavour], bins=100, range=(-1,16))
                for i, f_index in enumerate(flavour_indices):
                    colour = colour_list[i]
                w_ax.legend()
                for i, f_index in enumerate(flavour_indices):
                    unsorted_density = uniform.density[f_index][u_index]
                    unsorted_values = uniform.values[f_index][u_index]
                    sorted_vars =\
                        sorted(zip(unsorted_values, unsorted_density),
                               key=lambda tup: tup[0])
                    xs, ys = zip(*sorted_vars)
                    lines[f_index][u_index].set_ydata(ys)
                    if f_index != match_flavour:
                        unsorted_density_dif =\
                            uniform.density_dif[f_index][u_index]
                        sorted_dens_dif =\
                            sorted(zip(unsorted_values, unsorted_density_dif),
                                   key=lambda tup: tup[0])
                        _, y2s = zip(*sorted_dens_dif)
                        dif_lines[f_index][u_index].set_ydata(y2s)
            fig.canvas.draw()
        if all_done:
            break
        if counter % 50 == 0:
            print("Loop number {0}".format(counter))
            for flavour in flavour_indices:
                if flavour == match_flavour:
                    continue
                for index in uniform_indices:
                    print("last {0}, {1} dnsty df= {2}"
                          .format(flavour, index,
                                  total_density_difs[flavour][index][-1]))
        if time.time() > t_end:
            print("Warning, may not have properly finished")
            break
    # extract and reshape the weights
    print("extractng the learned weights")
    weights = {flavour: np.array(uniform.weights[flavour]) for
               flavour in flavour_indices}
    if 'weight' not in data_dic:
        # put the weights into the data
        # it will sit on the end
        data_dic["weight"] = len(data_dic)
        weights = {flavour: f_weights.reshape(num_jets[flavour], 1)
                   for flavour, f_weights in weights.items()}
        data_groups = {flavour: np.hstack((f_data, weights[flavour])) for
                       flavour, f_data in data_groups.items()}
    else:
        weight_index = data_dic['weight']
        for flavour in data_groups:
            data_groups[flavour][:, weight_index] =\
                weights[flavour]
    # now put the data back together
    print("reassembling the data")
    all_data = np.zeros_like(data)
    jet_reached = 0
    for flavour in data_groups:
        next_jet_reached = len(data_groups[flavour]) + jet_reached
        all_data[jet_reached:next_jet_reached] = data_groups[flavour]
        jet_reached = next_jet_reached
    return data_dic, all_data, total_density_difs


def reweigh_check(uniform, density_tol, batch_size, total_density_difs):
    """The process of chosing a jet and checking if it should be deleted
    Helper function for reweigh flavours

    Parameters
    ----------
    uniform : Uniform
        the object that hold information about
        densities that should be made uniform
    density_tol : float
        float that relates to how close the density distributions must get before
        they are considred equal and the process is terminated
         (Default value = 0.002)
    batch_size : int
        number of points to select in one check
    total_density_difs : dictionary
        the denisty difernces for each flavour, for each uniform variable
        at each point in time.

    Returns
    -------
    all_done : boolean
        is the process complete?
    total_density_difs : dictionary
        the denisty difernces for each flavour, for each uniform variable
        at each point in time.


    """
    assert len(uniform.weights[1]) > batch_size,\
        "The should be many more jets than the batch size"
    dif_is_below_tol = {u_index: True for u_index
                        in uniform.uniform_indices}
    for flavour in uniform.flavour_indices:
        if flavour == uniform.match_flavour:
            continue
        for u_index in uniform.uniform_indices:
            total_dif = sum([abs(dd) for dd
                             in uniform.density_dif[flavour][u_index]])
            total_density_difs[flavour][u_index].append(total_dif)
            if total_dif > density_tol:
                dif_is_below_tol[u_index] = False
    # if all the inputs are below the tollarance we are done
    all_done = all(dif_is_below_tol.values())
    if all_done:
        return all_done, total_density_difs
    # otherwise, sort the lists of density doffrences and
    # reweight jets near the largest diffrences
    for u_index in uniform.uniform_indices:
        for flavour in uniform.flavour_indices:
            if flavour == uniform.match_flavour:
                continue
            factor = 1.5 / uniform.num_values[flavour]
            max_change = 0.3
            weight_change = [min(d*factor, max_change) for d
                             in uniform.density_dif[flavour][u_index]]
            weight_change = [max(wc, -max_change) for wc in weight_change]
            max_weight = 15
            uniform.weights[flavour] =\
                [min(w + c, max_weight) for w, c
                 in zip(uniform.weights[flavour], weight_change)]
            # we force the weight to be positive also
            uniform.weights[flavour] =\
                [max(w, 0) for w in uniform.weights[flavour]]
    return all_done, total_density_difs


def continue_reweighing(total_density_difs, ave_density_tol, average_over=30):
    """A function to decide if the deletion process should stop

    Parameters
    ----------
    max_deletions : dictionary of ints
        the maximum number of deletions that each jet flavour can suffer
        if we go past this we should stop regardless of the regularization state
    deletions : dictionary of ints
        dictionary discribing how many jets have been deleted from each flavour
    total_density_difs : dictionary
        this is a dictionary of dictionarys containing lists.
        The top layer dictionary is all the flavours but the one matched to.
        The next layer dictionary is all the uniform vars by var name
        The list contains the density diference measured at each step.
        This can be used to judge how well this process worked.
    ave_density_tol : float
        if the average over the last few density diffrences is below this number
        we can stop
    average_over : int, optional
        number of previous density difs to consider for ave_density_tol (Default value = 30)

    Returns
    -------
    : boolean
        should the reweighing continue
    """
    for f_key in total_density_difs:
        num_tests = len(list(total_density_difs[f_key].values())[0])
        if num_tests < average_over:
            # not enough tests to check this
            return True
        net_dens_tol = ave_density_tol * average_over
        for u_key in total_density_difs[f_key]:
            if sum(total_density_difs[f_key][u_key][-average_over:])\
                    > net_dens_tol:
                return True
    print("The density history is under the required average.")
    return False


class Uniform:
    """ object to hold the discription of the factors effecting uniformity """
    def __init__(self, uniform_indices, match_flavour, inp_groups, data_dic):
        """ initilisation
        
        Parameters
        ---------- 
        uniform_indices : list of ints
            the indices of the variables tht are to be matched
        match_flavour : int
            the number assigned ot the flavour wholes distribution will remain static
        inp_groups : dictionary
            dictionary where the keys are flavour and each value is
            a 2D numpy array of data for the that flavour
        data_dic : dictionary
            dictionary where the keys are the names of the columns in the data
            the values are the indices of the columns
            this discribes the layout of the data in inp_groups values

        """
        self.match_flavour = match_flavour
        self.uniform_indices = uniform_indices
        # sometimes we can only handle a chunk at once
        # for memory reasons
        self.chunk_length = 500
        self.flavour_indices = list(inp_groups.keys())
        # get the values of the variables to be made uniform
        self.num_values = {flavour: 0 for flavour in self.flavour_indices}
        self.values = {key: {} for key in self.flavour_indices}
        self.all_values = {index: [] for index in self.uniform_indices}
        for flavour in self.flavour_indices:
            for index in self.uniform_indices:
                self.values[flavour][index] =\
                        inp_groups[flavour][:, index]
                for i, value in enumerate(self.values[flavour][index]):
                    self.all_values[index].append((flavour, i, value))
            self.num_values[flavour] =\
                len(self.values[flavour][uniform_indices[0]])
        # check is the input contains weights
        if 'weight' in data_dic:
            print("using existing weights")
            self.weights = {k: v[:, data_dic['weight']] for
                            k, v in inp_groups.items()}
        else:
            # in the begining all te weighs should be 1
            self.weights = {k: np.ones(v) for k, v in self.num_values.items()}
        # the match density is the density of the flavour we match against
        # measurerd at the points of all other flavours
        # it wont change
        self.match_density = {flavour: {} for flavour in self.flavour_indices}
        self.density = {key: {} for key in self.flavour_indices}
        for index in uniform_indices:
            match_kernal =\
                    gaussian_kde(self.values[match_flavour][index],
                                 weights=self.weights[match_flavour])
            for flavour in self.flavour_indices:
                if flavour == match_flavour:
                    # the match flavour does not require a match to itself
                    # it does need an ordinary density though
                    self.density[flavour][index] =\
                        np.zeros(self.num_values[flavour])
                    # this must be done n chunks the prevent memory error
                    num_chunks = int(self.num_values[flavour]
                                     / self.chunk_length)
                    for chunk in range(num_chunks):
                        c_start = self.chunk_length * chunk
                        c_end = c_start + self.chunk_length
                        self.density[flavour][index][c_start:c_end] =\
                            match_kernal(self.values[flavour][index][c_start:c_end])
                    end_chunk = (self.num_values[match_flavour]
                                 - num_chunks * self.chunk_length)
                    self.density[flavour][index][-end_chunk:] =\
                        match_kernal(self.values[flavour][index][-end_chunk:])
                    # self.density[flavour][index] =\
                    #     match_kernal(self.values[flavour][index])
                else:
                    self.match_density[flavour][index] =\
                            np.zeros(self.num_values[flavour])
                    # this must be done n chunks the prevent memory error
                    num_chunks = int(self.num_values[flavour]
                                     / self.chunk_length)
                    for chunk in range(num_chunks):
                        c_start = self.chunk_length * chunk
                        c_end = c_start + self.chunk_length
                        self.match_density[flavour][index][c_start:c_end] =\
                            match_kernal(self.values[flavour][index][c_start:c_end])
                    end_chunk = (self.num_values[match_flavour]
                                 - num_chunks * self.chunk_length)
                    self.match_density[flavour][index][-end_chunk:] =\
                        match_kernal(self.values[flavour][index][-end_chunk:])
                    # self.match_density[flavour][index] =\
                    #     match_kernal(self.values[flavour][index])
        # 'update'
        self.update()
    
    def update(self):
        """update the density distributions to reflect the current weights """
        self.density_dif = {key: {} for key in self.flavour_indices
                            if key != self.match_flavour}
        for index in self.uniform_indices:
            for flavour in self.flavour_indices:
                if flavour == self.match_flavour:
                    continue
                kernal = gaussian_kde(self.values[flavour][index],
                                      weights=self.weights[flavour])
                self.density[flavour][index] =\
                    np.zeros(self.num_values[flavour])
                # again, make chunks to prevent memory error TODO can do without?
                num_chunks = int(self.num_values[flavour]
                                 / self.chunk_length)
                for chunk in range(num_chunks):
                    c_start = self.chunk_length * chunk
                    c_end = c_start + self.chunk_length
                    self.density[flavour][index][c_start:c_end] =\
                        kernal(self.values[flavour][index][c_start:c_end])
                end_chunk = (self.num_values[flavour]
                             - num_chunks * self.chunk_length)
                self.density[flavour][index][-end_chunk:] =\
                    kernal(self.values[flavour][index][-end_chunk:])
                self.density_dif[flavour][index] =\
                    np.array([md-d for md, d in
                              zip(self.match_density[flavour][index],
                                  self.density[flavour][index])])


def split_data_by_target(target_dic, data_dic, data, target_name="target"):
    """sort of the opposite of shuffle inputs

    Parameters
    ----------
    target_dic : dictionary
        dictionary whos keys are the string names of the targets
        the values are the numbers that represent the target in the data
    data_dic : dictionary
        dictionary where the keys are the names of the columns in the data
        the values are the indices of the columns
    data : 2D numpy array
        array containing all variable, targets and discriminating variables
        the structure is given by data_dic
    target_name : string
        the name that the targt column is given in data_dic
         (Default value = "target")

    Returns
    -------
    data_groups : dictionary
        dictionary where the keys are targets and each value is
        a 2D numpy array of data for the that target

    """
    data_groups = {tar: [] for tar in target_dic.values()}
    target_index = data_dic[target_name]
    for jet in data:
        target = jet[target_index]
        data_groups[target].append(jet)
    data_groups = {tar: np.array(jets) for tar, jets in data_groups.items()
                   if len(jets) > 0}
    return data_groups


def balence_target_weights(target_dic, data_dic, data):
    """
    Balence the weights of each flavour so that the sum of
    all weights for each flavour is equal.

    Parameters
    ----------
    target_dic : dictionary
        dictionary whos keys are the string names of the targets
        the values are the numbers that represent the target in the data
    data_dic : dictionary
        dictionary where the keys are the names of the columns in the data
        the values are the indices of the columns
    data : 2D numpy array
        array containing all variable, targets and discriminating variables
        the structure is given by data_dic

    Returns
    -------
    data : 2D numpy array
        array containing all variable, targets and discriminating variables
        the structure is given by data_dic

    """
    data_groups = split_data_by_target(target_dic, data_dic, data)
    weight_index = data_dic['weight']
    sum_weight = {target: sum(f_data[:, weight_index]) for target, f_data
                  in data_groups.items()}
    match_flavour = target_dic['b']
    match_weight = sum_weight[match_flavour]
    for target in data_groups.keys():
        rescale = match_weight/sum_weight[target]
        data_groups[target][:, weight_index] *= rescale
    data = np.vstack(data_groups.values())
    return data


def reweigh_script(start_date, base_name, save_name, t_end):
    """
    A script that reads the data from file, runs the reweighing process
    then writes it back again

    Parameters
    ----------
    start_date : string
        the data given on the directory name to read from        
    base_name : string
        identifying component of the file names to read from
    save_name : string
        identifying component for the file names to write
    t_end : float
        when to tend the proccess to avoid hitting wall time

    """
    read_dir = "train_vertex_new_" + start_date
    # check the directory exists
    if not os.path.isdir("./" + read_dir):
        print("The directory {0} does not exist. Skipping.".format(read_dir))
        return 1
    print("Reading the data from the hdf5 in {0} with base name {1}.."
          .format(read_dir, base_name))
    target_dic, data_dic, data =\
        read_combined_data(read_dir, base_name)
    print("reweighing the vertices...")
    data_dic, data, density_difs =\
        reweigh_flavours(target_dic, data_dic, data, t_end)
    reweigh_dir = base_name + "_newnew_" + start_date
    print("Saving into directory called {0}...".format(reweigh_dir))
    try:
        mkdir(reweigh_dir)
    except OSError as e:
        if e.errno != errno.EEXIST:
            print("Some error when trying to make a folder for saving")
            return 1
    save_combined_data(reweigh_dir, base_name, target_dic,
                       data_dic, data)
    difs_file_name = 'difs_' + save_name + '_weights_' + start_date + '.csv' 
    with open(os.path.join(reweigh_dir, difs_file_name), 'a') as csv_file:
        csv_writer = csv.writer(csv_file)
        for dif in density_difs:
            csv_writer.writerow([float(dif)])
    print("Reweigh complete!")
    return 0


if __name__ == "__main__":
    er_code = 0
    start_t = time.time()
    # check the first argument exists
    if len(sys.argv) < 2:
        print("Please specify the date and time of the hdf5s to read.")
        sys.exit(1)
    else:
        start_date = sys.argv[1]
    if len(sys.argv) < 3:
        print("Please specify the base name")
        sys.exit(2)
    else:
        base_name = sys.argv[2]
    if len(sys.argv) < 4:
        print("Please specify the save_name")
        sys.exit(3)
    else:
        save_base = sys.argv[3]
    if len(sys.argv) < 5:
        print("Please give a run time in minutes")
        sys.exit(4)
    else:
        run_mins = int(sys.argv[4])
        end_t = start_t + run_mins * 60
        print("Time remaining = {0}".format(end_t - start_t))
    er_code = reweigh_script(start_date, base_name, save_base, end_t)
    sys.exit(er_code)

