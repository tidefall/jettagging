"""Utilities that may be needed in one or many data processing steps"""
from os import path
import copy

def expected_tags():
    meta_names = ["num jets per event", "num trks per event"]
    jets_names = ["parton flavour", "num eta-rel trks per jet",
                  "SV 2D flight dist sig", "num SVs",
                  "SV mass", "num trks from SV",
                  "SV energy ratio", "SV delta R",
                  "num trks per jet", "jet pt",
                  "jet eta", "summed trks Et ratio",
                  "summed delta R",
                  "first trk 2D IP sig above charm"
                  ]
    tracks_names = ["2D IP sig", "3D IP sig",
                    "trk relative pt", "trk delta R",
                    "trk pt ratio", "trk distance",
                    "trk decay length"
                    ]
    etaRel_names = ["trk eta rel"]
    return [meta_names, jets_names, tracks_names, etaRel_names]


def make_fileNames(tags_list, dir_name=""):
    """Make a list of the file names grouped by type.

    Parameters
    ----------
    tags_list : list of lists of strings
        A list of each set of tags.
    dir_name : string
        If the files should have a directory name in front to them it should be
        passed in here (Default value = "")

    Returns
    -------
    grouped_filenames : list of strings
        the full path to the files
    
    """
    grouped_fileNames = [[path.join(dir_name, name.replace(' ', '_') + ".csv")
                         for name in tags] for tags in tags_list]
    return grouped_fileNames


def unfile_name(file_names):
    """

    Parameters
    ----------
    file_names : list of strings
        the file names

    Returns
    -------
    names : list of strings
        pretty names

    """
    names = [name[:-4].replace('_', ' ') for name in file_names]
    return names

def get_all_input_dic():
    all_inputs = {"trk delta R 1": 12,
                   "num trks per jet": 19,
                   "trk decay length 1": 15,
                   "SV energy ratio": 5,
                   "trk relative pt 1": 11,
                   "trk distance 1": 14,
                   "SV delta R": 6,
                   "trk eta rel 1": 2,
                   "3D IP sig 1": 7,
                   "SV 2D flight dist sig": 0,
                   "3D IP sig 3": 9,
                   "3D IP sig 2": 8,
                   "3D IP sig 4": 10,
                   "trk pt ratio 1": 13,
                   "summed delta R": 17,
                   "first trk 2D IP sig above charm": 18,
                   "jet pt": 20,
                   "num trks from SV": 4,
                   "SV mass": 3,
                   "jet eta": 21,
                   "num SVs": 1,
                   "summed trks Et ratio": 16}
    return all_inputs


def get_var_keys():
    """A function to return the keys of the variables used in the paper"""
    var_list = [(1, "SV 2D flight dist sig"),
                (1, "num SVs"),
                (3, "trk eta rel"),
                (1, "SV mass"),
                (1, "num trks from SV"),
                (1, "SV energy ratio"),
                (1, "SV delta R"),
                (2, "3D IP sig"),
                (2, "trk relative pt"),
                (2, "trk delta R"),
                (2, "trk pt ratio"),
                (2, "trk distance"),
                (2, "trk decay length"),
                (1, "summed trks Et ratio"),
                (1, "summed delta R"),
                (1, "first trk 2D IP sig above charm"),
                (1, "num trks per jet"),
                (1, "jet pt"),
                (1, "jet eta")
                ]
    return var_list

def get_tar_key():
    return (1, "parton flavour")

def get_uniform_input_names():
    return ["jet pt", "jet eta"]


def get_2DIPsig_key():
    return (2, "2D IP sig")

def make_psudo_dic(input_dic):
    """

    Parameters
    ----------
    input_dic : dictionary
        a full dictionary of all variables

    Returns
    -------
    psudo_dic : dictonary
        dictionary contaning only the pesudo variables
    old_indices : list of ints
        the inices these varibles used to have in the input_dic

    """
    temp_dic = copy.deepcopy(input_dic)
    del temp_dic["SV 2D flight dist sig"]
    del temp_dic["SV energy ratio"]
    del temp_dic["num SVs"]
    # now the indices need reshuffling
    pairs = sorted(temp_dic.items(), key=lambda a: a[1])
    psudo_dic = {pair[0]: i for i, pair in enumerate(pairs)}
    old_indices = [pair[1] for pair in pairs]
    return psudo_dic, old_indices


def make_no_dic(input_dic):
    """

    Parameters
    ----------
    input_dic : dictionary
        a full dictionary of all variables

    Returns
    -------
    no_dic : dictonary
        dictionary contaning only the no variables
    old_indices : list of ints
        the inices these varibles used to have in the input_dic

    """
    temp_dic = copy.deepcopy(input_dic)
    del temp_dic["SV 2D flight dist sig"]
    del temp_dic["SV delta R"]
    del temp_dic["SV energy ratio"]
    del temp_dic["SV mass"]
    del temp_dic["num SVs"]
    del temp_dic["num trks from SV"]
    # now the indices need reshuffling
    pairs = sorted(temp_dic.items(), key=lambda a: a[1])
    no_dic = {pair[0]: i for i, pair in enumerate(pairs)}
    old_indices = [pair[1] for pair in pairs]
    return no_dic, old_indices

def plot_colour():
    plot_colour_dic = {
                       'b': '#7e3988',
                       'c': '#a0cf31',
                       'udsg': '#dc4735',
                       'base': '#4CB2D4',
                       0: '#7e3988',
                       1: '#a0cf31',
                       2: '#dc4735',
                       3: '#4CB2D4',
                       4: '#493997',
                       5: '#4b0656',
                       6: '#547013',
                       7: '#7f0d01',
                       8: '#4aa576',
                       'black': '#060607',
                       'grey': '#87837C'
                       }
    return plot_colour_dic
