''' script to read in a data file and launch a trainign session '''
import torch
import numpy as np
import sys
import csv


arg_num = 1
# all parameters are stored in a file
# the results will be appened to this file
# get the input filename
if len(sys.argv) < arg_num+1:
    print("Please give an input filename")
    sys.exit(1)
else:
    csv_file_name = sys.argv[arg_num]
    arg_num += 1


# read the parameters from the file
file_contents = []
with open(csv_file_name, 'r') as csvfile:
    reader = csv.reader(csvfile, delimiter=' ')
    for row in reader:
        file_contents.append(row)

# we only expect one row, and we expect it to have 5 items in it
assert len(file_contents) == 1, "{} dosn't have one row of data".format(csv_file_name)
n_args = 7
assert len(file_contents[0]) == n_args, "{} dosn't have {} variabels".format(csv_file_name, n_args) 
    
args = file_contents[0]
arg_num = 0
# first arg shoulld be an h5 name
h5_name = args[arg_num]
assert h5_name[-3:] == '.h5', "{} dosn't seem to be an .h5".format(h5_name)
arg_num += 1

# get desired run time in minutes
run_time = float(args[arg_num])  # this should prodice recognisable errors
arg_num += 1

# get batch size
batch_size = int(args[arg_num])
arg_num += 1

# decide if it's a GPU run
# process to remove spaces and convert to lower case
run_type = ''.join(args[arg_num].split()).lower()
if run_type == 'gpu':
    use_gpu = True
elif run_type == 'cpu':
    use_gpu = False
else:
    print("Error, receved {}, expecting 'cpu' or 'gpu'".format(run_type))
    sys.exit(1)
arg_num += 1

# get net type
net_type = ''.join(args[arg_num].split()).lower()
valid_nets = ['csvv2', 'deepcsv']
assert net_type in valid_nets, "net_type should be one of {}, found {}".format(valid_nets, net_type)
arg_num += 1

# get library to use 
library = ''.join(args[arg_num].split()).lower()
arg_num += 1

# import the correct net
if library == 'pytorch':
    from NN_pytorch import begin_training
elif library == 'keras':
    from NN_keras_test import begin_training
else:
    print("library fould be 'pytorch' or 'keras', don't recongnise {}".format(library))
    sys.exit(1)

# the machine running the test is the final argument


print("Found args {}".format(args))


device_name, total_ram, training_returns = begin_training(h5_name, run_time, batch_size, use_gpu, net_type)
net, time_stamps, training_loss, test_loss, mag_weights, learning_rates = training_returns
# save the net in a file of the same name
if library == 'pytorch':
    net_save_name = csv_file_name[:csv_file_name.rfind('.')] + ".torch"
    torch.save(net.state_dict(), net_save_name)
elif library =='keras':
    net_save_name = csv_file_name[:csv_file_name.rfind('.')] + ".keras"
    net.save(net_save_name)
else:
    print("No save protocol for this library")

# add the device name to the argument list
args.append(device_name)
# add the totla ram
args.append("Total_ram={}".format(total_ram))

# make a names of the columns 
col_names = ["time_stamps", "training_loss", "test_loss", "mag_weights", "learning_rates"]
# make a numpy array of the contents
contents = np.array([time_stamps, training_loss, test_loss, mag_weights, learning_rates]).T
# write it all to the csv file
with open(csv_file_name, 'w') as csvfile:
    writer = csv.writer(csvfile, delimiter=' ')
    writer.writerow(args)
    writer.writerow(col_names)
    for row in contents:
        writer.writerow(row)

print("File {} written".format(csv_file_name))


