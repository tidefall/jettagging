''' a Module to implement reweighting the jets'''
from data_readwrite import read_variables, variable, Indexer, indices_by_flavour
from ipdb import set_trace as st
from hep_ml.reweight import GBReweighter
import numpy as np
import h5py
import sys


def generate_weights(hdf5_fname):
    print("Obtaining file structure info")
    # discover the columns that the required values live at
    jet_vars = read_variables(hdf5_fname)
    flavour_v = variable('flavour', jet_vars)
    weight_i = variable('weight', jet_vars).indices[0]
    flavour_names = flavour_v.classes.values()
    # open the file to get at the data
    print("Opening file to read data")
    with h5py.File(hdf5_fname, 'a') as hf:
        hf['data'].attrs['weighted'] = True
        num_jets = hf['data'].shape[0]
        print("Total jets {}".format(num_jets))
        # split jet indices into signal and background distributions
        jet_flavours = hf['data'][:, flavour_v.indices[0]]
        # get teh background class name
        sg_bg_indices = indices_by_flavour(flavour_v, jet_flavours)
        # now find the columns of the variables that should be reweighted
        reweighted_names = ["jetPt", "jetEta"]
        reweighted_indices = sorted([v.indices[0] for v in jet_vars
                                     if v.short_name in reweighted_names])
        for name, ind in zip(reweighted_names, reweighted_indices):
            print("Reweighting {} at col {}".format(name, ind))
        reweight_cols_data = hf['data'][:, reweighted_indices]
        # get the data
        sg_bg_data = {f: reweight_cols_data[sg_bg_indices[f]] for f in flavour_names}
        reweighter = GBReweighter()
        # chose a distribution to match to
        match_flavour = 'b'
        print("Matching to flavour {}, data.shape = {}".format(match_flavour, sg_bg_data[match_flavour].shape))
        sys.stdout.flush()
        # array to hold tha calculated weights in 
        all_weights = np.full(num_jets, np.nan)
        for flavour in flavour_names:
            if flavour == match_flavour:
                # we dont need to match the distributio to itself
                weights = np.ones(len(sg_bg_indices[flavour]))
            else:
                print("Matching flavour {}, data.shape = {}".format(flavour, sg_bg_data[flavour].shape))
                reweighter.fit(original=sg_bg_data[flavour], target=sg_bg_data[match_flavour])
                weights = reweighter.predict_weights(sg_bg_data[flavour])
            print("Weights found, ave = {}, std = {}".format(np.mean(weights), np.std(weights)))
            sys.stdout.flush()
            all_weights[sg_bg_indices[flavour]] = weights
        hf['data'][:, weight_i] = all_weights
    print("Done")


def class_ballence(h5_name, target_classes=['b', 'c']):
    print("Obtaining file structure info")
    # discover the columns that the required values live at
    jet_vars = read_variables(h5_name)
    flavour_v = variable('flavour', jet_vars)
    weight_i = variable('weight', jet_vars).indices[0]
    # open the given hdf5 file
    with h5py.File(h5_name, 'a') as hf:
        # check that weights have been assigned
        assert hf['data'].attrs["weighted"], "error, use generate_weights() first"
        print("Obtaingin jet flavours")
        jet_flavours = hf['data'][:, flavour_v.indices[0]]
        sg_bg_indices = indices_by_flavour(flavour_v, jet_flavours, target_classes)
        weights = hf['data'][:, weight_i]
        sg_bg_weight_sum = {k: np.sum(weights[v]) for k, v in sg_bg_indices.items()}
        # now, it is not desirable to divide by too large a number
        # that would result in rounding error
        # so we rescale by the average weight sum
        print("calculating rescale")
        ave_weight_sum = np.mean(list(sg_bg_weight_sum.values()))
        # now check this needs doing
        all_the_same = True
        alpha = ave_weight_sum/1000.
        for key in sg_bg_weight_sum:
            if np.abs(sg_bg_weight_sum[key] - ave_weight_sum) > alpha:
                all_the_same = False
                break
        if all_the_same:
            print("The flavours are already balanced; {}".format(sg_bg_weight_sum))
            print("Done")
            return
        else:
            print("Starting weight distribution is {}".format(sg_bg_weight_sum))
        sg_bg_rescaling = {k: ave_weight_sum/v for k, v in sg_bg_weight_sum.items()}
        # now multiply the right indices by this factor
        for key in sg_bg_indices:
            weights[sg_bg_indices[key]] *= sg_bg_rescaling[key]
        # put it back in the file
        sg_bg_weight_sum = {k: np.sum(weights[v]) for k, v in sg_bg_indices.items()}
        print("Now weight distribution is {}".format(sg_bg_weight_sum))
        hf['data'][:, weight_i] = weights
    print("Done")



        

        
