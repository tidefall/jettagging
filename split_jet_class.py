''' module to split the jets into classes'''
from data_readwrite import read_variables, variable
from ipdb import set_trace as st
import numpy as np
import h5py
import sys


def assign_class(hdf5_fname):
    print("Obtaining file structure info")
    # discover the columns that the required values live at
    jet_vars = read_variables(hdf5_fname)
    class_i = variable('jetClass', jet_vars).indices[0]
    num_SVs_i = variable('nSVs', jet_vars).indices[0]
    # num_tracks_i = variable('nTrks', jet_vars).indices[0]
    vertex_mass_i = variable('vertexMass', jet_vars).indices[0]
    ip2dsig_is = variable('IP2DSig', jet_vars).indices
    # assignment dictionary
    class_dict = variable('jetClass', jet_vars).classes
    a_dict = {v: k for k, v in class_dict.items()}
    # som deciding variables
    mass_KShort = 497.614
    required_mass_dist = 50.
    required_sip = 2
    print("Begginign calss assignment")
    n_reco = 0
    n_psudo = 0
    n_no = 0
    # now open the file and go through each jet, deciding
    with h5py.File(hdf5_fname, 'a') as hf:
        num_jets = hf['data'].shape[0]
        for jet_n in range(num_jets):
            this_jet = np.nan
            # first check if there is a SV
            if hf['data'][jet_n, num_SVs_i] > 0:
                n_reco += 1
                # if there is a SV this jet is a reco jet
                this_jet = a_dict['reco jet']
            # now if there are at least 2 tracks who's SV Sip is above 2
            # and tey have a combind invariant mass at least 50 mev from kShort
            elif(np.nansum(hf['data'][jet_n, ip2dsig_is]) > required_sip and
                    abs(hf['data'][jet_n, vertex_mass_i] - mass_KShort) > required_mass_dist):
                n_psudo += 1
                # its a psudo jet
                this_jet = a_dict['psudo jet']
            else:
                n_no += 1
                this_jet = a_dict['no jet']
            hf['data'][jet_n, class_i] = this_jet
            if jet_n % 100 == 0:
                print('.', end='')
                sys.stdout.flush()
    print("Finished assigning jet classes, reco ={}, psudo = {}, no = {}".format(n_reco, n_psudo, n_no))



    
    
