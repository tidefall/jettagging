''' A module to calculate the time for a net to converge base on loss plots '''
from ipdb import set_trace as st
from matplotlib.lines import Line2D
import numpy as np
import operator
from output_read import get_flag, Run_recording, filter_runs, split_list,\
                        sort_catigory_names
from quick_plots import colour
import matplotlib.pyplot as plt
from matplotlib.transforms import blended_transform_factory
from sys import argv
from evaluate_training import generate_legend
import sys
from scipy.stats import spearmanr, kendalltau, pearsonr


def rank_diff(x, y):
    x_rank = np.array(x).argsort().argsort()
    y_rank = np.array(y).argsort().argsort()
    diff = x_rank - y_rank
    return diff


def ave_diff(times, points):
    diffs = rank_diff(times, points)
    num_behind = np.arange(len(times))
    backward = np.cumsum(diffs)/num_behind
    num_ahead = np.flip(num_behind, axis=0)
    forward = np.flip(np.cumsum(np.flip(diffs, axis=0)), axis=0)/num_ahead
    return backward, forward
    
    
def diff_convergance_fun(fig, ax, c, times, points, name):
    forward, backward = ave_diff(times, points)
    plot_convergance(fig, ax[1], times, forward, c, name)
    plot_convergance(fig, ax[1], times, backward, c, name)
    return 10


def std_convergance_function(fig, ax, c, times, points, name):
    tail_len = 10
    std_change = [np.std(points[point:])/np.std(points[:point])
                  for point in range(len(points)-tail_len)]
    plot_convergance(fig, ax[1], times[:-tail_len], std_change, c, name)
    min_i = np.nanargmin(std_change)
    if min_i == len(points) - tail_len - 1 or min_i == 0:
        min_i = -1
    return min_i


def calculate_spearman_ratio(x, y):
    N = len(x) - 2
    sp_ratio = [0.]
    sp_p_vals = [0.]
    for point in range(2, N):
        rho_forwards, p_val_forwards = spearmanr(x[point:], y[point:])
        rho_backwards, p_val_backwards = spearmanr(x[:point], y[:point])
        ratio = abs(rho_backwards/rho_forwards)
        sp_ratio.append(ratio)
        p_val = p_val_backwards + p_val_forwards - p_val_backwards * p_val_forwards
        sp_p_vals.append(p_val)
    return sp_ratio, sp_p_vals


def first_peak(points):
    # begin by making th points start at zero
    points = np.array(points)
    points = points - min(points)
    # consideres to have found a peak if it has decended
    # and at least 0.3 of the higest_point
    highest_point = 0.
    highest_i = 0
    required_decent = 1. - 0.3
    for i in range(1, len(points)):
        if points[i] > highest_point:
            highest_i = i
            highest_point = max(points[i], highest_point)
        elif points[i] < highest_point*required_decent:
            return highest_i
    # if we didn't find one return -1
    return -1


def descent_ratios(points):
    positions = range(1, len(points)-1)
    forward_disp = np.array([0] + [max(abs(points[i] - min(points[i:])), abs(points[i] - max(points[i:]))) for i in positions] + [0])
    backwards_disp = np.array([0] + [max(abs(points[i] - min(points[:i])), abs(points[i] - max(points[:i]))) for i in positions] + [0])
    ratio = backwards_disp/forward_disp
    return ratio


def descent_convergance_fun(fig, ax, c, times, points, name):
    ratios = descent_ratios(points)
    peak_i = first_peak(ratios)
    plot_convergance(fig, ax[1], times, ratios, c, name)
    return peak_i


def plot_convergance(fig, ax, times, ratios, c, line_label):
    ratio_label = line_label + " convergance"
    times = times[:len(ratios)]
    ax.plot(times, ratios, color=c, alpha=0.7, ls=':', label=ratio_label)


def spearman_convergance_fun(fig, ax, c, times, points, name):
    ratios, p_values = calculate_spearman_ratio(times, points)
    peak_i = first_peak(ratios)
    plot_convergance(fig, ax[1], times, ratios, c, name)
    return peak_i


def make_graph(split_dict, convergance_function, convergance_name, testTrain='train', settings_names=None):
    fig, ax = plt.subplots(2, 1, sharex=True)
    ax[0].set_title(input("Give a title "))
    ax[0].set_ylabel("{} loss".format(testTrain))
    ax[1].set_ylabel(convergance_name)
    ax[1].set_xlabel("time from first epoch")
    plt.subplots_adjust(hspace=.0)
    colours = [colour(c) for c in np.linspace(0.1, 0.9, len(split_dict))]
    y_max = 0.
    y_min = 1.
    x_max = np.inf
    label_height=0.95
    if settings_names:
        ordered_cat_names = sort_catigory_names(list(split_dict.keys()))
    else:
        ordered_cat_names = sorted(split_dict.keys())
    for name, c in zip(ordered_cat_names, colours):
        print("plotting {}".format(name))
        runs = split_dict[name]
        if testTrain == 'train':
            col_num = 1
        elif testTrain == 'test':
            col_num = 2
        convergance_times = []
        convergance_epochs = []
        for run in runs:
            times = run[:, 0]
            times -= np.min(times)  # start at t=0
            x_max = min(times[-1], x_max)
            points = run[:, col_num]
            ax[0].plot(times, points, color=c)
            i_converge = convergance_function(fig, ax, c, times, points, str(run))
            convergance_epochs.append(i_converge)
            if i_converge < 0:
                print("Error, no convergance point found for {}".format(run))
            else:
                convergance_times.append(times[i_converge])
        y_max = max(y_max, *points[2:])
        y_min = min(y_min, *points)
        # now plot the mean convergance point
        mean_converg = np.mean(convergance_times)
        convergance_epoch = np.mean(convergance_epochs)
        print("The convergance mean of {} is {} in epoch {}".format(name, mean_converg, convergance_epoch))
        std_converg = np.std(convergance_times, ddof=1)/np.sqrt(len(convergance_times))
        print("The convergance pop devation of {} is {}".format(name, std_converg))
        ax[1].axvline(mean_converg, 0., 1.,  color=c)
        ax[1].axvspan(mean_converg - std_converg, mean_converg + std_converg, 0., 1.,
                      color=c, alpha=0.2)
        trans = blended_transform_factory(ax[1].transData, ax[1].transAxes)
        text_c = tuple(min(c_part*0.8, 1.) for c_part in c[:3])
        text_c = (*text_c, 1.)
        ax[1].text(mean_converg - 18, label_height,  
                   r"$\mu = {:6.2f}$"
                   .format(mean_converg),
                   color=text_c, transform=trans, rotation=90,
                   bbox=dict(facecolor=(1., 1., 1., 0.0),
                             edgecolor=(0., 0., 0., 0.),
                             boxstyle='round'))
        ax[1].text(mean_converg + 5, label_height,  
                   r"$\sigma = {:6.2f}$"
                   .format(std_converg),
                   color=text_c, transform=trans, rotation=90,
                   bbox=dict(facecolor=(1., 1., 1., 0.0),
                             edgecolor=(0., 0., 0., 0.),
                             boxstyle='round'))
        # label_height -= 0.05  # shuffle next label down
    if settings_names:
        legend_fill = generate_legend(ordered_cat_names, settings_names, colours)
    else:
        legend_lines = [Line2D([0], [0], color=c) for c in colours]
        legend_fill = [legend_lines, split_dict.keys()]
    ax[1].legend(*legend_fill)
    ax[1].set_xlim(0., x_max)
    ax[1].set_ylim(top=ax[1].get_ylim()[1]*1.3)
    ax[0].set_ylim(y_min, y_max)
    # ax[1].set_ylim(-1, 7)
    plt.show()


def main():
    flags = ['-m', '-r', '-c', '--end', '--test']
    if len(argv) == 1:
        print("Flags are {}".format(flags))
        print("-m is mode, should be all or split")
        print("-r is runs, a bundel of file paths containging the desired runs")
        print("-c is the convergance function, should be 'spear' or 'diff' or 'std' ok 'des'")
        print("--end is the end point for convergance plotting")
        print("--test add 'y' to test to used test loss")
        sys.exit(0)

    mode = get_flag('-m', flags)
    if len(mode) == 0:
        mode = 'all'
    else:
        mode = mode[0]
    testTrain = get_flag('--test', flags)
    if len(testTrain) == 0:
        testTrain = 'train'
    elif 'y' in testTrain[0]:
        testTrain = 'test'
    else:
        testTrain = 'train'
    convergance = get_flag('-c', flags)[0]
    if 'spear' in convergance:
        convergance_fun = spearman_convergance_fun
        convergance_name = "Spearman ratio"
    elif 'diff' in convergance:
        convergance_fun = diff_convergance_fun
        convergance_name = "rank difference ratio"
    elif 'std' in convergance:
        convergance_fun = std_convergance_function
        convergance_name = "standard devation ratio"
    elif 'des' in convergance:
        convergance_fun = descent_convergance_fun
        convergance_name = "descent ratio"
    run_names = get_flag('-r', flags)
    assert len(run_names) > 0, 'no runs given'
    run_list = filter_runs(run_names)
    if mode == 'split':
        split_dict, settings_names = split_list(run_list)
    elif mode == 'all':
        split_dict = {str(run): [run] for run in run_list}
        settings_names = None
    else:
        print("Error, expected mode 'all' or 'split' but got {}".format(mode))
    make_graph(split_dict, convergance_fun, convergance_name, testTrain, settings_names=settings_names)


if __name__ == '__main__':
    main()
