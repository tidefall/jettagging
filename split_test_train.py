''' module to set some jets to the test set and some to the train set '''
import h5py
from data_readwrite import read_variables, variable
import numpy as np


def assign_jets(h5_name, percent_test=0.2):
    print("retreving variables")
    jet_vars = read_variables(h5_name)
    tt_var = variable("testTrain", jet_vars)
    inverse_tt_dic = {v: k for k, v in tt_var.classes.items()}
    print("opening file")
    with h5py.File(h5_name, 'a') as hf:
        first_val = hf['data'][0, tt_var.indices[0]]
        # double check it hasn't been run already
        if not np.isnan(first_val):
            print("It apears this sample has already been assigned.")
            print("I see value {} in the first jet".format(first_val))
            decision = input("Do you want to reasign? ('yes' to reasign)")
            if decision.strip().lower() == 'yes':
                print("Ok reasigning")
            else:
                print("Leaving the current assignments")
                return
        num_jets = hf['data'].shape[0]
        num_test = int(num_jets * percent_test)
        jet_indices = np.arange(num_jets)
        np.random.shuffle(jet_indices)
        test_indices = jet_indices[:num_test]
        jet_tt = []
        print("working through data")
        for i, jet in enumerate(hf['data'][:]):
            if i in test_indices:
                jet_tt.append(inverse_tt_dic['test'])
            else:
                jet_tt.append(inverse_tt_dic['train'])
            if i % 100 == 0:
                print('.', end='')
        hf['data'][:, tt_var.indices[0]] = np.array(jet_tt)
    print("Done")
