''' Normalisation is a destructive process, so a normalised copy of the data is created'''
import numpy as np
import h5py
from data_readwrite import read_variables, setup_data_file, variable
from os.path import split, join

def norm_copy(old_name):
    # make a new name for the copy
    split_path = list(split(old_name))
    split_path[-1] = "normed_" + split_path[-1]
    copy_name = join(*split_path)
    print("Setting up copy file {}".format(copy_name))
    # get the vars and set up the file
    jet_vars = read_variables(old_name)
    setup_data_file(copy_name, jet_vars)
    with h5py.File(old_name, 'r') as old, h5py.File(copy_name, 'a') as cop:
        cop['data'].attrs['normed'] = True
        cop['data'].attrs['weighted'] = old['data'].attrs['weighted']
        # put the rigth number of rows and cols into the new dataframe
        dims = old['data'].shape
        cop['data'].resize(dims)
        cop['data'].attrs['n jets'] = dims[0]
        # go over each column
        print("Itterting over {} columns".format(dims[1]))
        for col_i in range(dims[1]):
            if col_i % 40 == 0:
                print(col_i)
            var = variable(col_i, jet_vars)
            if var.classes is not None:
                # then this is a symboic variable
                print("Direct copying {} because catigorical".format(col_i))
                cop['data'][:, col_i] = old['data'][:, col_i]
                continue
            if var.truth:
                # then thsi willnever be input
                print("Direct copying {} because truth".format(col_i))
                cop['data'][:, col_i] = old['data'][:, col_i]
                continue
            # if we reached this point the column here should eb normalised
            col_data = old['data'][:, col_i]
            col_mean = np.nanmean(col_data)
            col_std = np.nanstd(col_data)
            if col_std == 0:
                col_data = col_data - col_mean
            else:
                col_data = (col_data - col_mean)/col_std
            cop['data'][:, col_i] = col_data


