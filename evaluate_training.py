''' a module to evaluate the training of the neural net graphically'''
import csv
from ipdb import set_trace as st
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D  # for legends
from quick_plots import colour
from os.path import split
from scipy.optimize import curve_fit
from sys import argv
import sys
from output_read import get_flag, Run_recording, filter_runs, split_list,\
                        sort_catigory_names


def linear_fit_function(xs, initial_gradient, plateau_time, final_height):
    initial_slope = final_height -\
        initial_gradient * (plateau_time - xs[xs < plateau_time])
    plateau = np.full_like(xs[xs >= plateau_time], final_height)
    line = np.hstack((initial_slope, plateau))
    return line


def linear_initial_guess(times, points):
    final_height = np.mean(points[-10:])
    epsilon = (points[0] - final_height)/5
    plateau_i = 0
    while (points[plateau_i] - final_height > epsilon
           and len(points) > plateau_i):
        plateau_i += 1
    plateau_time = times[plateau_i]
    inital_gradient = (points[plateau_i] - points[0])/plateau_time
    p0 = [inital_gradient, plateau_time, final_height]
    return p0


def quadratic_fit_function(xs, initial_quad_coef, initial_lin_coef, plateau_time, final_height):
    curve_xs = plateau_time - xs[xs <= plateau_time]
    initial_curve = final_height +\
        initial_lin_coef * curve_xs +\
        initial_quad_coef * np.power(curve_xs, 2)
    plateau = np.full_like(xs[xs > plateau_time], final_height)
    line = np.hstack((initial_curve, plateau))
    assert len(line) == len(xs),\
        "Have {} xs but fitted {} points {}".format(len(xs), len(line))
    return line


def quadratic_initial_guess(times, points):
    final_height = np.mean(points[-10:])
    epsilon = (points[0] - final_height)/5
    plateau_i = 0
    while (points[plateau_i] - final_height > epsilon
           and len(points) > plateau_i):
        plateau_i += 1
    plateau_time = times[plateau_i]
    inital_lin_coef = (points[plateau_i] - points[0])/plateau_time
    inital_quad_coef = 0.1
    p0 = [inital_lin_coef, inital_quad_coef, plateau_time, final_height]
    return p0


def error_fit_function(xs, start, gain_magnitude, curvyness, speed):
    return start - gain_magnitude/(1. + np.exp(curvyness - speed*xs))


def error_initial_guess(times, points):
    guess_gain = points[-1] - points[0]
    guess_curve = 2.
    guess_start = points[0] + guess_gain/(1 + np.exp(guess_curve))
    guess_speed = 2./times[-1]
    if np.isinf(guess_speed):
        print("Warning, relativity violations :P")
        guess_speed = 1.
    p0 = [guess_start, guess_gain, guess_curve, guess_speed]
    return p0


def fit_group(ax, points, times, fit_colour, label, fit_function, p0):
    print("fitting {}".format(label))
    print("Initial guesses {}".format(p0))
    # do the fit
    try:
        popt, pcov = curve_fit(fit_function, times, points, p0)
        print("parameters fitted {}".format(popt))
        fitted_points = fit_function(times, *popt)
    except RuntimeError as err:
        print("Fit failed for {}".format(label))
        print("Captured error:{}".format(err))
        fitted_points = np.zeros_like(times)
    ax.plot(times, fitted_points, color=fit_colour, label=label)
    

def generalisation_progress(file_names):
    columns = ['test_loss']
    def function(t, x):
        return x
    plots(file_names, function, '', columns, quadratic_fit_function, quadratic_initial_guess)


def loss_progress(file_names):
    columns = ['test_loss', 'training_loss']
    def function(t, x):
        return x
    plots(file_names, function, '', columns, quadratic_fit_function, quadratic_initial_guess)


def derivative_plot(file_names, columns=None):
    def function(tim, pos):
        return np.gradient(pos, tim, axis=0)
    func_name = 'd/dt'
    plots(file_names, function, func_name, columns)


def cumulative_plot(file_names, columns=None):
    def function(tim, pos):
        return np.cumsum(pos, axis=0)
    func_name = 'cumulative'
    plots(file_names, function, func_name, columns)


def generate_legend(sorted_catigory_names, settings_names, colours):
    indent = "   "
    lines = []
    labels = []
    last_setting = [None for _ in settings_names]
    # if there is only one setting then we start at the lowest layer
    new_lowest_layer = len(settings_names) == 1
    for c, catigory_name in enumerate(sorted_catigory_names):
        # the legened is aranged in layers
        # each layer indicates a new catigory
        for layer in range(len(settings_names)-1):
            if last_setting[layer] == catigory_name[layer]:
                # we are already in the corect catigory
                # on this layer
                continue
            # if we ever reach this point we are building a new subcatigory
            new_lowest_layer = True
            # add an empty legend entry with the catigory name
            indent_here = indent * layer
            lines.append(Line2D([0], [0], alpha=0.))
            label_here = "{}{} is {}".format(indent_here,
                                             settings_names[layer],
                                             catigory_name[layer])
            labels.append(label_here)
            last_setting[layer] = catigory_name[layer]
        # now we have reached the corect catigory
        # it will be on the lowest layer
        indent_here = indent * (len(settings_names) - 1)
        if new_lowest_layer:
            lines.append(Line2D([0], [0], alpha=0.))
            label_here = "{}{}".format(indent_here, settings_names[-1])
            labels.append(label_here)
            new_lowest_layer = False
        # now finally put the actual line in
        labels.append("{}{}".format(indent_here, catigory_name[-1]))
        lines.append(Line2D([0], [0], color=colours[c]))
    return lines, labels


def plots(file_names, function=lambda t, x: x, func_name='', columns=None, fit_function=None, initial_guess_function=None):
    list_runs = filter_runs(file_names)
    print("Found {} runs".format(len(list_runs)))
    split_run_dict, settings_names = split_list(list_runs)
    print("Divided into {} catigories".format(len(split_run_dict)))
    if columns is None:
        example_run = list(split_run_dict.values())[0][0]
        columns = example_run.column_headings.copy()
        columns.remove('time_stamps')
    # make the right number of graphs
    fig, araxes = plt.subplots(len(columns), 1, sharex=True)
    if not hasattr(araxes, '__iter__'):
        araxes = [araxes]
    y_mins = [0 for _ in araxes]
    y_maxs = [0 for _ in araxes]
    x_max = 1000000
    # set up the colours
    # if we are plotting a fit function make all the other points pale
    if fit_function:
        alpha = 0.3
    else:
        alpha = 0.8
    colours = [colour(c, 1.) for c in np.linspace(0., 1., len(split_run_dict))]
    # put the catigories into a nice order
    ordered_cat_names = sort_catigory_names(list(split_run_dict.keys()))
    # loop over the catigories
    for catigory_name, cat_colour in zip(ordered_cat_names, colours):
        # get the relevant runs
        run_recordings = split_run_dict[catigory_name]
        # calculate derivatives
        timings = [rec.set_columns(['time_stamps']) for rec in run_recordings]
        # mak the end of the first epoch time = 0
        timings = [(tim - tim[0]).flatten() for tim in timings]
        positions = [rec.set_columns(columns) for rec in run_recordings]
        # if the traingin loss is used multipy it by the batch size
        if 'training_loss' in columns:
            i_tr = columns.index('training_loss')
            for r, rec in enumerate(run_recordings):
                positions[r][:, i_tr] *= rec.settings["batch_size"]
        line = [function(tim, pos) for pos, tim in zip(positions, timings)]
        # chop off the first point, because it makes an odd scale
        cut_points = 1
        for a, ax in enumerate(araxes):
            ax.set_xlabel("Time from first epoch (s)")
            ax.set_ylabel("{} {}".format(func_name, columns[a]))
            all_xy = []
            for r_index, run in enumerate(run_recordings):
                # the run will print as the label
                label = "{}".format(run)
                # and plot
                ax.plot(timings[r_index], line[r_index][:, a],
                        label=label, color=cat_colour, alpha=alpha)
                all_xy += list(zip(timings[r_index], line[r_index][:, a]))
                y_mins[a] = min(min(line[r_index][cut_points:, a]), y_mins[a])
                y_maxs[a] = max(max(line[r_index][cut_points:, a])*1.1, y_maxs[a])
                x_max = min(max(timings[r_index]), x_max)
            ax.set_ylim(y_mins[a], y_maxs[a])
            ax.set_xlim(0, x_max)
            if fit_function:
                # sort the points to time order
                all_xy = np.array(sorted(all_xy, key=lambda t: t[0]))
                p0 = initial_guess_function(all_xy[:, 0], all_xy[:, 1])
                # now working on the average line
                fit_group(ax, all_xy[:, 1], all_xy[:, 0],
                          cat_colour, catigory_name, fit_function, p0)
    legend_lines, legend_labels = generate_legend(ordered_cat_names, settings_names, colours)
    araxes[-1].legend(legend_lines, legend_labels)
    araxes[0].set_title(input("Give a title "))
    plt.subplots_adjust(hspace=.0)
    plt.show()



def main():
    flags = ['-m', '-r']
    if len(argv) == 1:
        print("Flags are {}".format(flags))
        print("-m is mode, should be prog, deriv, sum, fitgen, fitloss")
        print("-r is runs, a bundel of file paths containging the desired runs")
        sys.exit(0)

    mode = get_flag('-m', flags)
    assert len(mode) == 1, 'no valid mode given'
    run_names = get_flag('-r', flags)
    assert len(run_names) > 0, 'no runs given'
    if mode[0] == 'prog':
        plots(run_names)
    elif mode[0] == 'deriv':
        derivative_plot(run_names)
    elif mode[0] == 'sum':
        cumulative_plot(run_names)
    elif mode[0] == 'fitgen':
        generalisation_progress(run_names)
    elif mode[0] == 'fitloss':
        loss_progress(run_names)
    else:
        print('mode name {} not valid, should be "prog", "fitgen", "fitloss", "sum" or "deriv"')


if __name__ == '__main__':
    main()
