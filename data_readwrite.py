''' A module to deal with reading, writing and transforming the data'''

import numpy as np
from torch.utils.data import Dataset
import h5py
from ipdb import set_trace as st
import inspect
from itertools import islice
import sys

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Contents of the data files
# 1. A numpy array, the centeral array, containing some referance to all items
#    that apply to indervidual jets
# 2. A list of long pretty names of the central array
# 3. A list of short pretty names of the central array
# 4. For each column of the central array that is an enumerated class
#    a decoding list that gives short pretty names for each catigory
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

MAX_COLUMNS = 2000
INITIAL_ROWS = 0


def setup_data_file(file_name, jet_vars):
    ''' Create empty data file with variable info in it Parameters ----------
    file_name : string
        name of file to write
        
    jet_vars : list of JetVariables
        vaiables to put in file
        
    '''
    with h5py.File(file_name, 'w') as f_root:
        num_vars = len(jet_vars)
        # Create the centeral dataset
        data = f_root.create_dataset('data', (INITIAL_ROWS, num_vars),
                                     chunks=True, maxshape=(None, MAX_COLUMNS),
                                     compression='gzip', fillvalue=np.nan) 
        # create the relevant attributes
        data.attrs['normed'] = False
        data.attrs['weighted'] = False
        data.attrs['n jets'] = 0
    write_variables(file_name, jet_vars)
    return
       

def write_variables(file_name, jet_vars):
    '''
    Overwrite the vairables in a file with the given varaible set

    Parameters
    ----------
    file_name : string
        file naem to alter
        
    jet_vars : list of JetVariables
        vaiables to put in file

    '''
    with h5py.File(file_name, 'a') as f_root:
        # delete any existing tree
        if "variables" in f_root:
            del f_root["variables"]

        variables_root = f_root.create_group("variables")
        
        for var in jet_vars:
            v_group = variables_root.create_group(var.short_name)
            attr_props, arr_props = var.get_properties()
            for name, prop in attr_props:
                v_group.attrs[name] = prop
            for name, prop in arr_props:
                v_group.create_dataset(name, data=prop)


def read_variables(file_name):
    '''
    Reads the variables from a hdf5 file   

    Parameters
    ----------
    file_name : string
        name of file to read from
        

    Returns
    -------
    jet_vars : list of JetVariables
        vaiables to put in file
    '''
    var_data_names = set(("indices", "classes"))
    jet_vars = []
    with h5py.File(file_name, 'r') as f_root:
        v_branch = f_root['variables']
        for group_name in v_branch:
            branch = v_branch[group_name]
            assert set(branch.keys()).issubset(var_data_names),\
                ("Branch {} has variable attributes but contains too much"
                 .format(branch.name))
            attr_props = []
            arr_props = []
            for attr_name in branch.attrs:
                attr_props.append((attr_name, branch.attrs[attr_name]))
            for arr_name in branch:
                arr_props.append((arr_name, np.array(branch[arr_name])))
            jet_vars.append(JetVariable.from_props(attr_props, arr_props))
        return jet_vars


# need t implement another constrctor to read frm properties
class JetVariable:
    ''' object representing the nature of a variable '''
    def __init__(self, short_name, long_name, root_name, indices, truth=False,
                 classes=None, unphysical=None):
        self.short_name = short_name
        self.long_name = long_name
        self.root_name = root_name
        # check indices is a list, else convert
        if isinstance(indices, list):
            self.indices = indices
        else:
            self.indices = [indices]
        self.truth = truth
        self.unphysical = unphysical
        self.classes = classes

    # to save it to a file
    def get_properties(self):
        ''' return the stored information in a form that can be put in an hdf5'''
        attr_props = [("short_name", np.string_(self.short_name)),
                      ("long_name", np.string_(self.long_name)),
                      ("root_name", np.string_(self.root_name)),
                      ("truth", int(self.truth))]
        if self.unphysical is not None:
            attr_props.append(("unphysical", self.unphysical))
        arr_props = [("indices", np.array(self.indices))]
        if self.classes is not None:
            classes_as_arr = np.array([[str(k), str(v)] for k, v in
                                       self.classes.items()],
                                      dtype=np.dtype('S10'))
            arr_props.append(("classes", classes_as_arr))
        return attr_props, arr_props

    @classmethod
    def from_props(cls, attr_props, arr_props):
        '''
        Create a jet variable from the properties of a jet variable, as stored in an hdf5
        

        Parameters
        ----------
        attr_props : list of tuples
            property names and
            properties that will be stored as attributes in the hdf5
            
        arr_props : list of tuples
            property names and
            properties that will be stored as arrays in the hdf5
            

        Returns
        -------
        : JetVariable
            the jet variable that has been reconstructed from these properties

        '''
        # we can pull the default parameters from teh class signature
        # technically we could pull everythign from the class sig,
        # but readablility.
        p_sig = inspect.signature(JetVariable).parameters
        # attribute names
        params = []
        params.append(next(p[1].decode('UTF-8') for p in attr_props
                           if p[0] == "short_name"))
        params.append(next(p[1].decode('UTF-8') for p in attr_props
                           if p[0] == "long_name"))
        params.append(next(p[1].decode('UTF-8') for p in attr_props
                           if p[0] == "root_name"))
        params.append(next(list(p[1]) for p in arr_props
                           if p[0] == "indices"))
        # technically truth has a default, but it should always be saved
        params.append(next(bool(p[1]) for p in attr_props
                           if p[0] == "truth"))
        # classes does have a defalut and will often not be found
        classes = next((p[1] for p in arr_props
                        if p[0] == "classes"),
                       p_sig["classes"].default)
        if classes is not None:
            classes = {(int(row[0]) if row[0].isdigit()
                        else row[0].decode('UTF-8')): row[1].decode('UTF-8')
                       for row in classes}
        params.append(classes)
        # unphysical does have a defalut and will often not be found
        params.append(next((p[1] for p in attr_props
                            if p[0] == "unphysical"),
                           p_sig["unphysical"].default))
        return cls(*params)


def gen_jet_vars():
    ''' 
    Generate the defualt list of jet variables

    Returns
    -------
    jet_vars : list of JetVariables
        vaiables to put in file
        
    var_tree : dictionary tree with numpy arrays of ints at the bottom
        logical structure of the jet varibles

    '''
    # setup
    jet_vars = []
    index = 0
    # add variabels

    jet_classes = {0: 'reco jet', 1: 'psudo jet',
                   2: 'no jet', 'other': 'unclassified'}
    var = JetVariable("jetClass", "jet class", None, index, True, jet_classes)
    jet_vars.append(var)
    index += 1

    var = JetVariable("weight", "weight", None, index, True)
    jet_vars.append(var)
    index += 1
    
    test_train_classes = {0: 'test', 1: 'train'}
    var = JetVariable("testTrain", "test jet or train jet", None, index,
                      True, test_train_classes)
    jet_vars.append(var)
    index += 1

    var = JetVariable("n_inter", "number interpolated_values", None, index, True)
    jet_vars.append(var)
    index += 1

    var = JetVariable("nJet", "num jets per event", "nJet", index)
    jet_vars.append(var)
    index += 1

    flavour_classes = {4: 'c', 5: 'b', 'other': 'udsg'}
    var = JetVariable("flavour", "parton flavour", "Jet_partonFlavour", index,
                      True, flavour_classes)
    jet_vars.append(var)
    index += 1

    var = JetVariable("nTrks", "num trks per jet", "TagVarCSV_jetNTracks",
                      index)
    jet_vars.append(var)
    index += 1

    var = JetVariable("nEtaRelTrks", "num eta-rel trks per jet",
                      "TagVarCSV_jetNTracksEtaRel", index)
    jet_vars.append(var)
    index += 1
    
    var = JetVariable("flightDis2DSig", "SV 2D flight dist sig",
                      "TagVarCSV_flightDistance2dSig", index, unphysical=-9999)
    jet_vars.append(var)
    index += 1
    
    var = JetVariable("nSVs", "num SVs", "TagVarCSV_jetNSecondaryVertices",
                      index)
    jet_vars.append(var)
    index += 1
    
    var = JetVariable("vertexMass", "SV mass",
                      "TagVarCSV_vertexMass", index,
                      unphysical=-9999)
    jet_vars.append(var)
    index += 1

    var = JetVariable("nSVTrks", "num trks from SV", "TagVarCSV_vertexNTracks",
                      index)
    jet_vars.append(var)
    index += 1

    var = JetVariable("SVeRatio", "SV energy ratio",
                      "TagVarCSV_vertexEnergyRatio", index,
                      unphysical=-9999)
    jet_vars.append(var)
    index += 1

    var = JetVariable("SVdeltaR", "SV delta R",
                      "TagVarCSV_vertexJetDeltaR", index,
                      unphysical=-9999)
    jet_vars.append(var)
    index += 1

    var = JetVariable("jetPt", "jet pt", "Jet_pt", index)
    jet_vars.append(var)
    index += 1

    var = JetVariable("jetEta", "jet eta", "Jet_eta", index)
    jet_vars.append(var)
    index += 1

    var = JetVariable("aboveC", "first trk 2D IP sig above charm",
                      "TagVarCSV_trackSip2dSigAboveCharm", index,
                      unphysical=-1)
    jet_vars.append(var)
    index += 1

    var = JetVariable("sumTrkEtRatio", "summed trks Et ratio",
                      "TagVarCSV_trackSumJetEtRatio", index, unphysical=-9999)
    jet_vars.append(var)
    index += 1

    var = JetVariable("sumDeltaR", "summed delta R",
                      "TagVarCSV_trackSumJetDeltaR", index,
                      unphysical=-9999)
    jet_vars.append(var)
    index += 1

    var = JetVariable("IP2DSig", "2D IP sig", "TagVarCSV_trackSip2dSig", index)
    jet_vars.append(var)
    index += 1

    var = JetVariable("IP3DSig", "3D IP sig", "TagVarCSV_trackSip3dSig", index)
    jet_vars.append(var)
    index += 1

    var = JetVariable("trkPtRel", "trk relative pt", "TagVarCSV_trackPtRel",
                      index)
    jet_vars.append(var)
    index += 1

    var = JetVariable("trkDeltaR", "trk delta R", "TagVarCSV_trackDeltaR",
                      index)
    jet_vars.append(var)
    index += 1

    var = JetVariable("trkPtRatio", "trk pt ratio",
                      "TagVarCSV_trackPtRatio", index)
    jet_vars.append(var)
    index += 1

    var = JetVariable("trkDis", "trk distance", "TagVarCSV_trackJetDistVal",
                      index)
    jet_vars.append(var)
    index += 1

    var = JetVariable("trkDecayLen", "trk decay length",
                      "TagVarCSV_trackDecayLenVal", index)
    jet_vars.append(var)
    index += 1

    var = JetVariable("trkEtaRel", "trk eta rel", "TagVarCSV_trackEtaRel",
                      index)
    jet_vars.append(var)
    index += 1
    return jet_vars


# helper function to pad array wiht nan
# credit https://stackoverflow.com/a/40571482
def boolean_indexing(v, fillval=np.nan):
    '''
    Pad a list of lists so it is rectangular

    Parameters
    ----------
    v : list of lists
        thing to be padded
        
    fillval : value to padd with
         (Default value = np.nan)

    Returns
    -------
    out : numpy array
        rectangular padded array

    '''
    lens = np.array([len(item) for item in v])
    mask = lens[:, None] > np.arange(lens.max())
    out = np.full(mask.shape, fillval)
    out[mask] = np.concatenate(v)
    return out


def convert_unphysical(vals, jet_var):
    '''
    Replace unphysical values with np.nan

    Parameters
    ----------
    vals : numpy array
        the values to be fixed
        
    jet_var : JetVariable
        the jet variable with information discribing the values
        

    Returns
    -------
    vals : numpy array
        the fixed values

    '''
    if jet_var.unphysical is None:
        return vals
    epsilon = 0.001  # consider values unphysical is withing epsilon
    prox = np.abs(jet_var.unphysical - vals)
    # if the proximity is less than epsilon consider unphysical
    vals[prox < epsilon] = np.nan
    return vals

    
def next_free_indices(jet_vars, n=1):
    '''
    Look for the next indices that new variables can be placed in

    Parameters
    ----------
    jet_vars : list of JetVariables
        discribes the current variables and gicves the indices they are located at
        
    n : int
        number of indices to find
         (Default value = 1)

    Returns
    -------
    next_indices : list of int
        the next avalible indices

    '''
    all_indices = [i for v in jet_vars for i in v.indices]
    # check that all indices exist
    assert set(all_indices) == set(range(len(all_indices)))
    next_index = len(all_indices)
    next_indices = [next_index + i for i in range(n)]
    return next_indices


def add_indices(short_name, new_indices, jet_vars ):
    '''
    Increase the number of indices listed for a varaible
    

    Parameters
    ----------
    short_name : string
        short name of the variable that should gain indices
        
    new_indices : list of ints
        the indices this variable should gain
        
    jet_vars : list of JetVariables
        all the varibles in the set

    '''
    # add it to the right jet_var
    for var in jet_vars:
        if var.short_name == short_name:
            var.indices += new_indices


# a method to get the jet varaible given a short name, long name or index
def variable(identifier, jet_vars):
    '''
    Get a variable out of the list of varibles from anything that could identify it uniquely

    Parameters
    ----------
    identifier : string or int
        unique identifier, can be a name or an index
        
    jet_vars : list of JetVariables
        the variables to search
        

    Returns
    -------
    var : JetVariable
        the jet variable corrisponding to the identifier

    '''
    # identifier can be anything that would be unique
    for var in jet_vars:
        if isinstance(identifier, str):
            if (permisive_same(var.short_name, identifier)
                    or permisive_same(var.long_name, identifier)
                    or permisive_same(var.root_name, identifier)):
                return var
        if identifier in var.indices:
            return var


def permisive_same(string1, string2):
    string1 = str(string1)
    string2 = str(string2)
    string1 = "".join(string1.split()).lower()
    string2 = "".join(string2.split()).lower()
    return string1 == string2

class Indexer:
    ''' Class to create fancy indexing patterns that select jets acording to given criteria'''
    def __init__(self, n_rows, n_cols):
        self.n_rows = n_rows
        self.n_cols = n_cols
        self.combined_rowsel = np.arange(self.n_rows)
        self.combined_colsel = np.arange(self.n_cols)
        self.rowsels = {}  # all jet based selection
        self.colsels = {}  # all observation based selection

    def __getitem__(self, idx):
        if isinstance(idx, tuple):
            i = self.combined_rowsel[idx[0]]
            j = self.combined_colsel[idx[1]]
        else:
            i = self.combined_rowsel[idx]
            j = self.combined_colsel
        return np.ix_(i, j)
    
    def list(self, idx):
        '''
        Method for getting selected jets corrispoding to a list of indices
        

        Parameters
        ----------
        idx : list of indices
            

        Returns
        -------
        : numpy array
            fancy indexing matrix

        '''
        i = self.combined_rowsel[idx]
        # return a mesh of indices
        return np.ix_(i, self.combined_colsel)

    # filters for jets
    def add_rowsel(self, name, selection):
        '''
        Method to creat a filter to exclude some jet indices.   

        Parameters
        ----------
        name : string
            desired name of this filter
            
        selection : list of int
            the indices not excluded by this filter
            

        '''
        # select a subset of the jets and record the name of the selection criteria
        assert len(selection) < self.n_rows
        assert np.max(selection) < self.n_rows
        self.rowsels[name] = selection
        # update the combined selection
        self.combined_rowsel = np.intersect1d(selection, self.combined_rowsel)
        print("Jet selection {} added.".format(name))
        # changing the jet selection may change if the data should be in ram

    def names_rowsel(self):
        ''' get the names of the row selections '''
        # return the names of the subsets
        return self.rowsels.keys()

    def get_rowsel(self, name):
        '''
        Get a row selection from it's name

        Parameters
        ----------
        name : string
            name of the desired selection
            

        Returns
        -------
        : list of ints
            the chosen selection

        '''
        # return the selction of a particular rowsel
        return self.rowsels[name]

    def rm_rowsel(self, name):
        '''
        Delete a row selection

        Parameters
        ----------
        name : sting
            name of the selection to be deleted
            

        '''
        # delete a given rowsel
        del self.rowsels[name]
        self.combined_rowsel = np.arange(self.n_rows)
        for sel in self.rowsels.values():
            self.combined_rowsel = np.intersect1d(sel, self.combined_rowsel)
        print("Jet selection {} removed.".format(name))

    # filters for observables
    def add_colsel(self, name, selection):
        '''
        

        Parameters
        ----------
        name :
            
        selection :
            

        Returns
        -------

        '''
        # select a subset of the observables and record the name of the selection criteria
        assert len(selection) < self.n_cols
        assert np.max(selection) < self.n_cols
        self.colsels[name] = selection
        # update the combined selection
        self.combined_colsel = np.intersect1d(selection, self.combined_colsel)
        self.combined_colsel = list(self.combined_colsel)
        # changing the obs selection may change if the data should be in ram
        print("Observable selection {} added.".format(name))

    def names_colsel(self):
        ''' '''
        # return the names of the subsets
        return self.colsels.keys()

    def get_colsel(self, name):
        '''
        

        Parameters
        ----------
        name :
            

        Returns
        -------

        '''
        # return the selction of a particular colsel
        return self.colsels[name]

    def rm_colsel(self, name):
        '''
        

        Parameters
        ----------
        name :
            

        Returns
        -------

        '''
        # delete a given colsel
        del self.colsels[name]
        self.combined_colsel = np.arange(self.n_cols)
        for sel in self.colsels.values():
            self.combined_colsel = np.intersect1d(sel, self.combined_colsel)
        self.combined_colsel = list(self.combined_colsel)
        print("Observable selection {} removed.".format(name))


def indices_by_flavour(flavour_var, jet_flavours, target_classes=None):
    # consider particles the same as anitiparticles
    jet_flavours = np.abs(jet_flavours)
    flavour_dict = flavour_var.classes
    # if we have been given target classes those are the 
    # only flavour we care about
    if target_classes:
        # list the unused classes
        unused = set(flavour_dict.values()) - set(target_classes)
        # they are added to the background
        bg_name = "+".join(unused)
        flavour_dict = {k: v for k, v in flavour_dict.items() if v in target_classes}
        flavour_dict['other'] = bg_name
    else:
        # get teh background class name
        if 'other' in flavour_dict:
            bg_name = flavour_dict['other']
        else:
            bg_name = 'udsg'
    flavour_names = flavour_dict.values()
    jet_f_names = np.array([flavour_dict.get(jet_f, bg_name) for jet_f in jet_flavours])
    print("assigning jets to signal and background")
    by_flavour_indices = {k: np.where(jet_f_names == k)[0] for k in flavour_names}
    for key in by_flavour_indices:
        print("{}: {} jets".format(key, len(by_flavour_indices[key])))
    return by_flavour_indices

