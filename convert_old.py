'''module to convert an old dataset to a new one'''
from data_readwrite import setup_data_file, gen_jet_vars, variable, next_free_indices
from split_test_train import assign_jets
import h5py
import numpy as np
import json


def convert_dataset(old_h5_name, old_ddict, old_tdict, new_h5_name, test_percent=1.):
    print("Converting {}".format(old_h5_name))
    jet_vars = gen_jet_vars()
    # load the dictionary of variabels
    print("Reading variables from {}".format(old_ddict))
    with open(old_ddict, 'r') as jf:
        ddict = json.loads(jf.read())
    # go through the dataset and put the correct indices on the jet_vars
    new_jet_vars = []
    for old_name, column_num in ddict.items():
        # which jet variable is this
        var, track_num = old_name_to_var(old_name, jet_vars)
        print("{} is {}".format(old_name, var.short_name))
        # do we already have a copy
        new_var = variable(var.short_name, new_jet_vars)
        if new_var is None:
            adding_var = True
            new_var = var
        else:
            adding_var = False
        # do we need more space for indices
        if len(new_var.indices) < track_num:
            old_indices = np.copy(new_var.indices)
            new_var.indices = np.full(track_num, np.nan)
            new_var.indices[:len(old_indices)] = old_indices
        # put the right index in
        new_var.indices[track_num-1] = column_num
        # if it's new add it to the list
        if adding_var:
            new_jet_vars.append(new_var)
    # this will be the dictionary for the targets
    print("Target names from {}".format(old_tdict))
    with open(old_tdict, 'r') as jf:
        tdict = json.loads(jf.read())
    flavour_var = variable('flavour', new_jet_vars)
    flavour_classes = {int(v): k for k, v in tdict.items()}
    flavour_var.classes = flavour_classes
    # now make a test_train variable
    testTrain_var = variable('testTrain', jet_vars)
    free_indices = np.array(next_free_indices(new_jet_vars))
    testTrain_var.indices = free_indices
    new_jet_vars.append(testTrain_var)
    # now make the file
    print("Making new data file {}".format(new_h5_name))
    setup_data_file(new_h5_name, new_jet_vars)
    # finally shift the data
    with h5py.File(old_h5_name, 'r') as old_hf, h5py.File(new_h5_name, 'a') as new_hf:
        old_data = old_hf['data'][:]
        old_cols = old_data.shape[1]
        old_rows = old_data.shape[0]
        new_hf['data'].resize((old_rows, old_cols + 1))
        new_hf['data'][:, :old_cols] = old_data
    assign_jets(new_h5_name, test_percent)
    print("Done")


def old_name_to_var(old_name, jet_vars):
    # this name changed
    if old_name == 'target':
        var = variable('flavour', jet_vars)
    else: 
        # try to match that old name to something in jet_vars
        var = variable(old_name, jet_vars)
    # by default assume we are dealing with the first track
    track_num = 1
    if var is None:
        # possible that the last bit is a track number
        old_split = old_name.split()
        if old_split[-1].isdigit():
            old_name_start = "".join(old_split[:-1])
            var = variable(old_name_start, jet_vars)
            track_num = int(old_split[-1])
            if var is None:
                # then we really don't know
                print("Error, could not match {}".format(old_name))
                return None
        else:
            print("Error, could not match {}".format(old_name))
            return None
    return var, track_num
                
