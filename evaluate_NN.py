import numpy as np
from sys import argv
import sys
import torch
from ipdb import set_trace as st
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
import matplotlib
from sklearn.metrics import roc_curve, auc
from NN_pytorch import CSVv2_dataset, CSVv2_Net, DeepCSV_dataset, DeepCSV_Net
from data_readwrite import read_variables, variable, indices_by_flavour
from quick_plots import colour
import h5py
from output_read import get_flag, predictions_and_targets, filter_runs


def prediction_for_target(run, h5_name, jet_class, focus=0, ddict_name=None):
    """A function to mesure the predictied probabilities of the test data being
    the focus target

    """
    # run the data throught he net
    results, targets, test_flavours = predictions_and_targets(run, h5_name, ddict_name=ddict_name)
    # grab the right column
    results = results[:, focus]
    # now we wish to sort the data into the true flavours
    all_flavours = set(test_flavours)
    results_by_flavour = {flavour: results[test_flavours == flavour]
                          for flavour in all_flavours}
    return results_by_flavour


def hist_predictions(list_runs, h5_names, focus=0, save_name=None, bins=70, focus_name='b', ddict_name=None, image_name=None):
    """
    punction to plot and save histograms of the predictions of the net(s) by true jet flavour
    """
    fig, ax = plt.subplots()
    ax.set_ylim(0.00001, 1000)
    ax.set_xlim(0., 1.)
    # get the background image
    if image_name:
        image = plt.imread(image_name)
        ax.imshow(image, transform=ax.transAxes, extent=[0., 1., 0., 1.])
    # now plot data
    if isinstance(h5_names, str):
        h5_names = [h5_names for _ in list_runs]
    if len(h5_names) == 1:
        h5_names = [h5_names[0] for _ in list_runs]
    assert len(list_runs) == len(h5_names), "need as many nets and data sets"
    # each histogram is to be plotted on the same axis
    if image_name:
        line_styles = [':', '--', '-.']
    else:
        line_styles = ['-', ':', '--', '-.']
    # we will limit to plotting max hists
    max_hist = len(line_styles)
    if len(list_runs) > max_hist:
        print("Plotting first {} runs out of {}.".format(max_hist, len(list_runs)))
        list_runs = list_runs[:max_hist]
    run_names = []
    # plot for each run
    for r, (h5_name, run) in enumerate(zip(h5_names, list_runs)):
        # load the data
        results_by_flavour = prediction_for_target(run, h5_name, focus, ddict_name=ddict_name)
        flavour_names = list(results_by_flavour.keys())
        min_max = (0., 1.)
        for name in flavour_names:
            results = results_by_flavour[name]
            weights = np.ones_like(results) / (3*len(results))
            line_colour = colour(name)
            face_colour = (*line_colour[:3], 0.3)
            ax.hist(results,
                    histtype='step', color=line_colour, label=name,
                    range=min_max,  log=True, bins=bins, weights=weights,
                    ls=line_styles[r], lw=3, facecolor=face_colour)
        run_names.append(input("Legend name for {}".format(run)))
    if image_name:
        flavour_patches = [Line2D([], [], color=colour(name), label=name, lw=2)
                           for name in flavour_names]
        handles = [Line2D([], [], alpha=0., label='CMS results')] + flavour_patches
    else:
        handles = []
    for run_name, line_style in zip(run_names, line_styles):
        handles.append(Line2D([], [], alpha=0., label=run_name))
        handles += [Line2D([], [], color=colour(name), label=name, lw=3, ls=line_style)
                    for name in flavour_names]
    ax.legend(handles=handles)
    if focus_name:
        xlabel = "Net predicting {}".format(focus_name)
    else:
        xlabel = "Predictions from output node {}".format(focus)
    ax.set_xlabel(xlabel)
    ax.set_ylabel("log normed frequency")
    ax.set_title("Net predictions split by true flavour")
    if save_name:
        plt.savefig(save_name)
    plt.show()


def roc(list_runs, h5_names, focus=0, target_flavour='b', save_name=None, ddict_name=None):
    if isinstance(h5_names, str):
        h5_names = [h5_names for _ in list_runs]
    if len(h5_names) == 1:
        h5_names = [h5_names[0] for _ in list_runs]
    assert len(list_runs) == len(h5_names), "need as many nets and data sets"
    # setup the plot
    plt.figure(figsize=(12, 12))
    plt.xlabel("False positive rate")
    plt.ylabel("True positive rate")
    plt.xlim([0., 1.])
    plt.ylim([0., 1.05])
    # go through the inputs
    colours = [colour(i) for i in np.linspace(0.1, 0.9, len(list_runs))]
    for run, h5_name, c in zip(list_runs, h5_names, colours):
        results_by_flavour = prediction_for_target(run, h5_name, focus, ddict_name=ddict_name)
        # n_jets = sum(len(results) for results in results_by_flavour.values())
        flavour_names = list(results_by_flavour.keys())
        results = np.concatenate([results_by_flavour[flavour] for flavour in flavour_names])
        # set up the targets
        targets = np.concatenate([np.full_like(results_by_flavour[flavour],
                                 flavour == target_flavour,
                                 dtype=np.int)
                                 for flavour in flavour_names])
        fpr, tpr, _ = roc_curve(targets, results)
        this_auc = auc(fpr, tpr)
        label = "{}; target {} (area={:5.3f})".format(input("Name {}".format(run)), target_flavour, this_auc)
        plt.plot(fpr, tpr, color=c, label=label)
    plt.legend(loc="lower right")
    if save_name:
        plt.savefig(save_name)
    plt.show()


def main():
    flags = ['-m', '-r', '-d', '--bins', '--ddict']
    # if there are no arguments explaint he options
    if len(argv) == 1:
        print("Flags are {}".format(flags))
        print("-m is mode, should be roc or hist")
        print("-r is runs, dump a bunch of file names that includes the runs wanted")
        print("-d is data, should eb the h5 file")
        print("--bins, int, number of bins")
        print("--ddict, if one of the nets is an old net")
        sys.exit(0)
    mode = get_flag('-m', flags)
    assert len(mode) == 1, 'no valid mode given'
    runs = get_flag('-r', flags)
    assert len(runs) > 0, 'no net given'
    data = get_flag('-d', flags)
    assert len(data) > 0, 'no data given'
    ddict_name = get_flag('--ddict', flags)
    if len(ddict_name) == 1:
        ddict_name = ddict_name[0]
    else:
        ddict_name = None
    list_runs = filter_runs(runs)
    print("Found {} runs".format(len(list_runs)))
    if mode[0] == 'roc':
        roc(list_runs, data, ddict_name=ddict_name)
    elif mode[0] == 'hist':
        bins = get_flag('--bins', flags)
        if len(bins) == 1:
            bins = int(bins[0])
        else:
            bins = 70
        hist_predictions(list_runs, data[0], bins=bins, ddict_name=ddict_name)
    else:
        print('mode name {} not valid, should be "roc" or "hist"')


if __name__ == '__main__':
    main()
