''' module to test spliting the jets into classes'''
from data_readwrite_test import test_file_path
from split_jet_class import assign_class
from data_readwrite import read_variables, variable
import numpy as np
import h5py
import numpy.testing as tst
from data_root import add_rootFile


def test_assign_class():
    with test_file_path() as h5_name:
        add_rootFile("/media/henry/Unicorn/files/work/PhD/jetTagger/Data/TTTo2L2Nu/TTTo2L2Nu_1_ntp.root", h5_name, max_events=10)
        jet_vars = read_variables(h5_name)
        class_i = variable('jetClass', jet_vars).indices[0]
        num_SVs_i = variable('nSVs', jet_vars).indices[0]
        vertex_mass_i = variable('vertexMass', jet_vars).indices[0]
        ip2dsig_is = variable('IP2DSig', jet_vars).indices
        # assignment dictionary
        class_dict = variable('jetClass', jet_vars).classes
        a_dict = {v: k for k, v in class_dict.items()}
        # som deciding variables
        mass_KShort = 497.614
        required_mass_dist = 50.
        required_sip = 2
        with h5py.File(h5_name, 'a') as hf:
            # make the first jet a reco jet
            hf["data"][0, num_SVs_i] = 1
            # make the second jet a psudo jet
            hf["data"][1, num_SVs_i] = 0
            hf["data"][1, ip2dsig_is[0]] = required_sip + 1
            hf["data"][1, vertex_mass_i] = mass_KShort - 2 * required_mass_dist
            # make the third jet a no jet
            hf["data"][2, num_SVs_i] = 0
            for i_sip in ip2dsig_is:
                hf["data"][2, i_sip] = 0
            hf["data"][2, vertex_mass_i] = mass_KShort 
        # run the assignment function
        assign_class(h5_name)
        # check the first 3 assignments
        with h5py.File(h5_name, 'r') as hf:
            jet_1_class = hf["data"][0, class_i]
            exp_1_class = a_dict["reco jet"]
            tst.assert_allclose(jet_1_class, exp_1_class)
            jet_2_class = hf["data"][1, class_i]
            exp_2_class = a_dict["psudo jet"]
            tst.assert_allclose(jet_2_class, exp_2_class)
            jet_3_class = hf["data"][2, class_i]
            exp_3_class = a_dict["no jet"]
            tst.assert_allclose(jet_3_class, exp_3_class)


def main():
    test_assign_class()


if __name__ == '__main__':
    main()
