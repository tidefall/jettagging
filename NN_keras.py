''' CSVv2 replica in keras '''
import os
import cpuinfo
from ipdb import set_trace as st
import sys
import time
from data_readwrite import read_variables, variable
import h5py
import numpy as np
import torch  # for device info
import keras
from keras.models import Sequential
from keras.layers import Dense
from keras.optimizers import Adam
from keras.callbacks import ReduceLROnPlateau
from timed_stopping import TimedStopping
from keras import backend
from NN_pytorch import CSVv2_dataset, DeepCSV_dataset, zero_fill_nan


def CSVv2_make_net(input_size, num_classes, initial_lr, weight_decay):
    """A 'deep' neural net with one hidden layer

    Parameters
    ----------
    input_size : int
        the number of nodes at the input layer. This must equal the number
        of variables in each data point.
    num_classes : int
        number of nodes at the output layer. This must equal the number of
        classes we wish to sort the data into.

    """
    # prepare the weight decay in a L2 reguliser
    reg = keras.regularizers.l2(weight_decay)
    hidden_size = input_size * 2
    net = Sequential()
    # this si the hidden layer
    net.add(Dense(hidden_size, activation='relu', input_shape=(input_size,), kernel_regularizer=reg))
    # move onto the output layer, which is softmax
    net.add(Dense(num_classes, activation='softmax', kernel_regularizer=reg))
    net.compile(loss='binary_crossentropy',
                optimizer=Adam(lr=initial_lr),
                metrics=['accuracy'])
    return net


def DeepCSV_make_net(input_size, num_classes, initial_lr, weight_decay):
    """A 'deep' neural net with one hidden layer

    Parameters
    ----------
    input_size : int
        the number of nodes at the input layer. This must equal the number
        of variables in each data point.
    num_classes : int
        number of nodes at the output layer. This must equal the number of
        classes we wish to sort the data into.

    """
    # prepare the weight decay in a L2 reguliser
    reg = keras.regularizers.l2(weight_decay)
    hidden_size = 100
    net = Sequential()
    # hidden layer 1
    net.add(Dense(hidden_size, activation='relu', input_shape=(input_size,), kernel_regularizer=reg))
    # hidden layer 2
    net.add(Dense(hidden_size, activation='relu', input_shape=(input_size,), kernel_regularizer=reg))
    # hidden layer 3
    net.add(Dense(hidden_size, activation='relu', input_shape=(input_size,), kernel_regularizer=reg))
    # hidden layer 3
    net.add(Dense(hidden_size, activation='relu', input_shape=(input_size,), kernel_regularizer=reg))
    # move onto the output layer, which is softmax
    net.add(Dense(num_classes, activation='softmax', kernel_regularizer=reg))
    net.compile(loss='binary_crossentropy',
                optimizer=Adam(lr=initial_lr),
                metrics=['accuracy'])
    return net


class Weight_history(keras.callbacks.Callback):
    def on_train_begin(self, logs={}):
        self.mag_weights = []
        self.times = []

    def on_epoch_end(self, batch, logs={}):
        weights_here = 0.
        for layer in self.model.layers:
            weights_here += sum([np.sum(np.abs(w)) for w in layer.get_weights()])
        self.mag_weights.append(weights_here)
        self.times.append(time.time())


def train(h5_name, run_time, net_type, verbose=0, batch_size=100, initial_lr=0.1, weight_decay=0.005):
    # create a dataset and a dataloader
    if net_type == 'csvv2':
        dataset = CSVv2_dataset(h5_name, jet_class=0)
        dataset.interpolate(zero_fill_nan)
    elif net_type == 'deepcsv':
        dataset = DeepCSV_dataset(h5_name)
        dataset.interpolate(zero_fill_nan)
    else:
        print("Error; net_type {} not recognised, expecting 'csvv2' or 'deepcsv'".format(net_type))
        sys.exit(1)
    test_targets = dataset.test_targets
    test_inputs = dataset.test
    train_targets = dataset.train_targets
    train_inputs = dataset.train
    train_weights = dataset.train_weights
    # apply a schedular to the learning rate
    if net_type == 'csvv2':
        net = CSVv2_make_net(len(test_inputs[0]), len(test_targets[0]), initial_lr, weight_decay)
    elif net_type == 'deepcsv':
        net = DeepCSV_make_net(len(test_inputs[0]), len(test_targets[0]), initial_lr, weight_decay)
    else:
        print("Error; net_type {} not recognised, expecting 'csvv2' or 'deepcsv'".format(net_type))
        sys.exit(1)
    # remeber the weights
    weight_hst = Weight_history()
    # adaptive learning rate
    reduce_lr = ReduceLROnPlateau()
    # make it timed
    timed_stop = TimedStopping(run_time)
    history = net.fit(train_inputs, train_targets,
                      batch_size=batch_size,
                      epochs=100000,
                      verbose=verbose,
                      sample_weight=train_weights,
                      callbacks=[reduce_lr, timed_stop, weight_hst],
                      validation_data=(test_inputs, test_targets))
    # extract the progress data
    time_stamps = weight_hst.times
    training_loss = history.history['loss']
    test_loss = history.history['val_loss']
    mag_weights = weight_hst.mag_weights
    learning_rates = history.history['lr']
    return net, time_stamps, training_loss, test_loss, mag_weights, learning_rates


def begin_training(h5_name, run_time, batch_size, use_gpu, net_type, learning_rate, weight_decay):
    backend_name = backend.backend()
    # Device configuration
    if use_gpu:
        if backend_name == 'tensorflow':
            from tensorflow.python.client import device_lib
            device_list = device_lib.list_local_devices()
            type_list = [device.device_type for device in device_list]
            print("Device list: {}".format(device_list))
            print("Type list: {}".format(type_list))
            if 'GPU' in type_list:
                ind = type_list.index('GPU')
                device_name = torch.cuda.get_device_name(0)
                tf_name = device_list[ind].name
                major_cap, minor_cap = torch.cuda.get_device_capability(0)
                device_discription =\
                    "{}/{}; Capability major={}, minor={}"\
                    .format(device_name, tf_name, major_cap, minor_cap)
            else:
                print("Error, requested gpu but no gpu listed")
                sys.exit(1)
        else:
            print("Error, gpu requested but backend is not tensorflow")
            sys.exit(1)
    else:
        if backend_name == 'theano':
            device_discription = cpuinfo.cpu.info[0]['model name']
        else:
            print("CPU requested but backend is not theano")
            print("attempting, but risk that gpu may be used if avaible")
            device_discription = cpuinfo.cpu.info[0]['model name']
            # sys.exit(1)

    # get the total ram to return
    mem = str(os.popen('free -t -m').readlines())
    total_ram = mem.index('T')

    verbose = 1
    training_returns = train(h5_name, run_time, net_type, verbose, batch_size, learning_rate, weight_decay)
    return device_discription, total_ram, training_returns
