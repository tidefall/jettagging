''' A module t allow the quick plotting of data'''
import matplotlib.pyplot as plt
import tillahoffmann_kde as thk
import copy
import matplotlib
import numpy as np
from scipy import stats
import h5py
from ipdb import set_trace as st
from data_readwrite import variable, indices_by_flavour, Indexer, read_variables
from itertools import cycle
from output_read import get_flag
from sys import argv, exit


def plot_params(values, weights=None, labels=None, colours=None):
    # need to remove nan values before plotting
    if isinstance(values[0], np.ndarray):
        nan_masks = [~np.isnan(v) for v in values]
    else:
        nan_masks = [[~np.isnan(values)]]
        values = [values]
    values = [v[mask] for v, mask in zip(values, nan_masks)]
    mean = np.nanmean(np.concatenate(values))
    spread = 2.5 * np.nanstd(np.concatenate(values))
    x_min = mean - spread
    x_max = mean + spread
    print("x min {}, x max {}".format(x_min, x_max))
    # if there are no colours make some up
    if colours is None and labels is not None:
        colours = [(0., 0., 1., 1.),
                   (0., 0.5, 0., 1.),
                   (1., 0., 0., 1.),
                   (0., 0.75, 0.75, 1.),
                   (0.75, 0., 0.75, 1.),
                   (0.75, 0.75, 0., 1.)]
        cycle_c = cycle(colours)
        colours = [next(cycle_c) for _ in labels]
    # make a weighted an unweighted version
    if weights:
        # remove weights corrisponding to nan values in the data
        weights = [w[mask] for w, mask in zip(weights, nan_masks)]
        # list of ones for unweighted data
        ones = [np.ones_like(w) for w in weights]
        weights += ones
        values += values
        labels += ["Unweighted " + l for l in labels]
        # give the reweighted set half transparent colours
        colours += [c[:3] + (0.4,) for c in colours]
    return values, weights, labels, colours, x_min, x_max 


def plot_density(ax, values, weights=None, labels=None, colours=None, fidelity=50):
    # prepare the parameters
    values, weights, labels, colours, x_min, x_max = plot_params(values, weights, labels, colours)
    x_step = (x_max - x_min)/fidelity
    # using kde get the densities
    x = np.arange(x_min, x_max, x_step)
    if weights:
        # use the hoffman density estmator
        densities = [thk.gaussian_kde(v, weights=w) for v, w in zip(values, weights)]
    else:
        densities = [stats.kde.gaussian_kde(v) for v in values]
    # if there are labels use them
    if labels is not None:
        for density, label, colour in zip(densities, labels, colours):
            ax.plot(x, density(x), label=label, color=colour)
    else:
        for density in densities:
            ax.plot(x, density(x))
    return ax   
    

def plot_hist(ax, values, weights=None, labels=None, fidelity=50, colours=None):
    # prepare the parameters
    values, weights, labels, colours, x_min, x_max = plot_params(values, weights, labels, colours)
    # if there are labels use them
    ax.hist(values, fidelity, (x_min, x_max), weights=weights, label=labels, histtype='step', density=True, color=colours)
    return ax   
 
    
def plot_firsts(hdf5_name, var_names=None, use_hist=False, reweighting=False, fidelity=50, plot_cols=None):
    matplotlib.rcParams.update({'font.size': 7})
    print("Obtaining file structure info")
    # discover the columns that the required values live at
    jet_vars = read_variables(hdf5_name)
    # we want to only retreve the items that we need from data
    # else we risk killing the ram
    first_dict = {}
    # get the first indices of each variable
    if var_names:
        # in the case we have been given variables use only them
        for name in var_names:
            var = variable(name, jet_vars)
            first_dict[var.indices[0]] = var.long_name
    else:
        # just use all the observable variabes
        for var in jet_vars:
            if var.truth:
                continue
            first_dict[var.indices[0]] = var.long_name
    # produce two sorted lists
    first_indices = sorted(list(first_dict.keys()))
    var_names = [first_dict[i] for i in first_indices]
    num_plots = len(var_names)
    # if we are reweighting then we need to split the variabes by flavour
    if reweighting:
        flavour_v = variable('flavour', jet_vars)
        weight_v = variable('weight', jet_vars)
        flavour_names = sorted(list(flavour_v.classes.values()), reverse=True)
    # open the file to get at the data
    print("Opening file to read data")
    with h5py.File(hdf5_name, 'r') as hf:
        num_jets = hf['data'].shape[0]
        print("Total jets {}".format(num_jets))
        all_var_data = hf['data'][:, first_indices].T
        jet_data = [var_data for var_data in all_var_data]
        if reweighting:
            jet_weights = hf['data'][:, weight_v.indices[0]]
            # get the indices for the signal and backgrund distributions
            jet_flavours = hf['data'][:, flavour_v.indices[0]]
            i_by_flavour = indices_by_flavour(flavour_v, jet_flavours)
            # split the data usng the indices
            jet_data = [[var_data[i_by_flavour[name]] for name in flavour_names] for var_data in jet_data]
            jet_weights = [jet_weights[i_by_flavour[name]] for name in flavour_names]
        else:
            flavour_names = None
            jet_weights = None
        if plot_cols is None:
            plot_cols = int(np.sqrt(num_plots))
        plot_rows = int(num_plots/plot_cols)
        if plot_rows * plot_cols < num_plots:
            plot_rows += 1
        # if its the reweighing we are into, get the weights
        fig, axarr = plt.subplots(plot_rows, plot_cols, sharex=True)
        if isinstance(axarr, np.ndarray):
            axarr = axarr.flatten()
        else:
            axarr = [axarr]
        for index, var_name in enumerate(var_names):
            ax = axarr[index]
            print(var_name, index)
            ax.set_title(var_name)
            if use_hist:
                ax.set_ylabel('Frequency')
            else:
                ax.set_ylabel('Density')
            ax.set_xlabel('Normalised value')
            var_data = jet_data[index]
            if use_hist:
                plot_hist(ax, var_data, jet_weights, copy.copy(flavour_names), fidelity=fidelity)
            else:
                plot_density(ax, var_data, jet_weights, copy.copy(flavour_names), fidelity=fidelity)
        leg = axarr[index].legend()
        leg.get_frame().set_alpha(0.5)
    plt.subplots_adjust(left=0.03, bottom=0.03, right=0.97, top=0.97, wspace=0.14, hspace=0.18)
    plt.show()


default_c_map = matplotlib.cm.get_cmap('gnuplot')
default_c_list = ['r', 'g', 'b', 'c', 'm']


def colour(c, opacity=1.0):
    if isinstance(c, float):
        c = min(c, 0.9) # avoid the end of the colour map
        return default_c_map(c, opacity)
    if isinstance(c, int):
        len_list = len(default_c_list)
        # we scale the opacity and pick from the colour list
        opacity = 1./(int(c/len_list) + 1.)
        pigment = default_c_list[c % len_list]
        return matplotlib.colors.to_rgba(pigment)[:3] + (opacity,)
    if isinstance(c, str):
        if c == 'b':
            return colour(0, opacity)
        if c == 'c':
            return colour(1, opacity)
        if 'udsg' in c:
            return colour(2, opacity)
        if c == 'grey':
            return (0.5, 0.5, 0.5, opacity)
        if c == 'black':
            return (0, 0, 0, opacity)
    print("Don't recognise colour {}".format(c))
        

def main():
    flags = ['-m', '-d', '--reweight', '--variables', '--fidelity', '--columns']
    if len(argv) == 1:
        print("Flags are {}".format(flags))
        print("-m is mode, this can be hist or density")
        print("-d is data, this si the h5 file")
        print("--reweight is to plot the reweighted values (y/n)")
        print("--variabels is the column names to plot")
        print("--fidelity is an int to select the fidelity")
        print("--columns is the number of columns to put in the plot grid")
        exit(0)
    mode = get_flag('-m', flags)[0]
    if 'hist' in mode.lower():
        use_hist = True
    else:
        use_hist = False
    data = get_flag('-d', flags)[0]
    reweight = get_flag('--reweight', flags)
    if len(reweight) == 0:
        reweighting = True
    elif reweight[0][:1].lower() == 'y':
        reweighting = True
    else:
        reweighting = False
    fidelity = get_flag('--fidelity', flags)
    if len(fidelity) == 0:
        fidelity = 50
    elif fidelity[0].isdigit():
        fidelity = int(fidelity[0])
    else:
        print("Error, fidelity should be an int")
        fidelity = 50
    n_cols = get_flag('--columns', flags)
    if len(n_cols) == 0:
        n_cols = None
    elif n_cols[0].isdigit():
        n_cols = int(n_cols[0])
    else:
        print("Error, columns should be an int")
        n_cols = None
    variables = get_flag('--variables', flags)
    if len(variables) == 0:
        variables = None
    plot_firsts(data, variables, use_hist, reweighting, fidelity, n_cols)
    
    
if __name__ == '__main__':
    main()
