''' file to unit test the data_root module'''
import h5py
from data_root import add_rootFile
from data_readwrite_test import test_file_path
from data_readwrite import read_variables, variable
import numpy as np
import numpy.testing as tst


def test_add_rootFile():
    with test_file_path() as h5_name:
        add_rootFile("/media/henry/Unicorn/files/work/PhD/jetTagger/Data/TTTo2L2Nu/TTTo2L2Nu_1_ntp.root", h5_name, max_events=10)
        # check some of the values are what they should be
        expected_first_line = [("nJet", 4),
                               ("nTrks", 2),
                               ("nEtaRelTrks", 0),
                               ("flightDis2DSig", np.nan),
                               ("nSVs", 0),
                               ("vertexMass", np.nan),
                               ("nSVTrks", 0),
                               ("jetPt", 53.503704),
                               ("jetEta", 1.72222025),
                               ("aboveC", 0.3603180),
                               ("sumTrkEtRatio", 0.8724590),
                               ("trkDecayLen", 0.0291923)]
        with h5py.File(h5_name, 'r') as hf:
            first_line = hf['data'][0]
        jet_vars = read_variables(h5_name)
        for exp_val in expected_first_line:
            var = variable(exp_val[0], jet_vars)
            read_val = first_line[var.indices[0]]
            tst.assert_allclose(read_val, exp_val[1], atol=0.001,
                                err_msg="{}={} not {} as expected".format(var.short_name, read_val, exp_val[1]))

def main():
    test_add_rootFile()

    
if __name__ == '__main__':
    main()
