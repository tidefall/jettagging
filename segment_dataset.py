''' take a datatset and create n datasets that have an equaly sized randome chunk'''
from data_readwrite import read_variables, setup_data_file
from ipdb import set_trace as st
import h5py
import numpy as np


def split(h5_name, n_split, n_fragments=None, tag='frag'):
    if n_fragments == None:
        n_fragments = n_split
    assert n_fragments <= n_split, "Cant make more fragments than we have splits"
    print("reading the variables")
    jet_vars = read_variables(h5_name)
    print("Opening the original file")
    with h5py.File(h5_name, 'r') as hf:
        total_jets = hf['data'].shape[0]
        n_cols = hf['data'].shape[1]
        assignments = np.random.randint(0, n_split, total_jets)
        for fragment_i in range(n_fragments):
            print("creating fragment {}".format(fragment_i))
            file_name = h5_name[:-3] + "_{}{}.h5".format(tag, fragment_i)
            setup_data_file(file_name, jet_vars)
            selected_jets = np.where(assignments==fragment_i)[0]
            n_rows = len(selected_jets)
            with h5py.File(file_name, 'a') as frag_hf:
                frag_hf['data'].resize((n_rows, n_cols))
                for new_i, old_i in enumerate(selected_jets):
                    frag_hf['data'][new_i] = hf['data'][old_i]
    print("done")

            

        


