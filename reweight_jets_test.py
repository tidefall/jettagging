''' a module to test the reweight_jets module'''
from data_readwrite import read_variables, variable, Indexer
from ipdb import set_trace as st
from reweight_jets import generate_weights, class_ballence
from data_readwrite_test import test_file_path
from data_root import add_rootFile
from matplotlib.pyplot import hist
import numpy as np
import h5py
import numpy.testing as tst


def test_generate_weights():
    with test_file_path() as h5_name:
        add_rootFile("/media/henry/Unicorn/files/work/PhD/jetTagger/Data/TTTo2L2Nu/TTTo2L2Nu_1_ntp.root", h5_name, max_events=10)
        generate_weights(h5_name)
        jet_vars = read_variables(h5_name)
        # get the two target variables
        reweighted_names = ["jetPt", "jetEta"]
        reweighted_indices = sorted([variable(name, jet_vars).indices[0] for name in reweighted_names])
        weight_i = variable('weight', jet_vars).indices[0]
        # get the values from the file
        with h5py.File(h5_name, 'r') as hf:
            one_weights = hf['data'][:, weight_i]
            reweighted = hf['data'][:, reweighted_indices]
        weights = np.zeros_like(reweighted)
        for col in range(len(reweighted_names)):
            weights[:, col] = one_weights
        # now make histograms
        hist_counts, _, _ = hist(reweighted, 20, weights=weights, stacked=True, density=True)
        # test if the historgrams are close
        for col in range(1, len(reweighted_names)):
            tst.assert_allclose(hist_counts[0], 
                                hist_counts[col],
                                rtol=0.2, atol=0.1)
        # check that no weight is over 100
        assert np.max(one_weights) < 100


#def class_ballence(h5_name, target_classes=['b', 'c']):
#    print("Obtaining file structure info")
#    # discover the columns that the required values live at
#    jet_vars = read_variables(h5_name)
#    flavour_v = variable('flavour', jet_vars)
#    weight_i = variable('weight', jet_vars).indices[0]
#    # open the given hdf5 file
#    with h5py.File(h5_name, 'a') as hf:
#        # check that weights have been assigned
#        assert hf['data'].attrs["weighted"], "error, use generate_weights() first"
#        print("Obtaingin jet flavours")
#        jet_flavours = hf['data'][:, flavour_v.indices[0]]
#        sg_bg_indices = indices_by_flavour(flavour_v, jet_flavours, target_classes)
#        weights = hf['data'][:, weight_i]
#        sg_bg_weight_sum = {k: np.sum(weights[v]) for k, v in sg_bg_indices.items()}
#        # now, it is not desirable to divide by too large a number
#        # that would result in rounding error
#        # so we rescale by the average weight sum
#        print("calculating rescale")
#        ave_weight_sum = np.mean(list(sg_bg_weight_sum.values()))
#        # now check this needs doing
#        all_the_same = True
#        alpha = ave_weight_sum/1000.
#        for key in sg_bg_weight_sum:
#            if np.abs(sg_bg_weight_sum[key] - ave_weight_sum) > alpha:
#                all_the_same = False
#                break
#        if all_the_same:
#            print("The flavours are already balanced; {}".format(sg_bg_weight_sum))
#            print("Done")
#            return
#        else:
#            print("Starting weight distribution is {}".format(sg_bg_weight_sum))
#        sg_bg_rescaling = {k: ave_weight_sum/v for k, v in sg_bg_weight_sum.items()}
#        # now multiply the right indices by this factor
#        for key in sg_bg_indices:
#            weights[sg_bg_indices[key]] *= sg_bg_rescaling[key]
#        # put it back in the file
#        sg_bg_weight_sum = {k: np.sum(weights[v]) for k, v in sg_bg_indices.items()}
#        print("Now weight distribution is {}".format(sg_bg_weight_sum))
#        hf['data'][:, weight_i] = weights
#    print("Done")
#


        

        
