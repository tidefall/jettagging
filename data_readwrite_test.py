''' Tests for data_readwrite'''

import numpy as np
import h5py
import numpy.testing as tst
import os
from data_readwrite import setup_data_file, INITIAL_ROWS, gen_jet_vars, JetVariable, read_variables, next_free_indices, add_indices, Indexer, variable
from ipdb import set_trace as st


MAX_COLUMNS = 100


def test_gen_jet_vars():
    jet_vars = gen_jet_vars()
    jv_indices = [v.indices[0] for v in jet_vars]
    tst.assert_allclose(np.array(jv_indices), np.arange(len(jet_vars)))


def test_JetVariable():
    names = ['a', 'b', 'c']
    index = 3
    classes = {2: 'a', 'f': 'b'}
    unphysical = -2
    jet_var1 = JetVariable(*names, index, classes=classes)
    jet_var2 = JetVariable(*names, index, classes=classes,
                           unphysical=unphysical)
    attrs1, arrs1 = jet_var1.get_properties()
    attrs2, arrs2 = jet_var2.get_properties()
    jet_var11 = JetVariable.from_props(attrs1, arrs1)
    jet_var22 = JetVariable.from_props(attrs2, arrs2)
    assert jet_var11.short_name == names[0]
    assert jet_var11.long_name == names[1]
    assert jet_var11.root_name == names[2]
    assert jet_var11.indices == [index]
    assert jet_var11.classes == classes
    assert jet_var11.unphysical is None
    assert jet_var22.unphysical == unphysical
    
    
def test_variable():
    names1 = ['a', 'b', 'c']
    names2 = ['d', 'e', 'f']
    classes = {2: 'a', 'f': 'b'}
    unphysical = -2
    jet_var1 = JetVariable(*names1, 0, classes=classes)
    jet_var2 = JetVariable(*names2, 1, classes=classes,
                           unphysical=unphysical)
    jet_vars = [jet_var1, jet_var2]
    get_var_1 = variable(0, jet_vars)
    assert get_var_1.short_name == 'a'
    get_var_1 = variable('b', jet_vars)
    assert get_var_1.short_name == 'a'
    get_var_2 = variable(1, jet_vars)
    assert get_var_2.short_name == 'd'
    get_var_2 = variable('f', jet_vars)
    assert get_var_2.short_name == 'd'


class test_file_path:
    '''context manager for creating a test folder and putting a test file in it'''
    def __init__(self):
        # find out what directory names are already in use
        root, dirs, files = next(os.walk("./"))
        # make one that isn't in use
        self.test_dir_name = "test"
        while (self.test_dir_name in dirs) or (self.test_dir_name in files):
            self.test_dir_name += 'a'
        assert self.test_dir_name not in dirs
        
    def __enter__(self):
        # set up a directory
        os.mkdir(self.test_dir_name)
        # put some data in it
        data_file_name = "myData.h5py"
        jet_vars = gen_jet_vars()
        self.data_path = os.path.join(self.test_dir_name, data_file_name)
        setup_data_file(self.data_path, jet_vars)
        return self.data_path

    def __exit__(self, *args):
        # delete the file
        os.remove(self.data_path)
        # delete the folder
        os.rmdir(self.test_dir_name)


def test_setup_data_file():
    ''' test script for function that creates a new data file'''
    jet_vars = gen_jet_vars()
    with test_file_path() as fp:
        print(fp)
        with h5py.File(fp, 'r') as f_root:
            assert 'data' in f_root
            data = f_root['data']
            assert data.shape == (INITIAL_ROWS, len(jet_vars))
            assert not data.attrs['normed']
            assert not data.attrs['weighted'] 
            assert data.attrs['n jets'] == 0
            assert "variables" in f_root


def test_read_variables():
    ''' test script for function that creates a new data file'''
    jet_vars = gen_jet_vars()
    with test_file_path() as fp:
        jet_vars_read = read_variables(fp)
        assert len(jet_vars) == len(jet_vars_read)
        for var in jet_vars:
            read_var = variable(var.short_name, jet_vars_read)
            assert read_var is not None


def test_next_free_indices():
    jet_vars = gen_jet_vars()
    # we know to start with that there are one indices per jet
    n_indices = len(jet_vars)
    for n_add in range(0, 5):
        expected = [n_indices + i for i in range(n_add)]
        got = next_free_indices(jet_vars, n_add)
        assert expected == got, "Expected indices {}, got {}".format(expected,
                                                                     got)
    # now add 3 indices to every jet
    for i, var in enumerate(jet_vars):
        expected = [n_indices + i*3 + ii for ii in range(3)]
        got = next_free_indices(jet_vars, 3)
        assert expected == got, "Expected indices {}, got {}".format(expected,
                                                                     got)
        add_indices(var.short_name, got, jet_vars)
        

def test_add_indices():
    # decide what to play with
    jet_vars = gen_jet_vars()
    to_add = [[23], [24, 25], []]
    short_names = ["nJet", "flavour", "trkEtaRel"]
    selected_vars = [v for v in jet_vars if v.short_name in short_names]
    initial_indices = [list(v.indices) for v in selected_vars]
    # add the first set
    add_indices(short_names[0], to_add[0], jet_vars)
    assert selected_vars[0].indices == initial_indices[0] + to_add[0]
    assert selected_vars[1].indices == initial_indices[1]
    assert selected_vars[2].indices == initial_indices[2]
    # second set
    add_indices(short_names[1], to_add[1], jet_vars)
    assert selected_vars[0].indices == initial_indices[0] + to_add[0]
    assert selected_vars[1].indices == initial_indices[1] + to_add[1]
    assert selected_vars[2].indices == initial_indices[2]
    # third set (shouldn't change anything)
    add_indices(short_names[2], to_add[2], jet_vars)
    assert selected_vars[0].indices == initial_indices[0] + to_add[0]
    assert selected_vars[1].indices == initial_indices[1] + to_add[1]
    assert selected_vars[2].indices == initial_indices[2]
    

def test_Indexer():
    my_a = np.arange(100).reshape((10, 10))
    # test basic properties
    my_i = Indexer(0, 0)
    assert len(my_a[my_i[:]]) == 0
    my_i = Indexer(1, 1)
    tst.assert_allclose(my_a[my_i[[0]]], np.array([[0]]))
    # test on array
    my_i = Indexer(10, 10)
    indices_list = [[0], [-1], ([1], [1]), (slice(None, None), [2]),
                    ([1], slice(3, None)), ([2, 3, 4])]
    for indices in indices_list:
        tst.assert_allclose(my_a[indices], my_a[indices])
    # test selections
    my_i.add_rowsel("skip0", [i for i in range(1, 10)])
    my_i.add_colsel("only5", [5])
    tst.assert_allclose(my_a[1, 5], my_a[my_i[[0]]])
    
