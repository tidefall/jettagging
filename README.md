# README #

This repository holds the core scripts, functions and classes they were used in a Jet Tagging project.

### How do I use them? ###

This isn't a program that can be run with one line and turn data into answers.
My use of these scripts has mostly been from a python prompt.


### Contact information ###

I am happy to answer whatever questions I am able to, and even more happy to get feedback on what you see here.
Please drop me an email at henrydayhall AT protonmail DOT com.