from ipdb import set_trace as st
import numpy as np
import matplotlib.pyplot as plt
from sys import argv
import sys
from output_read import get_flag, Run_recording, filter_runs, split_list,\
                        sort_catigory_names

def diffs(file_names):
    list_runs = filter_runs(file_names)
    print("Found {} runs".format(len(list_runs)))
    split_run_dict, settings_names = split_list(list_runs)
    print("Divided into {} catigories".format(len(split_run_dict)))
    diff_dict = {}
    for cat_name in split_run_dict:
        print("Analysing runs in {}".format(cat_name))
        global_diffs = []
        for run in split_run_dict[cat_name]:
            diffs = np.diff(run[:, 0])
            global_diffs += list(diffs)
        global_diffs = np.array(global_diffs)
        mean = np.mean(global_diffs)
        std_mean = np.std(global_diffs, ddof=1)/np.sqrt(len(global_diffs))
        diff_dict[cat_name] = (mean, std_mean)
        print("Global average for {} = {}+/-{}".format(cat_name, mean, std_mean))
    return diff_dict


def speeds(diff_dict, e_diff_dict):
    speed_dict = {}
    e_speed_dict = {}
    pal_speed_dict = {}
    for name in diff_dict:
        # get speed stats
        mean_speed = 1./diff_dict[name][0]
        std_mean_speed = mean_speed**2 * diff_dict[name][1]
        speed_dict[name] = (mean_speed, std_mean_speed)
        # get empty speed stats
        mean_e_speed = 1./e_diff_dict[name][0]
        std_mean_e_speed = mean_e_speed**2 * e_diff_dict[name][1]
        e_speed_dict[name] = (mean_e_speed, std_mean_e_speed)
        # get parallel speed stats
        mean_pal_diff = diff_dict[name][0] - e_diff_dict[name][0]
        mean_pal_speed = 1./mean_pal_diff
        std_pal_mean_speed = (mean_pal_speed**2 *
                              np.sqrt(std_mean_e_speed**2 + std_mean_speed**2))
        pal_speed_dict[name] = (mean_pal_speed, std_pal_mean_speed)
    return e_speed_dict, speed_dict, pal_speed_dict


def latex_speed(three_dicts):
    col1s = ["empty XXX", "XXX", "parallel speed"]
    names = [("IntelCPU",), ("GTXTi",), ("Tesla",)]
    table = []
    for col1, s_dict in zip(col1s, three_dicts):
        row = "{} ".format(col1)
        for name in names:
            row += "& \({:6f} \pm {:6f}\) ".format(*s_dict[name])
        row += "\\\\\n"
        table.append(row)
    return table


def save_latex(table):
    file_name = input("Give a file name ")
    with open(file_name, 'w') as f:
        for row in table:
            f.write(row)


def main():
    flags = ['-r1', '-r2']
    if len(argv) == 1:
        print("Flags are {}".format(flags))
        print("-r1 is the empty runs")
        print("-r2 is te ful runs")
        sys.exit(0)

    e_run_names = get_flag('-r1', flags)
    assert len(e_run_names) > 0, 'no runs given'
    run_names = get_flag('-r2', flags)
    assert len(run_names) > 0, 'no runs given'
    e_diff_dict = diffs(e_run_names)
    diff_dict = diffs(run_names)
    thress_dicts = speeds(diff_dict, e_diff_dict)
    table = latex_speed(thress_dicts)
    save_latex(table)



if __name__ == '__main__':
    main()
