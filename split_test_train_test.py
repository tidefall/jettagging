''' module to test the split_test_train module'''
import h5py
from data_root import add_rootFile
from data_readwrite_test import test_file_path
from split_test_train import assign_jets
from data_readwrite import read_variables, variable
import numpy as np
import numpy.testing as tst


def test_assign_jets():
    with test_file_path() as h5_name:
        add_rootFile("/media/henry/Unicorn/files/work/PhD/jetTagger/Data/TTTo2L2Nu/TTTo2L2Nu_1_ntp.root", h5_name, max_events=10)
        jet_vars = read_variables(h5_name)
        test_train_var = variable("testTrain", jet_vars)
        tt_i = test_train_var.indices[0]
        percent_test = 0.2
        inv_dict = {v: k for k, v in test_train_var.classes.items()}
        expt_mean = percent_test * inv_dict["test"] +\
                    (1 - percent_test) * inv_dict["train"]
        with h5py.File(h5_name, 'r') as hf:
            test_train_assigs = hf["data"][:, tt_i]
            assert np.all(np.isnan(test_train_assigs))
        assign_jets(h5_name)
        with h5py.File(h5_name, 'r') as hf:
            test_train_assigs = hf["data"][:, tt_i]
            assert not np.any(np.isnan(test_train_assigs))
            mean = np.mean(test_train_assigs)
            tst.assert_allclose(mean, expt_mean, atol=0.1)

            

