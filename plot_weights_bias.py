''' A module t allow the quick plotting of data'''
from scipy.stats import gaussian_kde
import csv
import matplotlib.pyplot as plt
import numpy as np
from ipdb import set_trace as st
from itertools import cycle
from output_read import get_flag, Run_recording
from matplotlib.lines import Line2D
import sys
import matplotlib
from matplotlib.lines import Line2D  # for legends


def plot_density(ax, values, fidelity=50):
    c_map = matplotlib.cm.get_cmap('cool')
    num_rows = float(len(values))
    min_x = np.min(values)
    max_x = np.max(values)
    xs = np.linspace(min_x, max_x, fidelity)
    for i, row in enumerate(values):
        c = float(i)/num_rows
        colour = c_map(c)
        kernal = gaussian_kde(row)
        density = kernal(xs)
        ax.plot(xs, density, color=colour)
    custom_lines = [Line2D([0], [0], color=c_map(0.), lw=1),
                    Line2D([0], [0], color=c_map(.5), lw=1),
                    Line2D([0], [0], color=c_map(1.), lw=1)]

    ax.set_xlim(min_x, max_x)
    ax.legend(custom_lines, ['Start', 'Mid point', 'End'])
    return ax   
    

def contour_plot(ax, values, epoch_gap=1, fidelity=50):
    min_y = np.min(values)
    max_y = np.max(values)
    # min_y = np.mean(values) - np.std(values) 
    # max_y = np.mean(values) + np.std(values) 
    num_rows = float(len(values))
    ys = np.linspace(min_y, max_y, fidelity)
    epochs = np.linspace(0, epoch_gap*num_rows, num_rows)
    densities = []
    for row in values:
        kernal = gaussian_kde(row)
        density = list(np.log(kernal(ys)+0.1))
        densities.append(density)
    densities = np.array(densities).T
    ax.pcolormesh(epochs, ys, densities, cmap='plasma')
    min_lim = min(abs(min_y), abs(max_y))
    ax.set_ylim(-min_lim, min_lim)
    return ax   


def get_run(log_file):
    weight_tail = "_weightL"
    bias_tail = "_biasL"
    base_name = log_file.split(weight_tail)[0]
    base_name = base_name.split(bias_tail)[0]
    run = Run_recording(base_name)
    return run


def plot_loss(over_ax, run):
    test_losses = run.set_columns("test_loss")
    # renormalise so that the loss has a minimum of 0 and peak around 1
    test_losses = test_losses - np.min(test_losses)
    test_losses = test_losses/max(test_losses[20:]*0.5)
    over_ax.plot(test_losses, color='lime', label="Test loss", alpha=1.)
    training_losses = run.set_columns("training_loss")
    # renormalise so that the loss has a minimum of 0 and peak around 1
    training_losses = training_losses - np.min(training_losses)
    training_losses = training_losses/max(training_losses[20:]*0.5)
    over_ax.plot(training_losses, color='gray', label="Train loss", alpha=1.)


def plot_weights(ax, run, over_ax):
    mag_weights = run.set_columns("mag_weights")
    mag_weights = (mag_weights - np.min(mag_weights))/(max(mag_weights[20:])*0.5)
    colour = 'white'
    label = "Magnitude weights"
    alpha = 1.
    line_style = ":"
    ax.plot(mag_weights, color=colour, label=label, alpha=alpha, ls=line_style)
    # add to the ledgened of over_ax
    over_ax.plot([0], [0], color=colour, ls=line_style, alpha=alpha, label=label)


def main():
    flags = ['-n', '-l', '-y', '--fidelity']
    if len(sys.argv) == 1:
        print("Flags are {}".format(flags))
        print("-n is name, (the title of the plot)")
        print("-y is y axis label")
        print("-l is the log file to work from (only one!)")
        print("--fidelity is the fidelity of the density ont eh y axis")
        sys.exit(0)
    name = " ".join(get_flag('-n', flags))
    y_label = " ".join(get_flag('-y', flags))
    log_file = get_flag('-l', flags)[0]
    fidelity = get_flag('--fidelity', flags)
    if len(fidelity) == 0:
        fidelity = 50
    elif fidelity[0].isdigit():
        fidelity = int(fidelity[0])
    else:
        print("Error, fidelity should be an int")
        fidelity = 50
    fig, ax = plt.subplots()
    # read the log of weight or bias
    with open(log_file, 'r') as csvfile:
        reader = csv.reader(csvfile, delimiter=' ')
        array = []
        for row in reader:
            array.append(row)
        array = np.array(array, dtype=float)
    # plot this as a colour map
    contour_plot(ax, array, fidelity=fidelity)
    ax.set_title(name)
    ax.set_ylabel(y_label)
    ax.set_xlabel("Epoch")
    # get the corrisponding run
    run = get_run(log_file)
    # plot this over the top
    over_ax = ax.twinx()
    plot_loss(over_ax, run)
    plot_weights(ax, run, over_ax)
    over_ax.set_ylabel("Loss (normalised unitless)")
    over_ax.set_ylim(0, 3)
    over_ax.legend()
    plt.show()
    
    
if __name__ == '__main__':
    main()
