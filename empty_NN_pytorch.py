''' CSVv2 replica '''
import torch
import os
import cpuinfo
from ipdb import set_trace as st
import sys
import time
import torch.nn as nn
from data_readwrite import read_variables, variable
from convert_old import old_name_to_var
from torch.utils.data import Dataset, DataLoader, WeightedRandomSampler
import h5py
import numpy as np


class CMS_dataset(Dataset):
    def __init__(self, h5_name, target_names, desired_cols, jet_class=None):
        # get the variable list 
        jet_vars = read_variables(h5_name)
        self.n_variables = len(desired_cols)
        assert len(set(desired_cols)) == self.n_variables, "Duplicate columns!! {}".format(desired_cols)
        # we also only want rows that contain the right class of jet
        if jet_class:
            desired_rows = []
            v_jetClass = variable("jetClass", jet_vars)
        else:
            desired_rows = slice(None, None)
        v_weight = variable("weight", jet_vars)
        v_flavour = variable("flavour", jet_vars)
        v_testTrain = variable("testTrain", jet_vars)
        flavour_classes = v_flavour.classes
        inv_f_classes = {v: k for k, v in flavour_classes.items()}
        self.target_names = target_names
        self.num_targets = len(self.target_names)
        target_classes = {inv_f_classes[f]: [int(i==p) for i in range(self.num_targets)]
                          for f, p in self.target_names.items()}
        bg_target = [0 for _ in range(self.num_targets)]
        # we also want the antiparticels
        target_classes = {**target_classes, 
                          **{-k: v for k, v in target_classes.items() 
                             if isinstance(k, int) or isinstance(k, float)}}
        with h5py.File(h5_name, 'r') as hf:
            n_jets = hf['data'].shape[0]
            if jet_class:
                is_class = np.isclose(hf['data'][:, v_jetClass.indices[0]],
                                      jet_class)
                desired_rows = np.where(is_class)[0]
            # get the flavours
            flavours = (hf['data'][:, v_flavour.indices[0]])[desired_rows]
            # transform the flavours into targets
            targets = np.array([target_classes.get(f, bg_target) for f in flavours])
            # the weights are also needed
            weights = (hf['data'][:, v_weight.indices[0]])[desired_rows]
            # finally we need to know what is test and what is train
            testTrain = (hf['data'][:, v_testTrain.indices[0]])[desired_rows]
        # split the aspects of the dataset into test and train
        test_key = next(k for k, v in v_testTrain.classes.items()
                        if v == 'test')
        train_key = next(k for k, v in v_testTrain.classes.items()
                         if v == 'train')
        desired_is_train = np.isclose(testTrain, train_key)
        train_rows = np.arange(n_jets)[desired_rows][desired_is_train]
        self.n_train = len(train_rows)
        desired_is_test = np.isclose(testTrain, test_key)
        self.test_rows = np.arange(n_jets)[desired_rows][desired_is_test]
        self.train_targets = targets[desired_is_train]
        self.test_targets = targets[desired_is_test]  
        self.test_flavours = np.array([flavour_classes.get(f, 'udsg')
                                      for f in flavours[desired_is_test]])
        self.train_weights = weights[desired_is_train]  # test set dosn't get weighted.
        # pull the right jets into ram
        with h5py.File(h5_name, 'r') as hf:
            # we can no longer garantee the columns are in order
            sorted_cols = sorted(desired_cols)
            col_order = list(map(sorted_cols.index, desired_cols))
            self.train = hf['data'][:, sorted_cols]
            # needed because h5py cannot index out of order
            self.train = self.train[:, col_order]
            self.train = self.train[train_rows]
            self.test = hf['data'][:, sorted_cols]
            self.test = self.test[:, col_order]
            self.test = self.test[self.test_rows]
            # hopefully the garbage collector works here

    def interpolate(self, function):
        self.test, [self.test_targets,
                    self.test_rows,
                    self.test_flavours] = function(self.test,
                                                   [self.test_targets,
                                                    self.test_rows,
                                                    self.test_flavours])
        self.train, [self.train_targets,
                     self.train_weights] = function(self.train,
                                                    [self.train_targets,
                                                     self.train_weights])
        self.n_train = self.train.shape[0]

    def __len__(self):
        return self.n_train

    def __getitem__(self, idx):
        jet = self.train[idx]
        target = self.train_targets[idx]
        self.n_train = len(self.train)
        return target, jet

    def get_testjets(self):
        return self.test


class cut_CSVv2_dataset(CMS_dataset):
    def __init__(self, h5_name, jet_class, max_num_tracks=4):
        # get the variable list 
        jet_vars = read_variables(h5_name)
        # sort out which variables are wanted
        used_observables = ["IP3DSig", "SVdeltaR", "SVeRatio", "aboveC",
                            "flightDis2DSig", "nSVTrks",
                            "nSVs", "nTrks", "sumDeltaR", "sumTrkEtRatio",
                            "trkDecayLen", "trkDeltaR", "trkDis", "trkEtaRel",
                            "trkPtRatio", "trkPtRel", "vertexMass"]
        # start by takng the first inidex of each observable
        desired_cols = [int(var.indices[0]) for var in jet_vars
                        if (not var.truth and
                            var.short_name in used_observables)]
        # add on up to 3 more from 3D IP signifcance to get the first 4 tracks
        assert max_num_tracks >= 1
        v_3dIPSig = variable("IP3DSig", jet_vars)
        desired_cols += list(map(int, v_3dIPSig.indices[1:min(max_num_tracks, 4)]))
        desired_cols = sorted(desired_cols)
        target_names = {'b': 0}
        CMS_dataset.__init__(self, h5_name, target_names, desired_cols, jet_class)
        # as there is no jet eta or jet pt there is no need for weights
        self.train_weights = np.ones_like(self.train_weights)


class CSVv2_dataset(CMS_dataset):
    def __init__(self, h5_name, jet_class, max_num_tracks=4):
        # get the variable list 
        jet_vars = read_variables(h5_name)
        # sort out which variables are wanted
        used_observables = ["IP3DSig", "SVdeltaR", "SVeRatio", "aboveC",
                            "flightDis2DSig", "jetEta", "jetPt", "nSVTrks",
                            "nSVs", "nTrks", "sumDeltaR", "sumTrkEtRatio",
                            "trkDecayLen", "trkDeltaR", "trkDis", "trkEtaRel",
                            "trkPtRatio", "trkPtRel", "vertexMass"]
        # start by takng the first inidex of each observable
        desired_cols = [int(var.indices[0]) for var in jet_vars
                        if (not var.truth and
                            var.short_name in used_observables)]
        # add on up to 3 more from 3D IP signifcance to get the first 4 tracks
        assert max_num_tracks >= 1
        v_3dIPSig = variable("IP3DSig", jet_vars)
        desired_cols += list(map(int, v_3dIPSig.indices[1:min(max_num_tracks, 4)]))
        desired_cols = sorted(desired_cols)
        target_names = {'b': 0}
        CMS_dataset.__init__(self, h5_name, target_names, desired_cols, jet_class)


class Old_dataset(CMS_dataset):
    def __init__(self, h5_name, jet_class, col_dict, max_num_tracks=3):
        assert max_num_tracks >=1
        # get the variable list 
        jet_vars = read_variables(h5_name)
        # sort out which variables are wanted
        # working with an old net
        # may need to rearange the columns
        inverse_col_dict = {v: k for k, v in col_dict.items()}
        desired_cols = []
        for col_n in range(len(col_dict)):
            old_name = inverse_col_dict[col_n]
            var, track_num = old_name_to_var(old_name, jet_vars)
            # by this point we should have a var num and a track num
            # check if this is a truth value
            if var.truth:
                continue
            # check it's track is not above the max num tracks
            if track_num > max_num_tracks:
                continue
            # need to get a column
            desired_cols.append(int(var.indices[track_num - 1]))
            # N.B. trac num starts from 1 not 0
        target_names = {'b': 0, 'c': 1, 'udsg': 3}
        CMS_dataset.__init__(self, h5_name, target_names, desired_cols, jet_class)


class CSVv2_Net(nn.Module):
    """A 'deep' neural net with one hidden layer"""
    def __init__(self, input_size, num_classes):
        """Initilzation for the net. Creates the layer that will be used.

        Parameters
        ----------
        input_size : int
            the number of nodes at the input layer. This must equal the number
            of variables in each data point.
        num_classes : int
            number of nodes at the output layer. This must equal the number of
            classes we wish to sort the data into.

        """
        hidden_size = input_size*2
        super(CSVv2_Net, self).__init__()
        # create a linear fully connected layer
        self.hidden_layer = nn.Linear(input_size, hidden_size)
        # create a ReLU activation function
        self.relu = nn.ReLU()
        # create anouther linear fully connected layer
        self.out_layer = nn.Linear(hidden_size, num_classes)

    def forward(self, x):
        """Defines the actions for a forward pass through the net.

        Parameters
        ----------
        x : torch.FloatTensor
            the input variables from ths data point

        Returns
        -------
        x : torch.FloatTensor
            the output of the net
        
        """
        # begin by passing the data to a linear fully connected layer,
        # this si the hidden layer
        x = self.hidden_layer(x)
        # apply the activation function at the hidden layer
        x = self.relu(x)
        # move onto the output layer, which is also linear and fully connected
        x = self.out_layer(x)
        return x
    
    def get_weights(self):
        # cannot convert CUDA tensor to numpy
        hidden_weights = self.hidden_layer.weight.data
        out_weights = self.out_layer.weight.data
        weights = [hidden_weights, out_weights]
        return weights

    def get_bias(self):
        hidden_bias = self.hidden_layer.bias.data
        out_bias = self.out_layer.bias.data
        bias = [hidden_bias, out_bias]
        return bias


def remove_outliers(array, out_bound=2):
    mean = np.mean(array)
    std_dev = np.std(array)
    top_bound = mean + std_dev*out_bound
    bottom_bound = mean - std_dev*out_bound
    array = array[array < top_bound]
    array = array[array > bottom_bound]
    return array


class DeepCSV_dataset(CMS_dataset):
    def __init__(self, h5_name, max_num_tracks=6):
        assert max_num_tracks >= 1
        # get the variable list 
        jet_vars = read_variables(h5_name)
        # sort out which variables are wanted
        used_observables = ["IP3DSig", "SVdeltaR", "SVeRatio", "aboveC",
                            "flightDis2DSig", "jetEta", "jetPt", "nSVTrks",
                            "nSVs", "nTrks", "sumDeltaR", "sumTrkEtRatio",
                            "trkDecayLen", "trkDeltaR", "trkDis", "trkEtaRel",
                            "trkPtRatio", "trkPtRel", "vertexMass"]
        # take the first 6 coulmns if they exist
        desired_cols = []
        for var in jet_vars:
            if var.truth:
                # if it's a truth value it's not an input
                continue
            if var.short_name in used_observables:
                sel_c = var.indices[0:min(6, len(var.indices), max_num_tracks)]
                desired_cols += sel_c
        desired_cols = sorted(desired_cols)
        target_names = {'b': 0, 'c': 1, 'udsg': 3}
        CMS_dataset.__init__(self, h5_name, target_names, desired_cols)


class cut_DeepCSV_dataset(CMS_dataset):
    def __init__(self, h5_name, max_num_tracks=6):
        # get the variable list 
        jet_vars = read_variables(h5_name)
        # sort out which variables are wanted
        used_observables = ["IP3DSig", "SVdeltaR", "SVeRatio", "aboveC",
                            "flightDis2DSig", "nSVTrks",
                            "nSVs", "nTrks", "sumDeltaR", "sumTrkEtRatio",
                            "trkDecayLen", "trkDeltaR", "trkDis", "trkEtaRel",
                            "trkPtRatio", "trkPtRel", "vertexMass"]
        # take the first 6 coulmns if they exist
        desired_cols = []
        for var in jet_vars:
            if var.truth:
                # if it's a truth value it's not an input
                continue
            if var.short_name in used_observables:
                sel_c = var.indices[0:min(6, len(var.indices), max_num_tracks)]
                desired_cols += sel_c
        desired_cols = sorted(desired_cols)
        target_names = {'b': 0, 'c': 1, 'udsg': 3}
        CMS_dataset.__init__(self, h5_name, target_names, desired_cols)
        # as there is no jet eta or jet pt there is no need for weights
        self.train_weights = np.ones_like(self.train_weights)


class DeepCSV_Net(nn.Module):
    """A deep neural net with four hidden layers"""
    def __init__(self, input_size, num_classes):
        """Initilzation for the net. Creates the layer that will be used.

        Parameters
        ----------
        input_size : int
            the number of nodes at the input layer. This must equal the number
            of variables in each data point.
        num_classes : int
            number of nodes at the output layer. This must equal the number of
            classes we wish to sort the data into.

        """
        hidden_size = 100
        super(DeepCSV_Net, self).__init__()
        # create the first linear fully connected layer
        self.hidden_layer_1 = nn.Linear(input_size, hidden_size)
        # create the second linear fully connected layer
        self.hidden_layer_2 = nn.Linear(hidden_size, hidden_size)
        # create the third linear fully connected layer
        self.hidden_layer_3 = nn.Linear(hidden_size, hidden_size)
        # create the forth linear fully connected layer
        self.hidden_layer_4 = nn.Linear(hidden_size, hidden_size)
        # create a ReLU activation function
        self.relu = nn.ReLU()
        # create anouther linear fully connected layer
        self.out_layer = nn.Linear(hidden_size, num_classes)

    def forward(self, x):
        """Defines the actions for a forward pass through the net.

        Parameters
        ----------
        x : torch.FloatTensor
            the input variables from ths data point

        Returns
        -------
        x : torch.FloatTensor
            the output of the net
        
        """
        # begin by passing the data to a linear fully connected layer,
        # this si the hidden layer
        x = self.hidden_layer_1(x)
        # apply the activation function at the hidden layer
        x = self.relu(x)
        # again
        x = self.hidden_layer_2(x)
        x = self.relu(x)
        # again
        x = self.hidden_layer_3(x)
        x = self.relu(x)
        # again
        x = self.hidden_layer_4(x)
        x = self.relu(x)
        # move onto the output layer, which is also linear and fully connected
        x = self.out_layer(x)
        return x
    
    def get_weights(self):
        weights = [self.hidden_layer_1.weight.data,
                   self.hidden_layer_2.weight.data,
                   self.hidden_layer_3.weight.data,
                   self.hidden_layer_4.weight.data,
                   self.out_layer.weight.data]
        return weights
    
    def get_bias(self):
        bias = [self.hidden_layer_1.bias.data, 
                self.hidden_layer_2.bias.data,
                self.hidden_layer_3.bias.data,
                self.hidden_layer_4.bias.data,
                self.out_layer.bias.data]
        return bias


def zero_fill_nan(inputs_array, other_components=[]):
    return np.nan_to_num(inputs_array), other_components


def fast_train(h5_file, run_time, device, net_type, batch_size=100, initial_lr=0.1, weight_decay=0.005):
    end_time = run_time + time.time()
    # create a dataset and a dataloader
    if net_type == 'csvv2':
        dataset = CSVv2_dataset(h5_file, jet_class=0)
        dataset.interpolate(zero_fill_nan)
    elif net_type == 'deepcsv':
        dataset = DeepCSV_dataset(h5_file)
        dataset.interpolate(zero_fill_nan)
    elif net_type == 'cutcsvv2':
        dataset = cut_CSVv2_dataset(h5_file, jet_class=0)
        dataset.interpolate(zero_fill_nan)
    elif net_type == 'cutdeepcsv':
        dataset = cut_DeepCSV_dataset(h5_file)
        dataset.interpolate(zero_fill_nan)
    else:
        print("Error; net_type {} not recognised, expecting 'csvv2' or 'deepcsv'".format(net_type))
        sys.exit(1)
    dataset_inv_size = 1./len(dataset)
    test_inputs = torch.FloatTensor(dataset.get_testjets()).to(device)
    test_targets = torch.FloatTensor(dataset.test_targets).to(device)
    sampler = WeightedRandomSampler(dataset.train_weights, len(dataset))
    dataloader = DataLoader(dataset, batch_size, sampler=sampler, pin_memory=False)
    # creat a net with a shape defined by the data
    if net_type == 'csvv2' or net_type == 'cutcsvv2':
        net = CSVv2_Net(dataset.n_variables, dataset.num_targets).to(device)
    elif net_type == 'deepcsv' or net_type == 'cutdeepcsv':
        net = DeepCSV_Net(dataset.n_variables, dataset.num_targets).to(device)
    else:
        print("Error; net_type {} not recognised, expecting 'csvv2' or 'deepcsv'".format(net_type))
        sys.exit(1)
    criterion = nn.BCEWithLogitsLoss() 
    # create an optimiser and an adaptive learning rate
    optimiser = torch.optim.Adam(net.parameters(), lr=initial_lr, weight_decay=weight_decay)
    scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimiser)
    # make initial measures
    net.eval()
    epoch_reached = 0
    last_time = time.time()
    time_stamps = [last_time]
    # need one batch to get an initial loss
    targets, inputs= next(dataloader.__iter__())
    targets = targets.type(torch.FloatTensor).to(device)
    inputs = inputs.to(device)
    # forward pass
    outputs = net(inputs)
    # find loss
    loss = criterion(outputs, targets)
    training_loss = [loss.item()]
    # test loss
    test_outputs = net(test_inputs)
    test_loss = [float(criterion(test_outputs, test_targets).item())]
    # Start working through the training epochs
    epoch_reached = 1
    last_time = time.time()
    while last_time < end_time:
        net.train()
        # start an epoch
        sum_loss = 0.
        for i_batch, sample_batched in enumerate(dataloader):
            targets, inputs = sample_batched
            targets = targets.type(torch.FloatTensor).to(device)
            inputs = inputs.to(device)
            # reset the optimiser
            optimiser.zero_grad()  # zero the gradient buffer
            # forward pass
            # don't pass anything through the net in the empty version
            # outputs = net(inputs)
            # find loss
            # loss = criterion(outputs, targets)
            # backwards pass
            # loss.backward()
            # sum_loss += loss.item()
            sum_loss += 1.
            # optimise weights according to gradient found by backpropigation
            # optimiser.step()
        training_loss.append(float(sum_loss) * dataset_inv_size)
        # calculate the loss on the test set
        net.eval()
        # test_outputs = net(test_inputs)
        # test_loss.append(float(criterion(test_outputs, test_targets).item()))
        test_loss.append(0.)
        # check to see if the lr should decrease
        # scheduler.step(test_loss[-1], epoch_reached)
        # look at current learning rate
        epoch_reached += 1 
        last_time = time.time()
        time_stamps.append(last_time)
    print("Finishing...")
    return net, time_stamps, training_loss, test_loss


def begin_training(h5_name, run_time, batch_size, use_gpu, net_type, learning_rate, weight_decay):
    # Device configuration
    if use_gpu:
        if torch.cuda.is_available():
            device = torch.device('cuda')
            device_name = torch.cuda.get_device_name(0)
            major_cap, minor_cap = torch.cuda.get_device_capability(0)
            device_discription =\
                "{}; Capability major={}, minor={}"\
                .format(device_name, major_cap, minor_cap)
        else:
            print("Error, requested gpu but cuda not avalible")
            sys.exit(1)
    else:
        device = torch.device('cpu')
        device_discription = cpuinfo.cpu.info[0]['model name']

    # get the total ram to return
    mem = str(os.popen('free -t -m').readlines())
    total_ram = mem.index('T')

    training_returns = fast_train(h5_name, run_time, device, net_type, batch_size, initial_lr=learning_rate, weight_decay=weight_decay)
    # training_returns = train(h5_name, run_time, device, net_type, batch_size, initial_lr=learning_rate, weight_decay=weight_decay)
    return device_discription, total_ram, training_returns
