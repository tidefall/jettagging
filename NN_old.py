"""
Neural network for processing data.
An immitation of CSVv2

"""
import torch.nn as nn
from ipdb import set_trace as st


#########################################################
#
# FUNCTION DECLARATIONS
#
#########################################################

class Net(nn.Module):
    """A 'deep' neural net with one hidden layer"""
    def __init__(self, input_size, num_classes):
        """Initilzation for the net. Creates the layer that will be used.

        Parameters
        ----------
        input_size : int
            the number of nodes at the input layer. This must equal the number
            of variables in each data point.
        num_classes : int
            number of nodes at the output layer. This must equal the number of
            classes we wish to sort the data into.

        """
        hidden_size = input_size*2
        super(Net, self).__init__()
        # create a linear fully connected layer
        self.hidden_layer = nn.Linear(input_size, hidden_size)
        # create a ReLU activation function
        self.relu = nn.ReLU()
        # create anouther linear fully connected layer
        self.out_layer = nn.Linear(hidden_size, num_classes)

    def forward(self, x):
        """Defines the actions for a forward pass through the net.

        Parameters
        ----------
        x : torch.FloatTensor
            the input variables from ths data point

        Returns
        -------
        x : torch.FloatTensor
            the output of the net
        
        """
        # begin by passing the data to a linear fully connected layer,
        # this si the hidden layer
        x = self.hidden_layer(x)
        # apply the activation function at the hidden layer
        x = self.relu(x)
        # move onto the output layer, which is also linear and fully connected
        x = self.out_layer(x)
        return x


