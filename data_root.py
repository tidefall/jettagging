''' a module the quarentines interacting with root files, no all machines have pyroot'''

import numpy as np
from torch.utils.data import Dataset
import h5py
from ipdb import set_trace as st
import inspect
from itertools import islice
import ROOT
import sys
from data_readwrite import read_variables, next_free_indices, add_indices, boolean_indexing, convert_unphysical, write_variables

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Contents of the data files
# 1. A numpy array, the centeral array, containing some referance to all items
#    that apply to indervidual jets
# 2. A list of long pretty names of the central array
# 3. A list of short pretty names of the central array
# 4. For each column of the central array that is an enumerated class
#    a decoding list that gives short pretty names for each catigory
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


def add_rootFile(root_fname, hdf5_fname, tree_path="btagana/ttree", max_events=None):
    # set up the root file
    print("opening root file...")
    root_file = ROOT.TFile(root_fname)
    root_tree = root_file.Get(tree_path)
    # read the variable list
    print("Generating variable list...")
    jet_vars = read_variables(hdf5_fname)
    # if the root name is non it's not in the root file
    root_jet_vars = [v for v in jet_vars if v.root_name != 'None']
    # open the hdf5
    print("Opening hdf5....")
    with h5py.File(hdf5_fname, 'a') as hf:
        # go through the jet variables and give them enough indices
        format_in_root = []  # is it per event, per track or per er track
        n_new_indices = 0  # to expand the data array
        print("Checking which indices to add...")
        for var in root_jet_vars:
            print("Working on {}...".format(var.short_name))
            leaf = root_tree.GetLeaf(var.root_name)
            counter = leaf.GetLeafCount()
            # if the counter is a null pointer then this is a per event var
            if not counter:
                format_in_root.append("event")  # int means one per event
                max_len = 1  # there is one event variable for several jets
            elif counter.GetName() == 'nJet':
                # using the name of the counter we can find the format
                # per jet
                format_in_root.append('jet')
                max_len = 1  # one jet per jet variable
            elif counter.GetName() == 'nTrkTagVarCSV':
                # per track
                format_in_root.append('trk')
                # the max of the counter is the max number of track per jet
                max_len = counter.GetMaximum()
            elif counter.GetName() == 'nTrkEtaRelTagVarCSV':
                # per eta rel track
                format_in_root.append('ertrk')
                # the max of the counter is max number of tracks per jet
                max_len = counter.GetMaximum()
            # if we need to add more indices
            if max_len <= len(var.indices):
                # we dont need to add any indices here
                continue
            else:
                num_to_add = max_len - len(var.indices)
                n_new_indices += num_to_add
                next_free_i = next_free_indices(jet_vars, num_to_add)
                add_indices(var.short_name, next_free_i, jet_vars)
        print("Added {} indices.".format(n_new_indices))
        # should now have enough indices to store all data
        # the file will be read in chunks
        chunk_size = 200
        n_rows, n_cols = hf["data"].shape
        jet_reached = n_rows
        # add the first chunk and the new indices
        n_rows += chunk_size
        n_cols += n_new_indices
        hf['data'].resize((n_rows, n_cols))
        # start itterating over the tree entries
        event_n = 0
        print("Looping over events in tree...")
        while root_tree.GetEntry(event_n):
            n_jets = root_tree.nJet
            # make a  block to store the next clump of data
            block = np.full((n_jets, n_cols), np.nan)
            # check if we need a new chunk
            while n_jets + jet_reached > n_rows:
                # need another chunk
                n_rows += chunk_size
                hf['data'].resize((n_rows, n_cols))
            # get the distribution of tracks between the jets
            num_tracks_per_jet = [int(n) for n
                                  in root_tree.TagVarCSV_jetNTracks]
            num_er_tracks_per_jet = [int(n) for n
                                     in root_tree.TagVarCSV_jetNTracksEtaRel]
            # go though the variables and add them
            for var_shape, var in zip(format_in_root, root_jet_vars):
                from_root = getattr(root_tree, var.root_name)
                if var_shape == 'event':
                    # this is a per event variable
                    # duplicate it for each jet and add to all jets
                    values = from_root * np.ones((n_jets, 1))
                elif len(from_root) == 0:
                    # we got nothing
                    continue
                elif var_shape == 'jet':
                    # simplist case
                    values = np.array(list(from_root)).reshape((n_jets, 1))
                elif var_shape == 'trk':
                    # per track variabels need sorting into jets
                    # using the fact that th eobject returned by
                    # root is an iterator and taking slices off the start
                    values = [list(islice(from_root, 0, i)) for i in num_tracks_per_jet]
                    values = np.array(boolean_indexing(values))
                elif var_shape == 'ertrk':
                    # same deal for ertracks
                    values = [list(islice(from_root, 0, i)) for i in num_er_tracks_per_jet]
                    values = np.array(boolean_indexing(values))
                # it is possible for there to be no entries
                if len(values) == 0:
                    continue
                # fix unphysical
                values = convert_unphysical(values, var)
                # put it in the data
                required_cols = var.indices[:len(values[0])]
                block[:, required_cols] = values
            hf['data'][jet_reached:jet_reached + n_jets] = block

            jet_reached += n_jets
            if event_n % 10 == 0:
                print(".", end='')
                sys.stdout.flush()
            event_n += 1
            if max_events:
                if event_n > max_events:
                    break
        # trim the array back to the number of jets
        n_rows = jet_reached
        hf['data'].resize((n_rows, n_cols))

    print("\nDone reading tree.")
    # update the variables on the disk
    write_variables(hdf5_fname, jet_vars)
    # the root file must be closed manually
    # because pyroot was written by heathens
    print("Closing root file.")
    root_file.Close()

