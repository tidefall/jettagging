''' module for data reading tools '''
import re
from keras.models import load_model
import torch
import json
from sys import argv
import sys
from os.path import split
import time
import datetime
import csv
import numpy as np
from ipdb import set_trace as st
from NN_pytorch import CSVv2_dataset, DeepCSV_dataset, cut_CSVv2_dataset, CSVv2_Net, DeepCSV_Net, Old_dataset, zero_fill_nan, cut_DeepCSV_dataset
from NN_old import Net as Old_net
import h5py
from data_readwrite import read_variables


def get_flag(flag, flags):
    contents = []
    collect = False
    for arg in argv:
        if collect:
            # check this isn't the next flag
            if arg in flags:
                return contents
            else:
                contents.append(arg)
        if arg == flag:
            collect = True
    return contents


class Run_recording:
    def __init__(self, base_name):
        self.base_name = base_name
        progress_file_name = base_name + ".txt"
        with open(progress_file_name, 'r') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=' ')
            # the first line in the file is info
            info_line = next(csv_reader)
            assert len(info_line) == 11, "Why are there {} items in the info line? file {}".format(len(info_line), progress_file_name)
            self.settings = {}
            self.settings["pretty_name"] = split(base_name)[-1]
            self.settings["h5_name"] = info_line[0]
            self.settings["run_time"] = int(info_line[1])
            self.settings["batch_size"] = int(info_line[2])
            self.settings["initial_lr"] = float(info_line[3])
            self.settings["weight_decay"] = float(info_line[4])
            self.settings["device_type"] = info_line[5]
            self.settings["net_type"] = info_line[6]
            self.settings["library"] = info_line[7]
            self.settings["trained_on"] = info_line[8]
            self.settings["device_description"] = info_line[9]
            # create a one word device name:
            known_descriptions = {"Intel(R) Xeon(R) Gold 6138 CPU @ 2.00GHz": 'IntelCPU',
                                  "GeForce GTX 1080 Ti; Capability major=6, minor=1": 'GTXTi',
                                  "Tesla V100-PCIE-16GB; Capability major=7, minor=0": 'Tesla'}
            for desc in known_descriptions:
                if self.settings["device_description"] == desc:
                    self.settings["device_name"] = known_descriptions[desc]
            if "device_name" not in self.settings:
                name = self.settings["device_description"].split(' ')[0]
                print("Never seen {}, calling it {}".format(self.settings["device_description"],
                                                            name))
                self.settings["device_name"] = name
            self.settings["total_ram"] = int(info_line[10][10:])
            # the line under this is the columns
            self.column_headings = next(csv_reader)
            # then tehre is the table
            table = []
            for row in csv_reader:
                table.append(row)
            self.table = np.array(table, dtype=np.float)
            self.selected = np.copy(self.table)

    def __getitem__(self, idx):
        return self.selected[idx]

    def set_columns(self, columns):
        if isinstance(columns, str):
            columns = [columns]
        i_columns = []
        for column in columns:
            if not isinstance(column, int):
                column = self.column_headings.index(column)
            i_columns.append(column)
        self.selected = self.table[:, i_columns]
        return self.selected

    def __len__(self):
        return len(self.selected)

    def __str__(self):
        return "{}; device {}, net {}, batch {}"\
                .format(self.settings["pretty_name"],
                        self.settings["device_type"],
                        self.settings["net_type"],
                        self.settings["batch_size"])

    def load_net(self, n_variables, n_targets):
        if self.settings["library"] == 'pytorch':
            extention = '.torch'
        elif self.settings["library"] == 'keras':
            extention = '.keras'
        net_name = self.base_name + extention
        # load the net
        if ((self.settings["net_type"].lower() == 'csvv2' 
             or self.settings["net_type"].lower() == 'cutcsvv2')
                and self.settings["library"] == 'pytorch'):
            net = CSVv2_Net(n_variables, n_targets)
            net.load_state_dict(torch.load(net_name, map_location='cpu'))
        elif ((self.settings["net_type"].lower() == 'deepcsv'
               or self.settings["net_type"].lower() == 'cutdeepcsv')
                and self.settings["library"] == 'pytorch'):
            net = DeepCSV_Net(n_variables, n_targets)
            net.load_state_dict(torch.load(net_name, map_location='cpu'))
        elif (self.settings["net_type"].lower() == 'old'
                and self.settings["library"] == 'pytorch'):
            net = Old_net(22, 3)
            state_dict = torch.load(net_name, map_location='cpu')["net"][-1]
            net.load_state_dict(state_dict)
        elif self.settings["library"] == 'keras':
            net = load_model(net_name)
        else:
            print("Error; self.settings['net_type'] {} not recognised, expecting 'csvv2' or 'deepcsv'".format(self.settings["net_type"]))
            sys.exit(1)
        self.net = net
        return net

    def get_time(self):
        # pick the first timestamp
        t_0 = self.selected[0][0]
        time_0 = time.gmtime(t_0)
        self.time = datetime.datetime(*time_0[:6])
        return self.time




def get_base_names(all_file_names):
    # start by grabbing the relevant files
    file_extentions = ["txt", "torch", "keras"]
    run_file_names = [f.rpartition('.')[0] for f in all_file_names
                      if f.rpartition('.')[-1] in file_extentions]
    # now reduce to a set of base names
    base_names = list(set(run_file_names))
    return base_names


def sort_catigory_names(catigory_names):
    # start by pullling any numbers out
    num_names = []
    for name in catigory_names:
        num_name = []
        for part in name:
            if isinstance(part, str):
                found_values = re.findall('-?\d+\.?\d*', part)
                if found_values != []:
                    num_name.append(float(found_values[0]))
                else:
                    num_name.append(part)
            else:
                num_name.append(part)
        num_names.append(num_name)
    sorted_list = sorted(zip(num_names,catigory_names), key=lambda t: t[0])
    _, sorted_names = zip(*sorted_list)
    return sorted_names


def map_settings(list_runs, catigory_function, continuous_function):
    settings_keys = list(sorted(list_runs[0].settings.keys()))
    # remove the two fields likely to contain spaces
    settings_keys.remove("pretty_name")
    settings_keys.remove("device_description")
    print("Avalible settings;")
    print(list(enumerate(settings_keys)))
    desired_settings = input("Give the numbers of settings to operate on (seperator=,); ")
    desired_settings = [settings_keys[int(s)] for s
                        in desired_settings.split(',')
                        if s != '']
    return_vals = None
    for setting_key in desired_settings:
        print("Working on {}".format(setting_key))
        all_values = list(set([run.settings[setting_key]
                               for run in list_runs]))
        if isinstance(all_values[0], str):
            input_processing = lambda s: str(s)
            return_vals, list_runs = catigory_function(all_values, return_vals,
                                                       setting_key, list_runs,
                                                       input_processing)
        elif isinstance(all_values[0], int):
            input_processing = lambda i: int(i)
            if len(all_values) < 8:
                return_vals, list_runs = catigory_function(all_values, return_vals,
                                                           setting_key, list_runs,
                                                           input_processing)
            else:
                return_vals, list_runs = continuous_function(all_values, return_vals,
                                                             setting_key, list_runs,
                                                             input_processing)
        elif isinstance(all_values[0], float):
            input_processing = lambda f: float(f)
            return_vals, list_runs = continuous_function(all_values, return_vals,
                                                         setting_key, list_runs,
                                                         input_processing)
        assert len(list_runs) > 0, "error, no valid runs"
    return return_vals, desired_settings


def filter_catigory(all_values, return_vals, setting_key,
                    list_runs, input_processing):
    print("Possible values={}".format(list(all_values)))
    desired_values = input("Name the ones to keep (seperator=,); ").split(',')
    desired_values = [input_processing(val) for val in desired_values if val!='']
    list_runs = [run for run in list_runs
                 if run.settings[setting_key]
                 in desired_values]
    return_vals = list_runs
    return return_vals, list_runs


def filter_continuous(all_values, return_vals, setting_key,
                      list_runs, input_processing):
    print("Values range from {} to {}".format(min(all_values), max(all_values)))
    print("All values {}".format(all_values))
    bounds = input("Give 'lower, upper' bounds; ").split(',')
    lower_bound = input_processing(bounds[0])
    upper_bound = input_processing(bounds[1])
    list_runs = [run for run in list_runs
                 if run.settings[setting_key]>lower_bound
                 and run.settings[setting_key]<upper_bound]
    return return_vals, list_runs


def filter_runs(all_file_name):
    base_names = get_base_names(all_file_name)
    list_runs = []
    not_runs = []
    for name in base_names:
        try:
            run = Run_recording(name)
            list_runs.append(run)
        except AssertionError:
            not_runs.append(name)
    num_not_runs = len(not_runs)
    if num_not_runs > 0:
        print("{} files were not runs".format(num_not_runs))
        if num_not_runs < 4:
            print(not_runs)
    print("Number of real runs {}".format(len(list_runs)))
    list_runs, _ = map_settings(list_runs, filter_catigory, filter_continuous)
    return list_runs


def split_catigory(all_values, return_vals, setting_key,
                    list_runs, input_processing):
    print("creating catigories {}".format(list(all_values)))
    if return_vals is None:
        return_vals = {tuple(): list_runs}
    new_return_vals = {}
    for old_catigory_name in return_vals:
        for new_catigory in all_values:
            new_name = old_catigory_name + (new_catigory,)
            new_runs = [run for run in return_vals[old_catigory_name]
                        if run.settings[setting_key] == new_catigory]
            if len(new_runs) > 0:
                new_return_vals[new_name] = new_runs
    return new_return_vals, list_runs


def split_continuous(all_values, return_vals, setting_key,
                     list_runs, input_processing):
    display_name = setting_key
    if len(display_name) > 8:
        display_name = display_name.split("_")[0]
    # start by checking is we want to play min max games
    print("All values {}".format(all_values))
    catigories = input("Do you want all of these? (yes) ")
    if 'yes' in catigories:
        return split_catigory(all_values, return_vals, setting_key,
                              list_runs, input_processing)
    # ok, let the games commence
    min_val, max_val = min(all_values), max(all_values)
    print("Values range from {} to {}".format(min_val, max_val))
    splits = input("Give split points (seperator=,); ").split(",")
    splits = [input_processing(s) for s in splits if s!='']
    n_sections = len(splits) + 1
    epsilon = 1.
    splits = [min_val-epsilon] + splits + [max_val+epsilon]
    if return_vals is None:
        return_vals = {tuple(): list_runs}
    new_return_vals = {}
    for catigory_name in return_vals:
        old_cat_sum = len(return_vals[catigory_name])
        new_cat_sum = 0
        for i_sec in range(n_sections):
            lower_bound = splits[i_sec]
            upper_bound = splits[i_sec+1]
            split_name = "{}<{}<{}".format(lower_bound, display_name, upper_bound)
            new_name = catigory_name + (split_name, )
            new_runs = [run for run in return_vals[catigory_name]
                        if (lower_bound < run.settings[setting_key]  
                        and run.settings[setting_key] <= upper_bound)]
            new_cat_sum += len(new_runs)
            new_return_vals[new_name] = new_runs
        assert old_cat_sum == new_cat_sum, "Lost some runs"
    return new_return_vals, list_runs


def split_list(list_runs):
    catigory_dict, desired_settings = map_settings(list_runs, split_catigory, split_continuous)
    for catigory_name in catigory_dict:
        print("{} has {} runs".format(catigory_name, len(catigory_dict[catigory_name])))
    return catigory_dict, desired_settings


def guess_dataset_type(h5_name):
    with h5py.File(h5_name, 'r') as df:
        if 'variables' not in df:
            return 'old'
    # if we get this far it's a new type
    # now work on the name
    if 'csvv2' in h5_name.lower() or 'new' in h5_name.lower():
        return 'csvv2'
    elif 'deepcsv' in h5_name.lower():
        return 'deepcsv'
    else:
        print("Could not identify data type")


def create_data_set(data_type, net_type, h5_name, ddict_name):
    net_type = net_type.lower()
    if net_type == 'csvv2':
        dataset = CSVv2_dataset(h5_name, jet_class=0)
        dataset.interpolate(zero_fill_nan)
    elif net_type == 'cutcsvv2':
        dataset = cut_CSVv2_dataset(h5_name, jet_class=0)
        dataset.interpolate(zero_fill_nan)
    elif net_type == 'deepcsv':
        if data_type != 'deepcsv':
            print("Warning, data type may be insufficient for this net")
        dataset = DeepCSV_dataset(h5_name)
        dataset.interpolate(zero_fill_nan)
    elif net_type == 'cutdeepcsv':
        if data_type != 'deepcsv':
            print("Warning, data type may be insufficient for this net")
        dataset = cut_DeepCSV_dataset(h5_name)
        dataset.interpolate(zero_fill_nan)
    elif net_type == 'old':
        assert ddict_name is not None, "Need to give a ddict"
        with open(ddict_name, 'r') as jf:
            ddict = json.loads(jf.read())
        dataset = Old_dataset(h5_name, 0, ddict, 4)
        dataset.interpolate(zero_fill_nan)
    else:
        print("Error; net_type {} not recognised, expecting 'old', 'csvv2' or 'deepcsv'".format(net_type))
        sys.exit(1)
    return dataset


def predictions_and_targets(run_record, h5_name, ddict_name=None):
    """ returns a dataloader and a net using settings apropreate to the input """
    net_type = run_record.settings["net_type"]
    if net_type is None:
        print("Going with CSVv2")
        net_type = 'csvv2'
    backend = run_record.settings["library"]
    if backend is None:
        print("Going with pytorch")
        backend = 'torch'
    data_type = guess_dataset_type(h5_name)
    if data_type is None:
        print("Going with CSVv2")
        data_type = 'csvv2'
    # load the data
    dataset = create_data_set(data_type, net_type, h5_name, ddict_name)
    # load the net
    net = run_record.load_net(dataset.n_variables, dataset.num_targets)
    if backend == 'pytorch':
        test_inputs = torch.FloatTensor(dataset.get_testjets())
        net.eval()
        test_outputs = net(test_inputs)
        test_outputs = test_outputs.detach().numpy()
        # put the data through a sigmoid
        def sigmoid(vec):
            return 1./(1. + np.exp(-vec))
        results = sigmoid(test_outputs) 
    elif backend == 'keras':
        results = net.predict(dataset.get_testjets())
    else:
        print("Error; don't recognise backend {}, expecting 'torch' or 'keras'".format(backend))
        sys.exit(1)
    test_targets = dataset.test_targets
    test_flavours = dataset.test_flavours
    return results, test_targets, test_flavours


if __name__ == '__main__':
    file_names = argv[1:]
    runs_list = filter_runs(file_names)
    print("Done")
    
